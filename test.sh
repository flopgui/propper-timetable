#!/bin/bash
FILES=prop/fib/upc/edu/*Driver.class
CYAN='\033[0;36m'
BOLD='\033[1m'
NC='\033[0m'

set -e

cd target/test-classes

if [ ! -f asm-7.0.jar ]; then
    printf "\033[0;36mDownloading asm\n"
    wget -q https://repository.ow2.org/nexus/content/repositories/releases/org/ow2/asm/asm/7.0/asm-7.0.jar
fi
if [ ! -f asm-commons-7.0.jar ]; then
    printf "\033[0;36mDownloading asm-commons\n"
    wget -q https://repository.ow2.org/nexus/content/repositories/releases/org/ow2/asm/asm-commons/7.0/asm-commons-7.0.jar
fi
printf "\033[0;0m"


for f in $FILES
do
  printf "${CYAN}${BOLD}Processing $f ${NC}\n"
  tmp=$(basename $f)
  file="${tmp%.*}"
  echo "1 0" | java -ea -Djava.system.class.loader=prop.fib.upc.edu.CustomClassLoader \
    -cp ".:../classes:asm-7.0.jar:asm-commons-7.0.jar" prop.fib.upc.edu.${file}
done


