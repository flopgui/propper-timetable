# propper-timetable

Proyecto de PROP.


## TODO
Aquí faltan muchas cosas, pero bueno

- [X] Implementar el algoritmo de creación de Horarios.
- [X] Realizar esquema en UML presentacion.
- [X] Actualizar esquema en UML dominio.
- [X] Realizar esquema en UML persistencia.
- [X] Realizar diagrama de casos?
- [ ] Quitar System.out.print, TODOs y comentarios
- [ ] Que el jar sea uno, grande y libre
- [ ] Añadir recurso muere cuando añades muchos identicos


### PRESENTACION
- [X] VistaAulas (Niebo)
- [X] Gestión de recursos
- [ ] VistaTitulaciones: Mover arrastrando?
- [X] VistaAsignatura: Eliminar sesiones
- [ ] VistaHorario: Mover arrastrando
- [ ] VistaHorario: Cambiar aula
- [X] VistaGenerar
- [X] Preferencias
- [ ] Que funcionen todas las opciones del menu:
-   [ ] Abrir recientes
-   [ ] Importar
-   [ ] Exportar
- [ ] Asignaturas de varias horas en horario
- [X] Colores (opcional)
- [ ] Botones de ayuda (opcional)

### PERSISTENCIA
- [X] Guardar y cargar archivo
- [ ] Importar y exportar aulas
- [X] Exportar horario como png/pdf (opcional)

### MANUAL
- [X] Gestion de aulas y recursos
- [X] Gestion de titulaciones y asignaturas
- [X] Gestion de horarios
- [X] Menus
- [ ] Quitar TODOs

### PDF
- [ ] Nombre del programa.
- [ ] Nombre y username de cada miembro del grupo.
- [ ] Version.
- [ ] Numeración.
- [ ] Índice.
- [ ] Diagrama de casos de uso.
- [ ] Diagrama del modelo conceptual de datos.
- [ ] Breve descripcción de cada clase.
- [ ] Breve descripción de las estruras de datos y algoritmos implementados.

### FICHEROS
- [ ] Código de todas las clases del modelo.
- [ ] Código de las de dominio (¿controladores?).
- [ ] Ejecutables.
- [ ] Test con Drivers y Stubs.

### DRIVERS
- [X] Algoritmo
- [X] Asignatura
- [X] Aula
- [X] BloqueET
- [X] BloqueHorario
- [ ] CtrlDominio
- [X] Cuatrimestre
- [X] Grupo
- [X] HoraClase
- [X] Horario
- [X] Titulacion
- [X] Recurso
- [X] Restriccion
- [X] Sesion
- [X] Titulacion
- [X] UnidadDocente

### ESTRUCTURA
**NO USAR ACENTOS**
- descripción.txt (`apellidos, nombre`, `mail@est.fib.upc.edu` en orden alfabetico).
- **DOCS**
  - entrega.pdf
- **FONTS**
  - index.txt
  - **USUARIO**
    - index.txt
    - Clases
    - Drivers
    - Stubs
- **EXE**
  - Full risas


### Comando para correr los test
```bash
java  prop.fib.upc.edu.ClassDriver
```

Los `.jar`, se pueden descargar del repositorio de [maven](https://repository.ow2.org/nexus/content/repositories/releases/org/ow2/asm/) 
o, siguiendo estos links:
- [asm](https://repository.ow2.org/nexus/content/repositories/releases/org/ow2/asm/asm/7.0/asm-7.0.jar)
- [asm-commons](https://repository.ow2.org/nexus/content/repositories/releases/org/ow2/asm/asm-commons/7.0/asm-commons-7.0.jar)

### Comando para el javadoc
```bash
javadoc -private \
    -docletpath TeXDoclet.jar \
	-doclet org.stfm.texdoclet.TeXDoclet \
	-noindex \
	-tree \
	-hyperref \
	-texsetup doc/setup.tex \
	-texintro doc/intro.tex \
	-texfinish doc/finish.tex \
	-texinit doc/preamble.tex \
	-imagespath ".." \
	-output doc/especificacion.tex \
	-title "TeXDoclet Java Documentation" \
	-subtitle "Created with Javadoc TeXDoclet Doclet" \
	-author "Greg Wonderly \and S\"oren Caspersen \and Stefan Marx" \
	-sourcepath src/main/java \
	-subpackages prop \
	-sectionlevel section
```

El TeXDoclet.jar se puede conseguir de [aquí](https://github.com/doclet/texdoclet.git).
