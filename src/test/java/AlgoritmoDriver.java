import java.util.Scanner;
import java.util.Random;

public class AlgoritmoDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Algoritmo" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. generate: genera un horario");
        System.out.println("   3. getRestricciones: Obtiene las restricciones");
        System.out.println("   4. toggleRestriccion: Cambia el estado de una restriccion");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        Restriccion[] ar;
        Random random = new Random();
        Horario[] poblacion = new Horario[1];
        HoraClase[] ahc = new HoraClase[0];
        poblacion[0] = new Horario(ahc,random);
        Horario h;
        Algoritmo a = new Algoritmo(random);
        String s;
        boolean b;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    h = a.generate(poblacion, 10);
                    System.out.format("Generado un horario a partir de una poblacion de 1 y 10 generaciones\n");
                    break;
                case 3:
                    ar = a.getRestricciones();
                    System.out.format("Las restricciones son:\n");
                    for (int i=0; i<ar.length; ++i) {
                        System.out.format("%s\n: %s", ar[i].getNombre(), ar[i].isActive() ? "activa" : "inactiva");
                    }
                    System.out.println();
                    break;
                case 4:
                    System.out.format("Restriccion a cambiar? ");
                    s = sc.next();
                    b = a.toggleRestriccion(s);
                    System.out.format("%s %s", s, b ? "activada" : "desactivada (si existia)");
                    break;
            }
        } while (test > 0 && test < 5);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing Algoritmo" + ConsoleColors.RESET);

        Restriccion[] ar;
        Random random = new Random();
        Horario[] poblacion = new Horario[1];
        HoraClase[] ahc = new HoraClase[0];
        poblacion[0] = new Horario(ahc,random);
        Horario h;
        Algoritmo a = new Algoritmo(random);
        String s;
        boolean b;
        System.out.format("Creado Algoritmo.\n");

        h = a.generate(poblacion, 10);
        System.out.format("Generado un horario a partir de una poblacion de 1 y 10 generaciones\n");

        ar = a.getRestricciones();
        System.out.format("Las restricciones son:\n");
        for (int i=0; i<ar.length; ++i) {
            assert ar[i].isActive();
            System.out.format("%s\n: %s", ar[i].getNombre(), ar[i].isActive() ? "activa" : "inactiva");
        }
        System.out.println();

        a.toggleRestriccion(ar[0].getNombre());
        System.out.format("Cambiado el estado de %s\n", ar[0].getNombre());
        assert !ar[0].isActive();
        System.out.format("%s está inactiva\n", ar[0].getNombre());
        a.toggleRestriccion(ar[0].getNombre());
        System.out.format("Cambiado el estado de %s\n", ar[0].getNombre());
        assert ar[0].isActive();
        System.out.format("%s está activa\n", ar[0].getNombre());

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Algoritmo pasado\n" + ConsoleColors.RESET);
    }
}
