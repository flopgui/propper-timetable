import java.util.Scanner;
import java.util.Random;
import java.util.UUID;

public class PlanEstudiosDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase PlanEstudios" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. getNombre: Obtiene el nombre del PlanEstudios");
        System.out.println("   3. setNombre: Cambia el nombre del PlanEstudios");
        System.out.println("   4. addCuatrimestre: anade Cuatrimestre al PlanEstudios");
        System.out.println("   5. getNumeroCuatrimestres: Obtiene el numero de cuatrimetres en el PlanEstudios");
        System.out.println("   6. removeCuatrimestre: Elimina el quatrimestre del PlanEstudios");
        System.out.println("   7. getCuatrimestre: Obtiene el cuatrimestre  del PlanEstudios");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        PlanEstudios pe = new PlanEstudios("pe prueba");
        int n;
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.format("El nombre es %s\n",pe.getNombre());
                    break;
                case 3:
                    System.out.print("nombre? ");
                    s = sc.next();
                    pe.setNombre(s);
                    System.out.format("Cambiado el nombre a %s\n",s);
                    break;
                case 4:
                    System.out.print("nombre cuatrimestre? ");
                    s = sc.next();
                    pe.addCuatrimestre(new Cuatrimestre(s));
                    System.out.format("Anadido cuatrimestre %s\n",s);
                    break;
                case 5:
                    System.out.format("El numero de cuatrimestres es %d\n", pe.getNumeroCuatrimestres());
                    break;
                case 6:
                    System.out.print("nombre cuatrimestre para eliminar? ");
                    s = sc.next();
                    if(pe.removeCuatrimestre(s)) System.out.format("Se ha eliminado cuatrimestre %s\n",s);
                    else System.out.println("No existe cuatrimestre con nombre " + s);
                    break;
                case 7:
                    try {
                        System.out.print("El número de cuatrimestre que quieres buscar?\n");
                        n = sc.nextInt();
                        System.out.format("El cuatrimestre que buscas es %s\n", pe.getCuatrimestre(n).getNombre());
                    } catch (IndexOutOfBoundsException e) {
                        System.out.format("Indice superior al numero de cuatrimestres o negativo\n");
                    }
                    break;
            }
        } while (test > 0 && test < 8);
    }


    private static void testSuite() {

        String s = UUID.randomUUID().toString();
        PlanEstudios t = new PlanEstudios(s);
        System.out.println("Creada PlanEstudios con nombre " + s);
        assert t.getNombre().equals(s);
        s = UUID.randomUUID().toString();
        System.out.println("Cambiado nombre PlanEstudios a " + s);
        t.setNombre(s);
        assert t.getNombre().equals(s);
        System.out.println("Nombre Planestudios " + s);
        Random r = new Random();
        int n = r.nextInt(11);
        for(int i = 0; i < n; ++i) {
            s = UUID.randomUUID().toString();
            t.addCuatrimestre(new Cuatrimestre(s));
            System.out.println("Anadido cuatrimestre con nombre " + s);
            assert t.getCuatrimestre(i).getNombre().equals(s);
        }

        System.out.println("El numero de cuatrimestres es " + n);
        assert t.getNumeroCuatrimestres()==n;

        for(int i = n-1; i >= 0; --i) {
            t.removeCuatrimestre(t.getCuatrimestre(i).getNombre());
            System.out.println("Eliminado cuatrimestre con nombre " + s);
        }

        System.out.println("El numero de cuatrimestres es " + 0);
        assert t.getNumeroCuatrimestres()== 0;

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de PlanEstudios pasado\n" + ConsoleColors.RESET);
    }
}
