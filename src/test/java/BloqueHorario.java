import java.util.Random;

public class BloqueHorario {

    int horaInicio;
    DiaSemana ds;

    public BloqueHorario(DiaSemana ds, int hora) {
        this.horaInicio = hora;
        this.ds = ds;
    }

    public BloqueHorario(Random rnd, int horaMin, int horaMax) {
        this.horaInicio = 1;//rnd.nextInt(horaMax-horaMin)+horaMin;
        this.ds = DiaSemana.randomDia(rnd);
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public DiaSemana getDs() {
        return ds;
    }
}
