import java.util.ArrayList;

public class Sesion {

    private String nombre;

    public Sesion(String nombre, Asignatura asignatura, int duracion, int nGrupos) {
        this.nombre = nombre;
    }

    public Sesion(String nombre, Asignatura asignatura, int duracion, int nGrupos, ArrayList<Recurso> recursos) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public int getDuracion() {
        return 2;
    }

    public ArrayList<Recurso> getRecursos() {
        return new ArrayList<Recurso>();
    }
}
