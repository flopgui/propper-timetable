public class Aula {

    String s;

    public Aula(String s, int m) {
        this.s = s;
    }

    public String getNombre() {
        return s;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Aula)
            return this.s.equals(((Aula) other).s);
        return false;
    }

    public boolean hasRecurso(Recurso t) {
        return true;
    }
}
