import java.util.ArrayList;
import java.util.Scanner;
import java.util.Random;
import java.util.UUID;


public class SesionDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Sesion" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. Constructor 1: Crea una sesion sin recursos");
        System.out.println("   3. Constructor 2: Crea una sesion con recursos");
        System.out.println("   4. getNombre: Obtiene nombre de Sesion");
        System.out.println("   5. setNombre: Cambia el nombre de la Sesion");
        System.out.println("   6. getDuracion: Obtiene la duracion de la Sesion");
        System.out.println("   7. getnGRupos: Obtiene el numero de grupos maximo de una sesion");
        System.out.println("   8. getRecursos: Obtiene la lista de Recursos necesarios para una sesion");
        System.out.println("   9. addRecurso: Anade un recurso a una sesion");
        System.out.println("   10. getAsignatura: Obtiene la asignatura de la sesion");
        System.out.println("   11. setAsignatura: Cambia la asignatura de la sesion");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        Sesion S = new Sesion("sesion", new Asignatura("pp","p",10),2,3);
        Asignatura a;
        int n,n1,n2;
        String s,s1;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.println("nombre asignatura?");
                    s = sc.next();
                    System.out.println("sigla asignatura?");
                    s1 = sc.next();
                    System.out.println("capacidad grupos?");
                    n = sc.nextInt();
                    a = new Asignatura(s, s1, n);
                    System.out.println("nombre sesion");
                    s = sc.next();
                    System.out.println("duracion?");
                    n = sc.nextInt();
                    System.out.println("numero de grupos maximo?");
                    n1 = sc.nextInt();
                    S = new Sesion(s,a,n,n1);
                    System.out.println("Se ha creado una sesion con nombre " + s);
                    break;
                case 3:
                    System.out.println("nombre asignatura?");
                    s = sc.next();
                    System.out.println("sigla asignatura?");
                    s1 = sc.next();
                    System.out.println("capacidad grupos?");
                    n = sc.nextInt();
                    a = new Asignatura(s, s1, n);
                    System.out.println("nombre sesion");
                    s = sc.next();
                    System.out.println("duracion?");
                    n = sc.nextInt();
                    System.out.println("numero de grupos maximo?");
                    n1 = sc.nextInt();
                    System.out.println("numero de recursos necesitados?");
                    n2 = sc.nextInt();
                    ArrayList<Recurso> auxi = new ArrayList<>();
                    for (int j = 0; j < n2; ++j) {
                        System.out.println("nom recurs?");
                        s1 = sc.next();
                        auxi.add(new Recurso(s1));
                    }
                    S = new Sesion(s,a,n,n1,auxi);
                    System.out.println("Se ha creado una sesion con nombre " + s);
                    break;
                case 4:
                    System.out.println("El nombre de la sesion es " + S.getNombre());
                    break;
                case 5:
                    System.out.println("nombre?");
                    s = sc.next();
                    S.setNombre(s);
                    System.out.println("Se ha cambiado nombre sesion a " + s);
                    break;
                case 6:
                    System.out.println("Duracion de la sesion es " + S.getDuracion());
                    break;
                case 7:
                    System.out.println("Numero de grupos maximo de la sesion es " + S.getnGrupos());
                    break;
                case 8:
                    ArrayList<Recurso> ar = S.getRecursos();
                    System.out.println("Los recursos necesarios de la sesion son: ");
                    for (Recurso anAr : ar) System.out.println(anAr.getNombre());
                    break;
                case 9:
                    System.out.println("recurso nombre?");
                    s = sc.next();
                    S.addRecurso(new Recurso(s));
                    System.out.println("Se ha anadido el recurso " + s);
                    break;
                case 10:
                    System.out.println("Asignatura de la sesion es " + S.getAsignatura().getNombre());
                    break;
                case 11:
                    System.out.println("nombre asignatura?");
                    s = sc.next();
                    System.out.println("siglas asignatura?");
                    s1 = sc.next();
                    System.out.println("Capacidad grupo?");
                    n = sc.nextInt();
                    S.setAsignatura(new Asignatura(s,s1,n));
            }
        } while (test > 0 && test < 12);
    }


    private static void testSuite() {

        Random r = new Random();
        int n,n1,n2;
        String s,s1, s2;
        s2 = UUID.randomUUID().toString(); //nombre asignatura
        s1 = UUID.randomUUID().toString(); //sigla asignatura
        n = r.nextInt(); //capacidad grupos
        Asignatura aux = new Asignatura(s2, s1, n);
        s = UUID.randomUUID().toString(); //nombre sesion
        n1 = r.nextInt(30) + 1; //ngrupos
        n2 = r.nextInt(3) + 1; //nrecursos

        Sesion S = new Sesion(s, aux, n1, n2);
        System.out.println("Se ha creado sesion con nombre " + s +"de la asignatura " + s2);
        assert S.getNombre().equals(s);
        assert S.getAsignatura().getSiglas().equals(s1);
        assert S.getnGrupos() == n2;
        assert S.getDuracion() == n1;

        s2 = UUID.randomUUID().toString();
        System.out.println("Se ha cambiado el nombre de la sesion a " + s2);
        S.setNombre(s2);
        assert S.getNombre().equals(s2);

        s2 = UUID.randomUUID().toString();
        s1 = UUID.randomUUID().toString(); //sigla asignatura
        n = r.nextInt(); //capacidad grupos
        System.out.println("Se ha cambiado la asignatura a "+ s2);
        S.setAsignatura(new Asignatura(s2,s1,n));

        s2 = UUID.randomUUID().toString(); //nombre asignatura
        s1 = UUID.randomUUID().toString(); //sigla asignatura
        n = r.nextInt(); //capacidad grupos
        aux = new Asignatura(s2, s1, n);
        s = UUID.randomUUID().toString(); //nombre sesion
        n = r.nextInt(2) + 1; //duracion
        n1 = r.nextInt(30) + 1; //ngrupos
        n2 = r.nextInt(3); //nrecursos
        ArrayList<Recurso> auxi = new ArrayList<>();
        for (int j = 0; j < n2; ++j) {
            s1 = UUID.randomUUID().toString(); //nomrecurs
            auxi.add(new Recurso(s1));
        }
        Sesion t = new Sesion(s, aux, n, n1, auxi);
        System.out.println("Se ha creado sesion con nombre " + s +"de la asignatura " + s2 + " con recursos");
        System.out.println("Los recursos necesarios de la sesion son: ");
        for (Recurso anAr : auxi) System.out.println(anAr.getNombre());

        assert t.getRecursos() == auxi;

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de PlanEstudios pasado\n" + ConsoleColors.RESET);
    }
}
