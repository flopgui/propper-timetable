public class Asignatura {

    private String b;

    public Asignatura(String a, String b, int c) {
        this.b = b;
    }

    public String getSiglas() {
        return b;
    }

    public String getNombre() {
        return "Stub";
    }
    
    @Override
    public boolean equals(Object o) {
    	if (o instanceof Asignatura) {
    		return b.equals(((Asignatura) o).b);
    	}
    	return false;
    }
}
