import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;
import java.util.Random;

public class HoraClaseDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase HoraClase" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. getDuracion: Obtiene la duracion de la Horaclase");
        System.out.println("   3. getSesion: Obtiene la sesion de la horaclase");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        HoraClase HC = new HoraClase(new Sesion("sesion prueba",new Asignatura("prueba","pr",20),20,42, new ArrayList<>()));
        int n;
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.println("La duracion de la hora de clase es " + HC.getDuracion());
                    break;
                case 3:
                    System.out.println("El nombre de la sesion de la hora de clase es " + HC.getSesion().getNombre());
                    break;
            }
        } while (test > 0 && test < 3);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing HoraClase" + ConsoleColors.RESET);

        String s = UUID.randomUUID().toString();
        Random r = new Random();
        int n = r.nextInt(21)+1;

        HoraClase HC = new HoraClase(new Sesion(s,new Asignatura("prueba","pr",20),n,42, new ArrayList<>()));

        System.out.println("creado nueva HoraClase de sesion " + HC.getSesion().getNombre() + " y duracion " + HC.getDuracion());
        assert HC.getSesion().getNombre().equals(s);
        assert HC.getDuracion()==n;
         

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de HoraClase pasado\n" + ConsoleColors.RESET);
    }
}
