import java.util.ArrayList;

public class CtrlDominio {

    private static CtrlDominio instance;

    public static CtrlDominio getCtrlDominio() {
        if (instance == null)
            instance = new CtrlDominio();
        return instance;
    }

    private CtrlDominio() {

    }

    public Aula randomAula() {
        return new Aula("S05", 45);
    }

    public UnidadDocente getUnidadDocente() {
        return new UnidadDocente("a", 1, 24);
    }

    public ArrayList<Restriccion> getRestriccionesAlgoritmo() {
        return new ArrayList<Restriccion>();
    }
}
