public class Cuatrimestre {

    private String s;

    public Cuatrimestre(String s) {
        this.s = s;
    }

    public String getNombre() {
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Cuatrimestre)
            return ((Cuatrimestre) o).s.equals(s);
        return false;
    }
}
