import java.util.Scanner;
import java.util.UUID;

public class RecursoDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Recurso" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. setNombre: Cambia el nombre del recurso");
        System.out.println("   3. getNombre: Obtiene el nombre del recurso");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        Recurso r = new Recurso("Recurso Recursivo");
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.print("nombre? ");
                    s = sc.next();
                    r.setNombre(s);
                    System.out.format("Cambiado el nombre a %s\n",s);
                    break;
                case 3:
                    s = r.getNombre();
                    System.out.format("El nombre es %s\n",s);
                    break;
            }
        } while (test > 0 && test < 4);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing Asignatura" + ConsoleColors.RESET);

        String s = UUID.randomUUID().toString();
        Recurso r = new Recurso(s);
        System.out.format("Creadao recurso de nombre %s\n", s);

        assert r.getNombre().equals(s);
        System.out.format("El nombre del recurso es %s\n", r.getNombre());
        s = UUID.randomUUID().toString();
        r.setNombre(s);
        System.out.format("Cambiado el nombre a %s\n", s);
        assert r.getNombre().equals(s);
        System.out.format("El nombre del recurso es %s\n", r.getNombre());

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Asignatura pasado\n" + ConsoleColors.RESET);
    }
}
