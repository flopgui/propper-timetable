import java.util.Scanner;
import java.util.UUID;

public class TitulacionDriver {
	
	public static void main(String args[]) {
		System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Titulacion" + ConsoleColors.RESET);
		System.out.println("   1. Ejecutar bateria de tests");
		System.out.println("   2. addAsignatura: Anade una asignatura");
		System.out.println("   3. getNumeroAsignatura: Obtiene el numero de asignaturas");
		System.out.println("   4. getAsignatura(int): Obtiene la asignatura en posicion i");
		System.out.println("   5. getAsignatura(String): Obtiene la asignatura con unas siglas determinadas");
		System.out.println("   6. removeAsignatura: Elimina la asignatura con unas siglas determinadas");
		System.out.println("   7. addPlanEstudios: Anade una asignatura");
		System.out.println("   8. getNumeroPlanEstudios: Obtiene el numero de planes de estudioss");
		System.out.println("   9. getPlanEstudios(int): Obtiene el plan de estudios en posicion i");
		System.out.println("  10. getPlanEstudios(String): Obtiene el plan de estudios con un nombre determinado");
		System.out.println("  11. removePlanEstudios: Elimina el plan de estudios con un nombre determinado");
		System.out.println("  12. getNombre: Obtiene el nombre de la asignatura");
		System.out.println("  13. setNombre: Cambia el nombre de la asignatura");
		System.out.println("  Cualquier otra tecla para salir");
		
		Scanner sc = new Scanner(System.in);
		
		int test;
		Titulacion t = new Titulacion("Test");
		Asignatura a;
		int n;
		String s;
		boolean b;
		PlanEstudios p;
		
		do {
			test = sc.nextInt();
			switch (test) {
			case 1:
				testSuite();
				break;
			case 2:
				System.out.print("Siglas? ");
				s = sc.next();
				a = new Asignatura(s,s,0);
				if (t.addAsignatura(a))
					System.out.println("Anadida asignatura");
				else
					System.out.println("Una asignatura con ese nombre ya exste");
				break;
			case 3:
				n = t.getNumeroAsignatura();
				System.out.format("Hay %d asignaturas\n", n);
				break;
			case 4:
				System.out.print("i? ");
				n = sc.nextInt();
				a = t.getAsignatura(n);
				if (a == null) System.out.println("i es negativo o mayor que el numero de asignaturas");
				else System.out.format("La asignatura %d tiene siglas %s\n",n,a.getSiglas());
				break;
			case 5:
				System.out.print("s? ");
				a = t.getAsignatura(sc.next());
				if (a == null) System.out.println("Asignatura no existe");
				else System.out.format("Encontrada asignatura %s\n",a.getSiglas());
				break;
			case 6:
				System.out.print("s? ");
				b = t.removeAsignatura(sc.next());
				if (b) System.out.println("Asignatura eliminada");
				else System.out.println("Asignatura no existe");
				break;
			case 7:
				System.out.print("Nombre? ");
				p = new PlanEstudios(sc.next());
				t.addPlanEstudios(p);
				System.out.println("Anadido plan de estudios");
				break;
			case 8:
				n = t.getNumeroPlanEstudios();
				System.out.format("Hay %d planes de estudios\n", n);
				break;
			case 9:
				System.out.print("i? ");
				n = sc.nextInt();
				p = t.getPlanEstudios(n);
				if (p == null) System.out.println("i es negativo o mayor que el numero de planes de estudios");
				else System.out.format("El plan de estudios %d tiene nombre %s\n",n,p.getNombre());
				break;
			case 10:
				System.out.print("s? ");
				p = t.getPlanEstudios(sc.next());
				if (p == null) System.out.println("Plan de estudios no existe");
				else System.out.format("Encontrado plan de estudios %s\n",p.getNombre());
				break;
			case 11:
				System.out.print("s? ");
				b = t.removePlanEstudios(sc.next());
				if (b) System.out.println("Plan de estudios eliminado");
				else System.out.println("Plan de estudios no existe");
				break;
			case 12:
				System.out.format("La titulacion tiene nombre %s\n", t.getNombre());
				break;
			case 13:
				System.out.print("Nombre? ");
				t.setNombre(sc.next());
				System.out.println("Nombre cambiado");
				break;
			}
		} while (test > 0 && test < 14);
	}

	
	private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing Titulacion" + ConsoleColors.RESET);

		String s = UUID.randomUUID().toString();
        Titulacion t = new Titulacion(s);
		System.out.format("Creada titulacion de nombre %s\n", s);

        assert t.getNombre().equals(s);
		System.out.format("El nombre de la titulacion es %s\n", t.getNombre());
        s = UUID.randomUUID().toString();
        t.setNombre(s);
		System.out.format("Cambiado el nombre a %s\n", s);
        assert t.getNombre().equals(s);
		System.out.format("El nombre de la titulacion es %s\n\n", t.getNombre());

        PlanEstudios p = new PlanEstudios("a");
        assert !t.removePlanEstudios("a");
		System.out.format("Comprobado que no se puede eliminar un PlanEstudios que no existe.\n");
        assert t.getNumeroPlanEstudios() == 0;
		System.out.format("Comprobado que hay 0 planes de estudios.\n");
        assert t.addPlanEstudios(p);
		System.out.format("Anadido plan de estudios.\n");
        assert !t.addPlanEstudios(p);
		System.out.format("Comprobado que no se puede volver a anadir el mismo plan de estudios.\n");
        assert t.getNumeroPlanEstudios() == 1;
		System.out.format("Comprobado que ahora hay 1 plan de estudios.\n");
        assert t.getPlanEstudios(0).equals(p);
		System.out.format("Comprobado que el primer plan de estudios es el que se ha anadido.\n");
        assert t.removePlanEstudios("a");
		System.out.format("Eliminado el PlanEstudios.\n\n");

        Asignatura a = new Asignatura(s, s, 0);
        assert !t.removeAsignatura("a");
		System.out.format("Comprobado que no se puede eliminar una Asignatura que no existe.\n");
		assert t.getNumeroAsignatura() == 0;
		System.out.format("Comprobado que hay 0 asignaturas.\n");
		assert t.addAsignatura(a);
		System.out.format("Anadida Asignatura.\n");
		assert !t.addAsignatura(a);
		System.out.format("Comprobado que no se puede volver a anadir la mismo Asignatura.\n");
		assert t.getNumeroAsignatura() == 1;
		System.out.format("Comprobado que ahora hay 1 Asignatura.\n");
		assert t.getAsignatura(0).equals(a);
		System.out.format("Comprobado que la primera Asignatura es la que se ha anadido.\n");
		assert t.removeAsignatura(s);
		System.out.format("Eliminado la Asignatura.\n\n");

		System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Titulacion pasado\n" + ConsoleColors.RESET);
	}
}
