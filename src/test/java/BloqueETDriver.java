import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class BloqueETDriver {

    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase BloqueET" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. setAula: Cambia el aula");
        System.out.println("   3. getAula: Obtiene el aula");
        System.out.println("   4. setBloqueHorario: Cambia el bloque horario");
        System.out.println("   5. getBloqueHorario: Obtiene el bloque horario");
        System.out.println("   6. solapa: Mira si dos bloques espacio-temporales se solapan");
        System.out.println("   7. constructor 1: Crea un bloque espacio-temporal el bloque horario, el aula y la duracion");
        System.out.println("   8. constructor 2: Crea un bloque espacio-temporal aleatorio dadas la hora minima, la maxima, el aula y la duracion");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        BloqueET b = new BloqueET(new Aula("A5001",42), new BloqueHorario(DiaSemana.LUNES,12),1);
        int n, n2, d, d2;
        String s, s2;
        Random rnd = new Random();

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.print("aula? ");
                    s = sc.next();
                    b.setAula(new Aula(s,42));
                    System.out.format("Cambiado el aula a %s\n", s);
                    break;
                case 3:
                    s = b.getAula().getNombre();
                    System.out.format("El aula es %s\n", s);
                    break;
                case 4:
                    System.out.print("hora del bloque horario?\n");
                    n = sc.nextInt();
                    b.setBloqueHorario(new BloqueHorario(DiaSemana.LUNES,n));
                    System.out.format("Cambiado el bloque horario a lunes a las %d\n", n);
                    break;
                case 5:
                    System.out.format("El bloque horario es %s a las %d\n", b.getBloqueHorario().getDs(), b.getBloqueHorario().getHoraInicio());
                    break;
                case 6:
                    System.out.print("aula 1?\n");
                    s = sc.next();
                    System.out.print("hora 1?\n");
                    n = sc.nextInt();
                    System.out.print("duracion 1?\n");
                    d = sc.nextInt();
                    System.out.print("aula 2?\n");
                    s2 = sc.next();
                    System.out.print("hora 2?\n");
                    n2 = sc.nextInt();
                    System.out.print("duracion 2?\n");
                    d2 = sc.nextInt();
                    BloqueET b1 = new BloqueET(new Aula(s,42), new BloqueHorario(DiaSemana.LUNES,n),d);
                    BloqueET b2 = new BloqueET(new Aula(s2,42), new BloqueHorario(DiaSemana.LUNES,n2),d2);
                    if (b1.solapa(b2)) {
                        System.out.println("Solapanse");
                    } else {
                        System.out.println("No se solapan");
                    }
                case 7:
                    System.out.println("dia semana?");
                    s = sc.next();
                    System.out.print("hora inicio?\n");
                    n = sc.nextInt();
                    BloqueHorario aux = new BloqueHorario(DiaSemana.fromString(s),n);
                    System.out.println("Aula?");
                    s = sc.next();
                    System.out.println("duracion?");
                    n = sc.nextInt();
                    BloqueET b3 = new BloqueET(new Aula(s,42), aux, n);
                    System.out.println("Nuevo bloqueET creado con dia semana " + b3.getBloqueHorario().getDs().toString() +
                            ", hora inicio " + b3.getBloqueHorario().getHoraInicio() + "y en el aula " + b3.getAula().getNombre());
                case 8:
                    System.out.println("hora minima?");
                    n = sc.nextInt();
                    System.out.println("hora maxima?");
                    n2 = sc.nextInt();
                    System.out.println("duracion?");
                    d = sc.nextInt();
                    BloqueET b4 = new BloqueET(rnd, n, n2, d);
                    System.out.println("Nuevo bloqueET creado con dia semana " + b4.getBloqueHorario().getDs().toString() +
                            ", hora inicio " + b4.getBloqueHorario().getHoraInicio() + "y en el aula " + b4.getAula().getNombre());

            }
        } while (test > 0 && test < 9);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing BloqueET" + ConsoleColors.RESET);

        Random r = new Random();
        int n = r.nextInt();
        BloqueHorario bh = new BloqueHorario(r, n, n+1);
        String s = UUID.randomUUID().toString();
        int n2 = r.nextInt(2+1) + 1;
        BloqueET Be = new BloqueET(new Aula(s,40),bh, n2);
        System.out.println("Creado nuevo bloqueET dia " + Be.getBloqueHorario().getDs().toString() + " a la hora " + Be.getBloqueHorario().getHoraInicio()
                + " en la aula " + Be.getAula().getNombre());
        assert Be.getBloqueHorario().getDs() == bh.getDs();
        assert Be.getBloqueHorario().getHoraInicio()==bh.getHoraInicio();
        assert Be.getAula().getNombre().equals(s);
        s = UUID.randomUUID().toString();

        System.out.println("Cambiando aula bloque ET a aula " + s);
        Be.setAula(new Aula(s,50));
        System.out.println("Aula nueva nombre "+ Be.getAula().getNombre());
        assert Be.getAula().getNombre().equals(s);

        System.out.println("Cambiando lunes a las 8 nuevo bloque horario");
        Be.setBloqueHorario(new BloqueHorario(DiaSemana.LUNES,8));
        System.out.println("Actualizacion BloqueET, nuevo dia " + Be.getBloqueHorario().getDs().toString() + " nueva hora inicio " +
                Be.getBloqueHorario().getHoraInicio());
        assert Be.getBloqueHorario().getDs()== DiaSemana.LUNES;
        assert Be.getBloqueHorario().getHoraInicio()==8;

        BloqueET Be2 = new BloqueET(new Aula(s,50),new BloqueHorario(DiaSemana.LUNES,8),1);
        System.out.println("Creado nuevo bloqueET dia " + Be2.getBloqueHorario().getDs().toString() + " a la hora " + Be2.getBloqueHorario().getHoraInicio()
                + " en la aula " + Be2.getAula().getNombre());
        assert Be2.getBloqueHorario().getDs()== DiaSemana.LUNES;
        assert Be2.getBloqueHorario().getHoraInicio()==8;
        assert Be2.getAula().getNombre().equals(s);

        System.out.print("Comprobando solapamiento entre los dos bloques : ... ");
        if(Be.solapa(Be2)) System.out.println("SOLAPA");
        else System.out.println(" NO SOLAPA");
        assert Be.solapa(Be2);

        Be2.setBloqueHorario(new BloqueHorario(DiaSemana.MARTES,10));
        System.out.println("Actualizacion BloqueET2, nuevo dia " + Be2.getBloqueHorario().getDs().toString() + " nueva hora inicio " +
                Be2.getBloqueHorario().getHoraInicio());

        System.out.print("Comprobando solapamiento entre los dos bloques : ... ");
        if(Be.solapa(Be2)) System.out.println("SOLAPA");
        else System.out.println(" NO SOLAPA");
        assert !Be.solapa(Be2);

        n = r.nextInt(23)+1;
        BloqueET Be3 = new BloqueET(r, n, 24, 1);
        System.out.println("Creado nuevo bloqueET constructor 2 dia " + Be.getBloqueHorario().getDs().toString() + " a la hora " + Be.getBloqueHorario().getHoraInicio()
                + " en la aula " + Be.getAula().getNombre());
        assert (Be3.getBloqueHorario().getHoraInicio() >= 1 && Be3.getBloqueHorario().getHoraInicio()<=24);


        System.out.println(ConsoleColors.GREEN_BRIGHT + "\nTest de BloqueET pasado\n" + ConsoleColors.RESET);
    }

}