import java.util.Random;
import java.util.Scanner;

public class BloqueHorarioDriver {

    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase BloqueHorario" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. setDs: Cambia el dia de la semana");
        System.out.println("   3. getDs: Obtiene el dia de la semana");
        System.out.println("   4. setHoraInicio: Cambia la hora de inicio");
        System.out.println("   5. getHoraInicio: Obtiene la hora de inicio");
        System.out.println("   6. constructor 1: Crea un bloque horario dados el dia y la hora");
        System.out.println("   7. constructor 2: Crea un bloque horario aleatorio dadas la hora minima y la maxima");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        BloqueHorario b = new BloqueHorario(DiaSemana.VIERNES, 14);
        int n, n2;
        String s;
        Random rnd = new Random();

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.print("dia (LUNES, MARTES, MIERCOLES, JUEVES o VIERNES)? ");
                    s = sc.next();
                    b.setDs(DiaSemana.fromString(s));
                    System.out.format("Cambiado el dia de la semana a %s\n", s);
                    break;
                case 3:
                    s = b.getDs().toString();
                    System.out.format("El dia de la semana es %s\n", s);
                    break;
                case 4:
                    System.out.print("hora inicio? ");
                    n = sc.nextInt();
                    b.setHoraInicio(n);
                    System.out.format("Cambiada la hora de inicio a %d\n", n);
                    break;
                case 5:
                    n = b.getHoraInicio();
                    System.out.format("La hora de inicio es %d\n", n);
                    break;
                case 6:
                    System.out.print("dia (LUNES, MARTES, MIERCOLES, JUEVES o VIERNES)? ");
                    s = sc.next();
                    System.out.print("hora inicio? ");
                    n = sc.nextInt();
                    b = new BloqueHorario(DiaSemana.fromString(s), n);
                    System.out.format("El bloque horario es %s a las %d\n", s, n);
                case 7:
                    System.out.print("hora minima? ");
                    n = sc.nextInt();
                    System.out.print("hora maxima? ");
                    n2 = sc.nextInt();
                    b = new BloqueHorario(rnd, n, n2);
                    System.out.format("El bloque horario es %s a las %d\n", b.getDs().toString(), b.getHoraInicio());
            }
        } while (test > 0 && test < 8);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing BloqueHorario" + ConsoleColors.RESET);

        Random rnd = new Random();

        int n = rnd.nextInt(24);
        DiaSemana d = DiaSemana.randomDia(rnd);
        BloqueHorario b = new BloqueHorario(d, n);
        System.out.format("Creado bloque horario, el %s a las %d", d.toString(), n);

        assert b.getDs() == d;
        System.out.format("El dia de la semana es %s\n", b.getDs().toString());
        d = DiaSemana.randomDia(rnd);
        b.setDs(d);
        System.out.format("Cambiado el dia de la semana a %s\n", d.toString());
        assert b.getDs() == d;
        System.out.format("El dia de la semana es %s\n", b.getDs().toString());

        assert b.getHoraInicio() == n;
        System.out.format("La hora de inicio es %d\n", b.getHoraInicio());
        n = rnd.nextInt(24);
        b.setHoraInicio(n);
        System.out.format("Cambiada la hora de inicio a %d\n", n);
        assert b.getHoraInicio() == n;
        System.out.format("La hora de inicio es %d\n\n", b.getHoraInicio());

        n = rnd.nextInt(7) + 6;
        int n2 = rnd.nextInt(12) + 12;
        System.out.format("Constructor aleatorio: creando 10 bloques horarios entre las %d y las %d:\n", n, n2);
        for (int i = 0; i < 10; ++i) {
            b = new BloqueHorario(rnd, n, n2);
            System.out.format("Creado bloque horario, el %s a las %d\n", b.getDs().toString(), b.getHoraInicio());
        }

        System.out.println(ConsoleColors.GREEN_BRIGHT + "\nTest de BloqueHorario pasado\n" + ConsoleColors.RESET);
    }

}