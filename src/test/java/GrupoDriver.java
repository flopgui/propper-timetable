import java.util.Scanner;
import java.util.UUID;

public class GrupoDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Grupo" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. setNombre: Cambia el nombre del grupo");
        System.out.println("   3. getNombre: Obtiene el nombre del grupo");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        Grupo g = new Grupo("grupo prueba");
        int test;
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.print("nombre? ");
                    s = sc.next();
                    g.setNombre(s);
                    System.out.format("Cambiado el nombre a %s\n",s);
                    break;
                case 3:
                    s = g.getNombre();
                    System.out.format("El nombre es %s\n",s);
                    break;
            }
        } while (test > 0 && test < 4);
    }

    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing Grupo" + ConsoleColors.RESET);

        String s = UUID.randomUUID().toString();
        String t = UUID.randomUUID().toString();
        Grupo g = new Grupo(s);
        System.out.println("Creado grupo de nombre " + s);
        assert g.getNombre().equals(s);

        System.out.format("El nombre del grupo es %s\n", g.getNombre());
        g.setNombre(t);
        System.out.format("Cambiado el nombre a %s\n", t);
        assert g.getNombre().equals(t);
        System.out.format("El nombre del grupo es %s\n", g.getNombre());


        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Grupo pasado\n" + ConsoleColors.RESET);
    }
}
