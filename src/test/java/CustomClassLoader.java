import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.commons.RemappingClassAdapter;

/**
 * ClassLoader para facilitar el testeo de clases con Drivers y Stubs.
 * Asume que los Drivers tienen la forma {@code ClaseATestearDriver} y que los Stubs tienen la forma {@code OtraClaseClaseATestearStub}.
 */
public class CustomClassLoader extends ClassLoader {

    /**
     * Clase que se está testando.
     */
    private String mainClass;

    /**
     * El padre se pasa para poder pedir que cargue las clases que no están en el paquete
     * {@code prop.fib.upc.edu}.
     * @param parent El ClassLoader padre (normalmente {@code getClass().getClassLoader()}).
     */
    public CustomClassLoader(ClassLoader parent) {
        super(parent);
        mainClass = "";
    }

    /**
     * Carga el Stub asociado a la clase pedida.
     * @param name Clase a cargar.
     * @return El Stub de la clase a cargar.
     * @throws ClassNotFoundException Se lanza si el Stub no existe.
     */
    private Class<?> getStub(String name) throws ClassNotFoundException {

        String file = (name + "Stub").replace('.', File.separatorChar) + ".class";
        byte[] b = null;

        try {
            b = loadClassData(file);
            b = rewriteClassName(b);

            Class<?> c = defineClass(name, b, 0, b.length);
            resolveClass(c);
            return c;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (NullPointerException e) {
            System.err.println(ConsoleColors.YELLOW_BOLD_BRIGHT + "[Warning]: No Stub for class " + name);
            System.err.println("[Warning]: Loading default class" + ConsoleColors.RESET);
            return getClass(name);
        }
    }

    private Class getClass(String name) throws ClassNotFoundException {

        // Uncomment when working in the deploy environment.
//        String tmp[] = name.split("\\.");
//        name = tmp[tmp.length - 1] + name;

        String file = name.replace('.', File.separatorChar) + ".class";
        byte[] b = null;
        try {
            b = loadClassData(file);

            Class c = defineClass(name, b, 0, b.length);
            resolveClass(c);
            return c;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Todas las peticiones de cargar una clase pasan por este método.
     * Si la clase es del paquete {@code prop.fib.upc.edu}, la interceptará
     * y cargará o bien un Stub o bien la clase en cuestión.
     * @param name El nombre completo de la clase.
     * @return La clase deseada.
     * @throws ClassNotFoundException Se lanza cuando la clase no se encuentra.
     */
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException {

        // System.out.println("loading class '" + name + "'");

        // TODO solve classes that contain $ in the name. (Not important)
        if (name.startsWith("prop.fib.upc.edu.") && !name.contains("$")) {

            if (mainClass.isEmpty()) {
                String t[] = name.split("\\.");
                mainClass = t[t.length - 1];
                mainClass = mainClass.substring(0, mainClass.length() - 6);
                // System.out.println("MainClass: " + mainClass);

                getClass(name);
            } else if (name.endsWith(mainClass) || name.endsWith(mainClass + "Driver")) {
                getClass(name);
            } else {
                // System.out.println("Loading Stub for " + name);
                return getStub(name);
            }
        }
        return super.loadClass(name);
    }


    /**
     * Carga un fichero (.class) a un array de bytes.
     * El fichero tiene que poder ser accesible, en nuestro caso
     * tiene que estar alojado en el classpath.
     * @param name Nombre del fichero a cargar.
     * @return Array de bytes del fichero.
     * @throws IOException Se lanza cuando existe un error leyendo el archivo.
     */
    private byte[] loadClassData(String name) throws IOException {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(name);
        int size = stream.available();
        byte buff[] = new byte[size];
        DataInputStream in = new DataInputStream(stream);
        // Reading the binary data
        in.readFully(buff);
        in.close();
        return buff;
    }


    /**
     * Cambia el nombre del bytecode de una clase que acabe con "Stub" y lo borra.
     * @param bytecode El bytecode de la clase a cambiar el nombre.
     * @return Retorna el bytecode modificado
     */
    public byte[] rewriteClassName(byte[] bytecode)  {
        ClassReader classReader = new ClassReader(bytecode);
        ClassWriter classWriter = new ClassWriter(classReader, 0);

        Remapper remapper = new DefaultClassNameRemapper();
        classReader.accept(new RemappingClassAdapter(classWriter, remapper), ClassReader.EXPAND_FRAMES);

        return classWriter.toByteArray();
    }

    class DefaultClassNameRemapper extends Remapper {

        @Override
        public String map(String typeName) {
            boolean hasStub = typeName.endsWith("Stub");
            if (!hasStub) {
                return typeName;
            } else {
                return typeName.substring(0, typeName.length() - 4);
            }
        }

    }
}
