import java.util.ArrayList;
import java.util.Scanner;
import java.util.UUID;

public class AsignaturaDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Asignatura" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. setNombre: Cambia el nombre de la asignatura");
        System.out.println("   3. getNombre: Obtiene el nombre de la asignatura");
        System.out.println("   4. setSiglas: Cambia las siglas de la asignatura");
        System.out.println("   5. getSiglas: Obtiene las siglas de la asignatura");
        System.out.println("   6. setCapacidadGrupo: Cambia la capacidad de un grupo de la asignatura");
        System.out.println("   7. getCapacidadGrupo: Obtiene la capacidad de un grupo de la asignatura");
        System.out.println("   8. addSesion: Anade una sesion de la asignatura");
        System.out.println("   9. getSesion: Obtiene todas las sesiones de la asignatura");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        Asignatura a = new Asignatura("Asignatura de prueba","TEST",42);
        int n,n2;
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.print("nombre? ");
                    s = sc.next();
                    a.setNombre(s);
                    System.out.format("Cambiado el nombre a %s\n",s);
                    break;
                case 3:
                    s = a.getNombre();
                    System.out.format("El nombre es %s\n",s);
                    break;
                case 4:
                    System.out.print("siglas? ");
                    s = sc.next();
                    a.setSiglas(s);
                    System.out.format("Cambiadas las siglas a %s\n",s);
                    break;
                case 5:
                    s = a.getSiglas();
                    System.out.format("Las siglas son %s\n",s);
                    break;
                case 6:
                    System.out.println("capacidad? ");
                    n = sc.nextInt();
                    a.setCapacidadGrupo(n);
                    System.out.format("Cambiada la capacidad del grupo a %d\n",n);
                    break;
                case 7:
                    n = a.getCapacidadGrupo();
                    System.out.format("La capacidad del grupo es %d\n",n);
                    break;
                case 8:
                    System.out.println("nombre sesion?");
                    s = sc.next();
                    System.out.println("numero grupos maximo de la sesion?");
                    n = sc.nextInt();
                    System.out.println("duracion sesion?=");
                    n2 = sc.nextInt();
                    a.addSesion(new Sesion(s,a,n2,n));
                    System.out.println("Se ha anadido sesion con nombre "+ s);
                    break;
                case 9:
                    ArrayList<Sesion> aux = a.getSesiones();
                    System.out.print("la lista de sesiones que tiene la asignatura " +a.getNombre() + " es:");
                    for(Sesion ac : aux) System.out.println(ac.getNombre() + " con duracion " +ac.getDuracion());
                    break;
            }
        } while (test > 0 && test < 8);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing Asignatura" + ConsoleColors.RESET);

        String s = UUID.randomUUID().toString();
        String t = UUID.randomUUID().toString();
        Asignatura a = new Asignatura(s,t,42);
        System.out.format("Creada asignatura de nombre %s, siglas %s y capacidad 42\n", s,t);

        assert a.getNombre().equals(s);
        System.out.format("El nombre de la asignatura es %s\n", a.getNombre());
        s = UUID.randomUUID().toString();
        a.setNombre(s);
        System.out.format("Cambiado el nombre a %s\n", s);
        assert a.getNombre().equals(s);
        System.out.format("El nombre de la asignatura es %s\n", a.getNombre());

        assert a.getSiglas().equals(t);
        System.out.format("Las siglas de la asignatura son %s\n", a.getSiglas());
        t = UUID.randomUUID().toString();
        a.setSiglas(t);
        System.out.format("Cambiadas las siglas a %s\n", t);
        assert a.getSiglas().equals(t);
        System.out.format("Las siglas de la asignatura son %s\n", a.getSiglas());

        assert a.getCapacidadGrupo() == 42;
        System.out.format("El la capacidad del grupo es %d\n", a.getCapacidadGrupo());
        a.setCapacidadGrupo(11);
        System.out.format("Cambiada la capacidad del grupo a 11\n");
        assert a.getCapacidadGrupo() ==  11;
        System.out.format("El la capacidad del grupo es %d\n", a.getCapacidadGrupo());

        s=UUID.randomUUID().toString();
        a.addSesion(new Sesion(s,a,1,20));
        System.out.format("Se ha anadido sesion con nombre %s\n", s );

        ArrayList<Sesion> aux = a.getSesiones();
        for(Sesion ss: aux) System.out.println("la asignatura contiene " + ss.getNombre()  + " sesion");


        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Asignatura pasado\n" + ConsoleColors.RESET);
    }
}
