import java.util.function.Function;

public class Restriccion {

    private String nombre;
    private boolean active;

    public Restriccion(String nombre, String descripcion, double peso, Function<Horario, Double> fEval) {
        this.nombre = nombre;
        this.active = true;
    }

    public String getNombre() {
        return this.nombre;
    }

    public boolean isActive() {
        return this.active;
    }

    public double getPeso() {
        return 0.8;
    }

    public double evalua(Horario o) {
        return 1;
    }

    public boolean toggleActive() {
        this.active = !this.active;
        return this.active;
    }

}
