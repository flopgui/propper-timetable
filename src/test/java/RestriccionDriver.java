import java.util.*;
import java.util.function.Function;

public class RestriccionDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Restriccion" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. evalua: Obtiene la evaluacion de una restriccion");
        System.out.println("   3. getNombre: Obtiene el nombre de la restriccion");
        System.out.println("   4. getDescripcion: Obtiene la descripcion de la restriccion");
        System.out.println("   5. isActive: Obtiene si la restriccion esta activada o no");
        System.out.println("   6. toggleActive: Activa/Desactiva la restriccion");
        System.out.println("   7. getPeso: Obtiene el peso de la restriccion");
        System.out.println("   8. setPeso: Cambia el peso de la restriccion");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);
        Random random = new Random();

        int test;
        Restriccion r = new Restriccion("rest", "no tienes limites", random.nextDouble(), new Function<Horario, Double>() {
            @Override
            public Double apply(Horario horario) {
                return random.nextDouble();
            }
        });
        HoraClase[] ahc = new HoraClase[0];
        Horario h = new Horario(ahc, random);
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.format("La restriccion vale %f\n",r.evalua(h));
                    break;
                case 3:
                    s = r.getNombre();
                    System.out.format("El nombre es %s\n",s);
                    break;
                case 4:
                    s = r.getDescripcion();
                    System.out.format("La descripción es: %s\n", s);
                    break;
                case 5:
                    boolean b = r.isActive();
                    if (b)
                        System.out.println("La restricción está activa");
                    else
                        System.out.println("La restricción no está activa");
                    break;
                case 6:
                    boolean c = r.toggleActive();
                    if (c)
                        System.out.println("La restricción ahora está activada");
                    else
                        System.out.println("La restricción ahora está desactivada");
                    break;
                case 7:
                    double d = r.getPeso();
                    System.out.format("El peso es %d", d);
                    break;
                case 8:
                    System.out.println("Nuevo peso? ");
                    double d2 = sc.nextDouble();
                    r.setPeso(d2);
                    System.out.println("El nuevo peso es " + r.getPeso());
            }
        } while (test > 0 && test < 9);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing Restriccion" + ConsoleColors.RESET);

        String s = UUID.randomUUID().toString();
        String t = UUID.randomUUID().toString();
        Random random = new Random();
        double f = random.nextDouble();
        Restriccion r = new Restriccion(s, t, f, new Function<Horario, Double>() {
            @Override
            public Double apply(Horario horario) {
                return random.nextDouble();
            }
        });
        System.out.format("Creada restriccion de nombre %s, descripcion %s y peso %f\n", s,t,f);

        assert r.getNombre().equals(s);
        System.out.format("El nombre es es %s\n", r.getNombre());
        assert r.getDescripcion().equals(s);
        System.out.format("La descripcion es %s\n", r.getDescripcion());
        assert r.getPeso() == f;
        System.out.format("El peso es %f\n", r.getPeso());
        f = random.nextDouble();
        r.setPeso(f);
        System.out.format("Cambiado el peso a %f\n", f);
        assert r.getPeso() == f;
        System.out.format("El peso es %f\n", r.getPeso());

        assert r.isActive();
        System.out.format("La restriccion esta activa\n");
        r.toggleActive();
        System.out.format("Se ha cambiado el estado de la restriccion\n");
        assert !r.isActive();
        System.out.format("La restriccion esta inactiva\n");
        r.toggleActive();
        System.out.format("Se ha cambiado el estado de la restriccion\n");
        assert r.isActive();
        System.out.format("La restriccion esta activa\n");


        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Restriccion pasado\n" + ConsoleColors.RESET);
    }
}
