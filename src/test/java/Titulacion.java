public class Titulacion {

    public String s;

    public Titulacion(String s) {
        this.s = s;
    }

    public String getNombre() {
        return s;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Titulacion)
            return ((Titulacion) o).s.equals(s);
        return false;
    }
}
