import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

public class AulaDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Aula" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. setNombre: Cambia el nombre de la Aula");
        System.out.println("   3. getNombre: Obtiene el nombre de la Aula");
        System.out.println("   4. getCapacidad: Obtiene la capacidad de la Aula");
        System.out.println("   5. setCapacidad: Cambia la capacidad de la Aula");
        System.out.println("   6. getNumeroRecursos: Obtiene el numero de recursos de la Aula");
        System.out.println("   7. hasRecurso: Devuelve si tiene el recurso en la Aula");
        System.out.println("   8. getRecurso: Retorna el recurso i de la Aula");
        System.out.println("   9. addRecurso: Anade un recurso a la Aula");
        System.out.println("   10. removeRecurso: Elimina un recurso de la Aula");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        int aux;
        Aula a = new Aula("Aulaprova", 1);
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.print("nombre? ");
                    s = sc.next();
                    a.setNombre(s);
                    System.out.format("Cambiado el nombre a %s\n",s);
                    break;
                case 3:
                    s = a.getNombre();
                    System.out.format("El nombre es %s\n",s);
                    break;
                case 4:
                    aux = a.getCapacidad();
                    System.out.format("La capacidad es %d\n",aux);
                    break;
                case 5:
                    System.out.print("capacidad? ");
                    aux = sc.nextInt();
                    a.setCapacidad(aux);
                    System.out.format("Cambiado la capacidad a %d\n",aux);
                    break;
                case 6:
                    aux = a.getNumeroRecursos();
                    System.out.format("El nombre de recursos de la aula es %d\n",aux);
                    break;
                case 7:
                    System.out.print("nombre recurso? ");
                    s = sc.next();
                    if(a.hasRecurso(new Recurso(s))) System.out.format("El recurso que buscas existe en la Aula\n");
                    else System.out.format("no existe\n");
                    break;
                case 8:
                    try {
                        System.out.print("El número de recurso que quieres buscar\n");
                        aux = sc.nextInt();
                        System.out.format("El recurso que buscas es %s\n", a.getRecurso(aux).getNombre());
                    } catch (IndexOutOfBoundsException e) {
                        System.out.format("Indice superior al numero de recursos o negativo\n");
                    }
                    break;
                case 9:
                    System.out.print("recurso?\n");
                    s = sc.next();
                    if(a.addRecurso(new Recurso(s))) System.out.format("El recurso %s fue anadido",s);
                    else System.out.print("no se pudo anadir el recurso\n");
                    break;
                case 10:
                    System.out.print("recurso a eliminar?\n");
                    s = sc.next();
                    if(a.removeRecurso(s)) System.out.format("El recurso %s fue anadido",s);
                    else System.out.print("no se pudo eliminar el recurso\n");
                    break;
            }
        } while (test > 0 && test < 11);
    }


    private static void testSuite() {
        System.out.println(ConsoleColors.BLUE + "Testing Aula" + ConsoleColors.RESET);

        Random rn = new Random();

        int aux = rn.nextInt(50+1) + 1;

        String s = UUID.randomUUID().toString();
        Aula a = new Aula(s, aux);
        System.out.format("Creada Aula de nombre %s\n y capacidad %d", s,aux);

        assert a.getNombre().equals(s);
        assert a.getCapacidad()==aux;
        System.out.format("El nombre de la Aula es %s\n", a.getNombre());
        System.out.format("La capacitat de la Aula es %s\n", a.getCapacidad());
        s = UUID.randomUUID().toString();
        a.setNombre(s);
        System.out.format("Cambiado el nombre a %s\n", s);
        assert a.getNombre().equals(s);
        System.out.format("El nombre de la Aula es %s\n", a.getNombre());
        aux = rn.nextInt(50+1);
        a.setCapacidad(aux);
        System.out.format("Cambiado la capacidad a %d\n", aux);
        assert a.getCapacidad()==aux;
        System.out.format("numero de recursos de la Aula %d\n", a.getNumeroRecursos());
        assert a.getNumeroRecursos()==0;

        s = UUID.randomUUID().toString();
        assert a.addRecurso(new Recurso(s));
        System.out.format("Anadiendo el recurso %s\n", s);

        System.out.format("Comprovando si tiene recurso %s\n", s);

        if(a.hasRecurso(new Recurso(s))) System.out.println("Sí que esta\n");
        assert a.hasRecurso(new Recurso(s));

        s = UUID.randomUUID().toString();
        System.out.format("Comprovar que no tiene recurso %s\n",s);
        if(!a.hasRecurso(new Recurso(s))) System.out.println("no tiene el recurso " + s);
        assert !a.hasRecurso(new Recurso(s));


        a.addRecurso(new Recurso(s));
        int nr = a.getNumeroRecursos();
        System.out.format("Anadiendo el recurso %s\n", s);
        for(int i = 0; i < 5; ++i){
            s = UUID.randomUUID().toString();
            a.addRecurso(new Recurso(s));
            System.out.format("Anadiendo el recurso %s\n", s);
            assert a.getRecurso(i + nr).getNombre().equals(s);
        }

        s = a.getRecurso(3).getNombre();
        System.out.println("Eliminando el recurso " + s + " de la Aula");
        assert a.removeRecurso(s);
        System.out.println("El recurso " + s + " ha sido eliminado");
        System.out.println("Listado de recursos actuales");
        for(int i = 0; i < a.getNumeroRecursos(); ++i){
            System.out.println("el recurso " + i + " es " + a.getRecurso(i).getNombre());
        }

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Aula pasado\n" + ConsoleColors.RESET);
    }
}

