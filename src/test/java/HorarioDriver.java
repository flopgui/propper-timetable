import java.util.*;
import java.util.Map.Entry;

public class HorarioDriver {
    public static void main(String args[]) {
        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase Horario" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. Constructor 1: Crea horario a partir de conjunto de Horas clases, y bloques ET");
        System.out.println("   3. Constructor 2: Crea horario a partir de conjunto de Horas clases");
        System.out.println("   4. reproduce: crea un horario a partir de otro");
        System.out.println("   5. getFitness: obtiene el fitness del horario");
        System.out.println("   6. getAsignaciones: Obtiene las asignaciones del Horario");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        ArrayList<Recurso> ra = new ArrayList<>();
        HoraClase hc = new HoraClase(new Sesion("sesion1",new Asignatura("prova", "pr", 5), 1,1, ra));
        HoraClase[] HC = {hc};
        Random r = new Random();
        Horario h = new Horario(HC,r);
        Horario h_proba = new Horario(HC,r);
        int n, n1, n2, k;
        String s, s1;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.println("numero de Horas clases a anadir");
                    k = sc.nextInt();
                    HoraClase[] HCaux = new HoraClase[k];
                    for(int i = 0; i < k; ++i) {
                        System.out.println("nombre asignatura?");
                        s = sc.next();
                        System.out.println("sigla asignatura?");
                        s1 = sc.next();
                        System.out.println("capacidad grupos?");
                        n = sc.nextInt();
                        Asignatura aux = new Asignatura(s, s1, n);
                        System.out.println("nombre sesion");
                        s = sc.next();
                        System.out.println("duracion?");
                        n = sc.nextInt();
                        System.out.println("numero de grupos maximo?");
                        n1 = sc.nextInt();
                        System.out.println("numero de recursos necesitados?");
                        n2 = sc.nextInt();
                        ArrayList<Recurso> auxi = new ArrayList<>();
                        for (int j = 0; j < n2; ++j) {
                            System.out.println("nom recurs?");
                            s1 = sc.next();
                            auxi.add(new Recurso(s1));
                        }
                        HCaux[i] = new HoraClase(new Sesion(s, aux, n, n1, auxi));
                    }
                    System.out.println("numero de bloques horas disponibles");
                    k = sc.nextInt();
                    BloqueET[] BE = new BloqueET[k];
                    for(int i = 0; i < k; ++i) {
                        System.out.println("nombre aula?");
                        s = sc.next();
                        System.out.println("capacidad aula?");
                        n = sc.nextInt();
                        Aula aux = new Aula(s,n);
                        System.out.println("dia?");
                        s = sc.next();
                        System.out.println("hora?");
                        n1 = sc.nextInt();
                        System.out.println("duracion?");
                        n2 = sc.nextInt();
                        BE[i] = new BloqueET(aux, new BloqueHorario(DiaSemana.fromString(s),n1),n2);
                    }
                    h = new Horario(HCaux, BE);
                    System.out.println("Se ha creado Horario con las caracteristicas anadidas");
                    break;
                case 3:
                    System.out.println("numero de Horas clases a anadir");
                    k = sc.nextInt();
                    HoraClase[] HCaux2 = new HoraClase[k];
                    for(int i = 0; i < k; ++i) {
                        System.out.println("nombre asignatura?");
                        s = sc.next();
                        System.out.println("sigla asignatura?");
                        s1 = sc.next();
                        System.out.println("capacidad grupos?");
                        n = sc.nextInt();
                        Asignatura aux = new Asignatura(s, s1, n);
                        System.out.println("nombre sesion");
                        s = sc.next();
                        System.out.println("duracion?");
                        n = sc.nextInt();
                        System.out.println("numero de grupos maximo?");
                        n1 = sc.nextInt();
                        System.out.println("numero de recursos necesitados?");
                        n2 = sc.nextInt();
                        ArrayList<Recurso> auxi = new ArrayList<>();
                        for (int j = 0; j < n2; ++j) {
                            System.out.println("nom recurs?");
                            s1 = sc.next();
                            auxi.add(new Recurso(s1));
                        }
                        HCaux2[i] = new HoraClase(new Sesion(s, aux, n, n1, auxi));
                    }
                    h = new Horario(HCaux2,r);
                    System.out.println("Se ha creado Horario con las caracteristicas anadidas");
                    break;
                case 4:
                    double d = r.nextDouble();
                    h = h.reproduce(h_proba, r, d);
                    System.out.println("Se ha creado horario hijo");
                    break;
                case 5:
                    System.out.format("El fitness del horario es %f\n", h.getFitness());
                    break;
                case 6:
                    HashMap<HoraClase, BloqueET> auxtree = h.getAsignaciones();
                    System.out.println("Asignacions del horari obtingudes:");
                    for(Entry<HoraClase, BloqueET> entry: auxtree.entrySet()){
                        System.out.println(" Sesion nom " + entry.getKey().getSesion().getNombre() + " es fa " + entry.getValue().getBloqueHorario().getDs().toString() +
                                " a la hora " + entry.getValue().getBloqueHorario().getHoraInicio());
                    }
            }
        } while (test > 0 && test < 7);
    }


    private static void testSuite() {
        int n, k, n1, n2;
        String s, s1;
        Random r = new Random();
        System.out.println("numero de Horas clases a anadir");
        k = r.nextInt(10)+1;
        HoraClase[] HCaux = new HoraClase[k];
        for(int i = 0; i < k; ++i) {
            s = UUID.randomUUID().toString(); //nombre asignatura
            s1 = UUID.randomUUID().toString(); //sigla asignatura
            n = r.nextInt(); //capacidad grupos
            Asignatura aux = new Asignatura(s, s1, n);
            s = UUID.randomUUID().toString(); //nombre sesion
            n = r.nextInt(2)+1; //duracion
            n1 = r.nextInt(30)+1; //ngrupos
            n2 = r.nextInt(3); //nrecursos
            ArrayList<Recurso> auxi = new ArrayList<>();
            for (int j = 0; j < n2; ++j) {
                s1 = UUID.randomUUID().toString(); //nomrecurs
                auxi.add(new Recurso(s1));
            }
            HCaux[i] = new HoraClase(new Sesion(s, aux, n, n1, auxi));
        }
        BloqueET[] BE = new BloqueET[k];
        for(int i = 0; i < k; ++i) {
            n = r.nextInt(20)+8;
            BE[i] = new BloqueET(r, 1, 22, n);
        }
         Horario h = new Horario(HCaux, BE);
        System.out.println("Se ha creado Horario");


        k = r.nextInt(10)+1;
        HoraClase[] HCaux2 = new HoraClase[k];
        for(int i = 0; i < k; ++i) {
            s = UUID.randomUUID().toString(); //nombre asignatura
            s1 = UUID.randomUUID().toString(); //sigla asignatura
            n = r.nextInt(50); //capacidad grupos
            Asignatura aux = new Asignatura(s, s1, n);
            s = UUID.randomUUID().toString(); //nombre sesion
            n = r.nextInt(2)+1; //duracion
            n1 = r.nextInt(30)+1; //ngrupos
            n2 = r.nextInt(3); //nrecursos
            ArrayList<Recurso> auxi = new ArrayList<>();
            for (int j = 0; j < n2; ++j) {
                s1 = UUID.randomUUID().toString(); //nomrecurs
                auxi.add(new Recurso(s1));
            }
            HCaux2[i] = new HoraClase(new Sesion(s, aux, n, n1, auxi));
        }
        BloqueET[] BE2 = new BloqueET[k];
        for(int i = 0; i < k; ++i) {
            BE2[i] = new BloqueET(r,8,22, 1);
        }
        Horario h2 = new Horario(HCaux, BE);
        System.out.println("Se ha creado Horario2");

        HashMap<HoraClase, BloqueET> auxtree = h.getAsignaciones();
        System.out.println("Asignacions del horari obtenidas:");
        for(Entry<HoraClase, BloqueET> entry: auxtree.entrySet()){
            System.out.println(" Sesion nom " + entry.getKey().getSesion().getNombre() + " es fa " + entry.getValue().getBloqueHorario().getDs().toString() +
                    " a la hora " + entry.getValue().getBloqueHorario().getHoraInicio());
        }

        h2 = h.reproduce(h2, r, r.nextDouble());

        auxtree = h2.getAsignaciones();
        System.out.println("Asignacions del horari2 obtenidas:");
        for(Entry<HoraClase, BloqueET> entry: auxtree.entrySet()){
            System.out.println(" Sesion nom " + entry.getKey().getSesion().getNombre() + " es fa " + entry.getValue().getBloqueHorario().getDs().toString() +
                    " a la hora " + entry.getValue().getBloqueHorario().getHoraInicio());
        }

        double d = h2.getFitness();
        System.out.println("Fitness de horario2: " + d);

        d = h.getFitness();
        System.out.println("Fitness de horario: " + d);

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de Horario pasado\n" + ConsoleColors.RESET);
    }
}
