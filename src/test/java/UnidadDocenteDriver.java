import java.util.Random;
import java.util.Scanner;
import java.util.UUID;

/**
 * Unit test for simple App.
 */
public class UnidadDocenteDriver {

    public static void main(String args[]) {

        System.out.println(ConsoleColors.YELLOW_UNDERLINED + "Test de la clase UnidadDocente" + ConsoleColors.RESET);
        System.out.println("   1. Ejecutar bateria de tests");
        System.out.println("   2. addTitulacion: anade Titulacion a la UD");
        System.out.println("   3. getTitulacion: Obtiene las titulaciones que hay en la UD");
        System.out.println("   4. getNumeroTitulaciones: Obtiene el numero de titulaciones en la UD");
        System.out.println("   5. getTitulacion 1: Obtiene las titulacion i de la UD");
        System.out.println("   6. getTitulacion 2: Obtiene la titulacion por su nombre en la UD");
        System.out.println("   7. addAula: Anade aula a la UD");
        System.out.println("   8. getNumeroAulas: Obtiene el numero de Aulas de la UD");
        System.out.println("   9. getAula 1: Obtiene la aula i de la UD");
        System.out.println("   10. getAula 2: Obtiene la aula por su nombre en la UD");
        System.out.println("   11. getNombre: Obtiene el nombre de la UD");
        System.out.println("   12. setNombre: Cambia el nombre de la UD");
        System.out.println("   13. getHoraMin: Obtiene la hora minima de la UD");
        System.out.println("   14. setHoraMin: Cambia la hora minima de la UD");
        System.out.println("   15. getHoraMax: Obtiene la hora maxima de la UD");
        System.out.println("   16. setHoraMax: Cambia la hora maxima de la UD");
        System.out.println("   Cualquier otra tecla para salir");

        Scanner sc = new Scanner(System.in);

        int test;
        UnidadDocente UD = new UnidadDocente("FIB", 8, 21);
        int n;
        String s;

        do {
            test = sc.nextInt();
            switch (test) {
                case 1:
                    testSuite();
                    break;
                case 2:
                    System.out.print("nombre titulacion a añadir?\n ");
                    s = sc.next();
                    UD.addTitulacio(new Titulacion(s));
                    System.out.println("Se ha anadido titulacion " + s);
                    break;
                case 3:
                    String[] s1 = UD.getTitulaciones();
                    System.out.println("Las titulaciones que hay en la UD son : ");
                    for (String aS1 : s1) System.out.format("%s\n", aS1);
                    break;
                case 4:
                    System.out.format("El numero de Titulaciones que hay en la UD es %d", UD.getNumeroTitulaciones());
                    break;
                case 5:
                    try{
                        System.out.print("numero titulacion?\n ");
                        n = sc.nextInt();
                        System.out.format("La titulacion es %s\n", UD.getTitulacion(n).getNombre());
                    } catch(IndexOutOfBoundsException e){
                        System.out.format("Indice superior al numero de titulaciones o negativo\n");
                    }
                    break;
                case 6:
                    try{
                        System.out.print("nombre titulacion?\n ");
                        s = sc.next();
                        System.out.format("La titulacion es %s\n", UD.getTitulacion(s).getNombre());
                    } catch(IndexOutOfBoundsException e){
                        System.out.format("No existe la titulacion buscada\n");
                    }
                case 7:
                    System.out.println("nombre Aula?");
                    s = sc.next();
                    System.out.println("capacidad aula?");
                    n = sc.nextInt();
                    UD.addAula(new Aula(s,n));
                    System.out.println("Aula " + s + " con capacidad " + n + " anadida a la UD");
                case 8:
                    System.out.println("El numero de aulas de la UD es " + UD.getNumeroAulas());
                case 9:
                    try{
                        System.out.print("Indice aula?\n ");
                        n = sc.nextInt();
                        System.out.format("La aula es %s\n", UD.getAula(n).getNombre());
                    } catch(IndexOutOfBoundsException e){
                        System.out.format("Indice superior al numero de titulaciones o negativo\n");
                    }
                    break;
                case 10:
                    try{
                        System.out.print("nombre aula?\n ");
                        s = sc.next();
                        System.out.format("La aula es %s\n", UD.getAula(s).getNombre());
                    } catch(IndexOutOfBoundsException e){
                        System.out.format("No existe la aula buscada\n");
                    }
                case 11:
                    System.out.print("nombre? ");
                    s = sc.next();
                    UD.setNombre(s);
                    System.out.format("Cambiado el nombre a %s\n", s);
                    break;
                case 12:
                    System.out.format("El nombre es %s\n", UD.getNombre());
                    break;
                case 13:
                    System.out.println("Hora minima de la UD es " + UD.getHoraMin());
                    break;
                case 14:
                    System.out.println("hora minima? ");
                    n = sc.nextInt();
                    UD.setHoraMin(n);
                    System.out.format("Cambiado la hora minima a %d\n", n);
                    break;
                case 15:
                    System.out.println("Hora maxima de la UD es " + UD.getHoraMax());
                    break;
                case 16:
                    System.out.println("hora maxima? ");
                    n = sc.nextInt();
                    UD.setHoraMax(n);
                    System.out.format("Cambiado la hora maxima a %d\n", n);
                    break;
            }
        } while (test > 0 && test < 17);
    }

    private static void testSuite(){
        String s = UUID.randomUUID().toString();
        UnidadDocente ud = new UnidadDocente(s, 8, 21);
        System.out.println("Creada UD con nombre " + s);
        assert ud.getNombre().equals(s);
        Random r = new Random();
        int n = r.nextInt(10+1)+1;
        for(int i = 0; i <= n; ++i){
            s = UUID.randomUUID().toString();
            ud.addTitulacio(new Titulacion(s));
            System.out.println("Anadida Titulacion " + s);
            assert ud.getTitulacion(i).getNombre().equals(s);
            assert ud.getTitulacion(s).getNombre().equals(s);
        }

        System.out.println("Numero de titulaciones totales = " + n+1);
        assert ud.getNumeroTitulaciones()==n+1;

        n = r.nextInt(10+1)+1;
        int aux;
        for(int i=0; i <= n; ++i){
            s = UUID.randomUUID().toString();
            aux = r.nextInt(20+1)+20;
            ud.addAula(new Aula(s,aux));
            System.out.println("Anadida Aula " + s + " con capacidad " + aux);
            assert ud.getAula(i).getNombre().equals(s);
            assert ud.getAula(s).getNombre().equals(s);
        }
        System.out.println("Numero de aulas totales = " + n+1);
        assert ud.getNumeroAulas()==n+1;

        s = UUID.randomUUID().toString();
        ud.setNombre(s);
        assert ud.getNombre().equals(s);

        n = r.nextInt(8)+8;
        ud.setHoraMin(n);
        System.out.println("Hora minima de la UD cambiada a " + n);
        assert ud.getHoraMin() == n;

        n = n + 8;
        ud.setHoraMax(n);
        System.out.println("Hora maxima de la UD cambiada a " + n);
        assert ud.getHoraMax() == n;

        System.out.println(ConsoleColors.GREEN_BRIGHT + "Test de UnidadDocente pasado" + ConsoleColors.RESET);
    }

}
