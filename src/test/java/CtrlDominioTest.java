import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Random;
import java.util.UUID;

public class CtrlDominioTest {

    CtrlDominio d;

    @Before
    public void setUp() {
        d = new CtrlDominio("b", 8, 21);
    }

    @Test
    public void testUnidadDocente() {
        Random random = new Random();
        int r = random.nextInt(23)+1;
        int r2;
        do {
            r2 = random.nextInt(23)+1;
        } while (r >= r2);
        d.renameUnidadDocente("b");
        assertEquals(d.getUnidadDocente().getNombre(), "b");

        d.changeHorasLectivas(r, r2);
        assertEquals(d.getUnidadDocente().getHoraMin(), r);
        assertEquals(d.getUnidadDocente().getHoraMax(), r2);
    }

    @Test
    public void testAula() {
        Random rnd = new Random();
        for (int i = 0; i < 6; i++) {
            String s = UUID.randomUUID().toString();
            int r = rnd.nextInt(50)+1;
            d.addAula(s, r);
            assertEquals(d.getUnidadDocente().getNumeroAulas(), i+1);
            assertEquals(d.getUnidadDocente().getAula(s).getCapacidad(), r);
            assertEquals(d.getUnidadDocente().getAula(s).getNombre(), s);
        }

        String s = d.getUnidadDocente().getAula(0).getNombre();
        d.changeCapacidadAula(s, 99);
        assertEquals(d.getUnidadDocente().getAula(s).getCapacidad(), 99);

        d.deleteAula(s);
        assertEquals(d.getUnidadDocente().getNumeroAulas(), 5);
        assertNull(d.getUnidadDocente().getAula(s));

        s = d.getUnidadDocente().getAula(0).getNombre();
        d.renameAula(s, "a");
        assertEquals(d.getUnidadDocente().getAula(0).getNombre(), "a");

        assertEquals(d.getRecursos().length, 0);
        d.addRecursoAula("a", "recurso1");
        assertEquals(d.getRecursos().length, 1);
        assertEquals(d.getUnidadDocente().getAula("a").getRecurso(0).getNombre(), "recurso1");

        d.removeRecursoAula("a", "recurso1");
        assertEquals(d.getUnidadDocente().getAula("a").getNumeroRecursos(), 0);
    }

    @Test
    public void testTitulacion() {
        d.addTitulacion("tit1");
        assertEquals(d.getUnidadDocente().getNumeroTitulaciones(), 1);
        assertEquals(d.getUnidadDocente().getTitulacion(0).getNombre(), "tit1");
        assertNotNull(d.getUnidadDocente().getTitulacion("tit1"));

        d.renameTitulacion("tit1", "tit2");
        assertNotNull(d.getUnidadDocente().getTitulacion("tit2"));
        assertNull(d.getUnidadDocente().getTitulacion("tit1"));
        assertEquals(d.getUnidadDocente().getNumeroTitulaciones(), 1);
        assertEquals(d.getUnidadDocente().getTitulacion(0).getNombre(), "tit2");

        d.deleteTitulacion("tit2");
        assertEquals(d.getUnidadDocente().getNumeroTitulaciones(), 0);
        assertFalse(d.deleteTitulacion("o"));
    }

    @Test
    public void testAsignatura() {
        d.addTitulacion("tit1");
        d.addTitulacion("tit2");

        d.addAsignatura("tit1", "Bases de datos", "BD", 20);
        d.addAsignatura("tit2", "Bases de datos", "BD", 20);
        assertNotNull(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD"));
        assertEquals(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD").getCapacidadGrupo(), 20);
        assertEquals(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD").getNombre(), "Bases de datos");

        d.deleteAsignatura("tit1", "BD");
        assertNull(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD"));

        d.moveAsignatura("tit2", "tit1", "BD");
        assertNotNull(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD"));
        assertNull(d.getUnidadDocente().getTitulacion("tit2").getAsignatura("BD"));

        d.changeNombreAsignatura("tit1", "BD", "Bisual Data");
        assertEquals(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD").getNombre(), "Bisual Data");

        d.changeGruposAsignatura("tit1", "BD", 4);
        assertEquals(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD").getNumeroGrupos(), 4);

        d.changeSiglasAsignatura("tit1", "BD", "PROP");
        assertNotNull(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("PROP"));
        assertNull(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD"));
        assertEquals(d.getUnidadDocente().getTitulacion("tit1").getAsignatura("BD").getNombre(), "Bisual Data");

        d.addSesion("tit1", "PROP", "Sesion1teo", 1, 2);
        ArrayList<Sesion> s = d.getUnidadDocente().getTitulacion("tit1").getAsignatura("PROP").getSesiones();
        assertEquals(s.size(), 1);
        assertEquals(s.get(0).getNombre(), "Sesion1teo");
        assertEquals(s.get(0).getDuracion(), 1);
        assertEquals(s.get(0).getnGrupos(), 2);

        assertEquals(d.getRestricciones().length, 2);
        assertEquals(d.getRestriccionDescription("SOLAPAMIENTOS"), "No puede haber dos clases en la misma aula al mismo tiempo.");
    }
}
