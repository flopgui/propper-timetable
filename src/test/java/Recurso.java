public class Recurso {

    private String name;

    public Recurso(String name) {
        this.name = name;
    }

    public void setNombre(String name) {
        this.name = name;
    }

    public String getNombre() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Recurso)
            return ((Recurso) o).name.equals(name);
        return false;
    }
}
