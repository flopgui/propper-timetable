import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.event.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class VistaAsignatura extends JDialog implements ActionListener, TableModelListener {

    private CtrlPresentacion ctrl;
    private JTextField nombre, siglas, capacidadgrupo, numeroalumnos;
    private JButton newsesion, deletesesion, confirmar, cancelar, botoncolor;
    private JTable table;
    private JPanel fields, buttons, tablepanel;
    String titulacion, oldsiglas;

    public VistaAsignatura(CtrlPresentacion ctrl, String titulacion) {

        this.ctrl = ctrl;
        this.titulacion = titulacion;
        setPreferredSize(new Dimension(600,300));
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setLocationRelativeTo(null);
        setLayout(new GridBagLayout());
        setTitle(Lang.getString("crearasignatura"));
        setModal(true);

        fields = new JPanel();
        fields.setLayout(new GridLayout(0,2));
        fields.add(new JLabel(Lang.getString("siglas")));
        siglas = new JTextField(8);
        fields.add(siglas);
        fields.add(new JLabel(Lang.getString("nombre")));
        nombre = new JTextField(25);
        fields.add(nombre);
        fields.add(new JLabel(Lang.getString("capacidadgrupo")));
        capacidadgrupo = new JTextField(8);
        fields.add(capacidadgrupo);
        fields.add(new JLabel(Lang.getString("numeroalumnos")));
        numeroalumnos = new JTextField(8);
        fields.add(numeroalumnos);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.8;
        c.fill = GridBagConstraints.BOTH;
        add(fields, c);

        botoncolor = new JButton();
        Random rnd = new Random();
        botoncolor.setBackground(CtrlPresentacion.randomPastelColor());
        botoncolor.addActionListener(this);
        botoncolor.setActionCommand("setcolor");
        c = new GridBagConstraints();
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.2;
        c.fill = GridBagConstraints.BOTH;
        add(botoncolor,c);

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        add(new JLabel(Lang.getString("sesiones")), c);

        tablepanel = new JPanel();
        tablepanel.setLayout(new BorderLayout());
        Object[][] data = new Object[0][3];
        String[] columnNames = {Lang.getString("nombre"),
                Lang.getString("duracion"),
                Lang.getString("numerogrupos"),
                Lang.getString("recursos")};
        table = new JTable(new MyTableModel(data, columnNames));

        TableColumn tc = table.getColumn(table.getModel().getColumnName(3));
        tc.setCellEditor(new TableRecursosEditor());

        table.putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        table.getModel().addTableModelListener(this);
        tablepanel.add(table.getTableHeader(), BorderLayout.NORTH);
        tablepanel.add(table, BorderLayout.CENTER);
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 2;
        c.weighty = 0.9;
        c.gridwidth = 2;
        c.fill = GridBagConstraints.BOTH;
        JScrollPane jsp = new JScrollPane(tablepanel);
        add(jsp,c);


        buttons = new JPanel();
        buttons.setLayout(new GridLayout(0,4));
        newsesion = new JButton(Lang.getString("nuevasesion"));
        newsesion.addActionListener(this);
        newsesion.setActionCommand("addsession");
        buttons.add(newsesion);
        deletesesion = new JButton(Lang.getString("deletesesion"));
        deletesesion.addActionListener(this);
        deletesesion.setActionCommand("deletesesion");
        buttons.add(deletesesion);
        confirmar = new JButton(Lang.getString("confirmar"));
        confirmar.addActionListener(this);
        confirmar.setActionCommand("addasignatura");
        buttons.add(confirmar);
        cancelar = new JButton(Lang.getString("cancelar"));
        cancelar.addActionListener(this);
        cancelar.setActionCommand("cancel");
        buttons.add(cancelar);
        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 2;
        add(buttons, c);
    }

    public VistaAsignatura(CtrlPresentacion ctrl, String titulacion, String oldsiglas) {
        this(ctrl, titulacion);
        setTitle(Lang.getString("editarasignatura")+" "+oldsiglas);
        confirmar.addActionListener(this);
        confirmar.setActionCommand("editasignatura");
        this.oldsiglas = oldsiglas;
        Map<String, String> data = ctrl.getAsignaturaData(titulacion, oldsiglas);

        siglas.setText(data.get("siglas"));
        nombre.setText(data.get("nombre"));
        capacidadgrupo.setText(data.get("capacidadgrupo"));
        numeroalumnos.setText(data.get("numeroalumnos"));
        botoncolor.setBackground(ctrl.getColorAsignatura(titulacion,oldsiglas));
        updateTable();
    }

    public String getSiglas() {
        return siglas.getText();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("addasignatura")) {
            addAsignatura();
        } else if (e.getActionCommand().equals("editasignatura")) {
            editAsignatura();
        } else if (e.getActionCommand().equals("addsession")) {
            addSesion();
        } else if (e.getActionCommand().equals("setcolor")) {
            JColorChooser cc = new JColorChooser();
            cc.setPreviewPanel(new JPanel());
            JPanel panel = new JPanel();
            panel.add(cc);
            int result = JOptionPane.showConfirmDialog(null, panel, Lang.getString("crearaula"),
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (result == JOptionPane.OK_OPTION) {
                botoncolor.setBackground(cc.getColor());
            }
        } else if(e.getActionCommand().equals("deletesesion")){
            deleteSesion();
        } else if (e.getActionCommand().equals("cancel")) {
            siglas.setText("");
            setVisible(false);
        }
    }

    public void addAsignatura() {
        try {
            if (Integer.parseInt(capacidadgrupo.getText()) <= 0) throw new NumberFormatException("es negatiu");
            if (Integer.parseInt(numeroalumnos.getText()) <= 0) throw new NumberFormatException("es negatiu");
            if (!ctrl.addAsignatura(titulacion, siglas.getText(), nombre.getText(), Integer.parseInt(capacidadgrupo.getText()), Integer.parseInt(numeroalumnos.getText()))) {
                throw new DuplicateFormatFlagsException("error");
        }
            ctrl.setSesiones(titulacion,siglas.getText(),getSesionesData());
            ctrl.setColorAsignatura(titulacion,siglas.getText(),botoncolor.getBackground());
            setVisible(false);
        } catch(DuplicateFormatFlagsException DFE) {
            ctrl.gestionError(Lang.getString("errorasignaturaduplicado"));
        } catch(NumberFormatException NFE) {
            ctrl.gestionError(Lang.getString("errorasignaturacapacidad"));
        } catch(IllegalArgumentException IAE) {
            ctrl.gestionError(Lang.getString("errorargumento"));
        } catch(Exception ex) {
            ctrl.gestionError(Lang.getString("errorinesperado"));
        }
    }

    public void deleteSesion(){
        int row = table.getSelectedRow();
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        dtm.removeRow(row);
        table.setModel(dtm);
        table.repaint();
    }

    public void editAsignatura() {
        Map<String, String> data = new HashMap<String,String>();
        data.put("siglas",siglas.getText());
        data.put("nombre",nombre.getText());
        data.put("capacidadgrupo",capacidadgrupo.getText());
        data.put("numeroalumnos", numeroalumnos.getText());
        try {
            if (Integer.parseInt(capacidadgrupo.getText()) <= 0) throw new NumberFormatException("es negatiu");
            if (Integer.parseInt(numeroalumnos.getText()) <= 0) throw new NumberFormatException("es negatiu");
            ctrl.editAsignatura(titulacion,oldsiglas,data);
            ctrl.setSesiones(titulacion,siglas.getText(),getSesionesData());
            ctrl.setColorAsignatura(titulacion,siglas.getText(),botoncolor.getBackground());
            setVisible(false);
        } catch(NumberFormatException NFE) {
            ctrl.gestionError(Lang.getString("errorasignaturacapacidad"));
        } catch(IllegalArgumentException IAE) {
            ctrl.gestionError(Lang.getString("errorargumento"));
        } catch(Exception ex) {
            ex.printStackTrace();
            ctrl.gestionError(Lang.getString("errorinesperado"));
        }
    }

    public void updateTable() {
        MyTableModel tm = (MyTableModel) table.getModel();
        String[] sesiones = ctrl.getSesiones(titulacion,oldsiglas);
        tm.setRowCount(0);
        for (String s: sesiones) {
            Map <String,String> data = ctrl.getInfoSesion(titulacion,oldsiglas,s);
            tm.addRow(new String[]{data.get("nombre"), data.get("duracion"), data.get("ngrupos"), data.get("recursos")});
        }
        table.repaint();
    }

    public void addSesion() {
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        String[] data = {"","1","1",""};
        data[0] = Integer.toString(dtm.getRowCount()+1);
        dtm.addRow(data);
        table.setModel(dtm);
        table.repaint();
    }

    public class MyTableModel extends DefaultTableModel {
        public MyTableModel(Object[][] tableData, Object[] colNames) {
            super(tableData, colNames);
        }

        public boolean isCellEditable(int row, int column) {
            //if (column == 3) return false;
            return true;
        }
    }

    public void tableChanged(TableModelEvent e) {
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        try {
            switch (e.getColumn()) {
                case TableModelEvent.ALL_COLUMNS:
                case 0:
                    for (int i=0; i<dtm.getRowCount(); ++i) {
                        if(e.getFirstRow() < dtm.getRowCount()) {
                            if (i != e.getFirstRow() && dtm.getValueAt(i, 0).equals(dtm.getValueAt(e.getFirstRow(), 0)))
                                dtm.setValueAt(dtm.getValueAt(e.getFirstRow(), 0) + "*", e.getFirstRow(), 0);
                        }
                            //throw new DuplicateFormatFlagsException("ya existe");
                    }
                    if (e.getColumn() != TableModelEvent.ALL_COLUMNS) break;
                case 1:
                    if(e.getFirstRow() < dtm.getRowCount()) {
                        if (Integer.parseInt((String) dtm.getValueAt(e.getFirstRow(),1)) <= 0) {
                            dtm.setValueAt("1", e.getFirstRow(), 1);
                            throw new NumberFormatException("es negatiu");
                        }
                    }
                    if (e.getColumn() != TableModelEvent.ALL_COLUMNS) break;
                case 2:
                    if(e.getFirstRow() < dtm.getRowCount()) {
                        if (Integer.parseInt((String) dtm.getValueAt(e.getFirstRow(),2)) <= 0) {
                            dtm.setValueAt("1", e.getFirstRow(), 2);
                            throw new NumberFormatException("es negatiu");
                        }
                    }
            }
        } catch(DuplicateFormatFlagsException DFE) {
            ctrl.gestionError(Lang.getString("errorduplicado"));
        } catch(NumberFormatException NFE) {
            ctrl.gestionError(Lang.getString("errorasesionesnegativo"));
        } catch(IllegalArgumentException IAE) {
            ctrl.gestionError(Lang.getString("errorargumento"));
        } catch(Exception ex) {
            ex.printStackTrace();
            ctrl.gestionError(Lang.getString("errorinesperado"));
        }

    }

    public String[][] getSesionesData() {
        DefaultTableModel dtm = (DefaultTableModel) table.getModel();
        String[][] s = new String[dtm.getRowCount()][4];
        for (int i=0; i<s.length; ++i) {
            s[i][0] = (String) dtm.getValueAt(i,0);
            s[i][1] = (String) dtm.getValueAt(i,1);
            s[i][2] = (String) dtm.getValueAt(i,2);
            s[i][3] = (String) dtm.getValueAt(i, 3);
        }
        return s;
    }

    private class TableRecursosEditor extends AbstractCellEditor implements TableCellEditor {

        String s = "";

        @Override
        public Object getCellEditorValue() {
            return s;
        }

        @Override
        public Component getTableCellEditorComponent(JTable jTable, Object o, boolean b, int i, int i1) {
            s = (String) jTable.getModel().getValueAt(i, i1);
            String[] tmp = s == null ? new String[0] : s.split(",");
            if (tmp.length == 1 && tmp[0].trim().equals(""))
                tmp = new String[0];
            String[] r = VistaRecursos.gestionaRecursos(ctrl.getrecursos(), Arrays.asList(tmp));
            s = String.join(",", r);
            return new JLabel(s);
        }
    }
}
