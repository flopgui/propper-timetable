import javax.swing.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Representa los días de la semana.
 */
public enum DiaSemana {

	LUNES("lunes"),
	MARTES("martes"),
	MIERCOLES("miercoles"),
	JUEVES("jueves"),
	VIERNES("viernes");

    /**
     * Nombre que se imprimirá cuando se ejecute la función {@link DiaSemana#toString()}
     */
	private String nombre;

    /**
     * Constructor privado de {@link DiaSemana}.
     * @param nombre Nombre que se escribirá
     */
	DiaSemana(String nombre) {
		this.nombre = nombre;
	}
	
	/**
	 * Pasa los {@link DiaSemana} a una string.
	 * @return Devulve el {@link DiaSemana} formateado y con acentos.
	 */
	@Override
	public String toString() {
		return nombre;
	}

	public int toNumber() {
		switch (this) {
			case LUNES:
				return 0;
			case MARTES:
				return 1;
			case MIERCOLES:
				return 2;
			case JUEVES:
				return 3;
			case VIERNES:
				return 4;
			default:
				return -1;
		}
	}

	public static DiaSemana fromNumber(int i) {
		switch (i) {
			case 0:
				return LUNES;
			case 1:
				return MARTES;
			case 2:
				return MIERCOLES;
			case 3:
				return JUEVES;
			case 4:
				return VIERNES;
		}
		throw new IllegalArgumentException(i + " is not a valid week day");
	}

	public static DiaSemana fromString(String s) {
        switch (s.toUpperCase()) {
            case "LUNES":
                return LUNES;
            case "MARTES":
                return MARTES;
            case "MIERCOLES":
            case "MIÉRCOLES":
                return MIERCOLES;
            case "JUEVES":
                return JUEVES;
            case "VIERNES":
                return VIERNES;
            default:
                return null;
        }
    }

    /**
     * Lista de todos los {@link DiaSemana} existentes.
     */
	private static final List<DiaSemana> VALUES = Collections.unmodifiableList(Arrays.asList(values()));

    /**
     * Generador aleatorio de {@link DiaSemana}.
     * @param random Instancia de {@link java.util.Random} para generar números aleatorios.
     * @return Devuelve un {@link DiaSemana} aleatorio.
     */
	public static DiaSemana randomDia(Random random) {
		return VALUES.get(random.nextInt(VALUES.size()));
	}
}
