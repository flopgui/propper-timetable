import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.ArrayList;

public class HorarioTable extends JTable {

    private HorarioTableModel tableModel;
    private CtrlPresentacion ctrl;

    public HorarioTable(CtrlPresentacion ctrl, String[] aulas, ArrayList<String> asignaturas) {
        super();
        this.ctrl = ctrl;

        this.setCellSelectionEnabled(true);
        this.tableModel = new HorarioTableModel(ctrl.getHoraMin(), ctrl.getHoraMax(), aulas, asignaturas, ctrl.getHorarioData());
        this.setModel(tableModel);
        this.setTransferHandler(new TransferHandler() {

            public boolean canImport(TransferSupport info) {
                if (!info.isDataFlavorSupported(DataFlavor.stringFlavor))
                    return false;
                int c = columnAtPoint(info.getDropLocation().getDropPoint());
                int r = rowAtPoint(info.getDropLocation().getDropPoint());
                if (c == -1 || r == -1)
                    return false;
                String[] s;
                try {
                    s = ((String) info.getTransferable().getTransferData(DataFlavor.stringFlavor)).split(",");
                } catch (UnsupportedFlavorException | IOException e) { // no pasa nunca
                    return false;
                }
                return !(s.length < 3);
            }

            public boolean importData(TransferSupport info) {
                String[] s = null;
                try {
                    s = ((String) info.getTransferable().getTransferData(DataFlavor.stringFlavor)).split(",");
                } catch (UnsupportedFlavorException | IOException e) {
                    e.printStackTrace();
                }
                JTable.DropLocation dl = (JTable.DropLocation) info.getDropLocation();
                int row = dl.getRow();
                int column = dl.getColumn();

                try {
                    ctrl.moveHoraClase(Integer.parseInt(s[1]), Integer.parseInt(s[0]) - 1, s[2], row, column-1, s[2]);
                } catch (Exception e) {e.printStackTrace();}
                reset();
                return true;
            }
        });

        this.setDefaultRenderer(String[].class, new HorarioCellRenderer(ctrl));
        this.setDefaultEditor(String[].class, new HorarioCellRenderer(ctrl));

        this.addComponentListener(new ComponentListener() {
            @Override
            public void componentResized(ComponentEvent e) {
                for (int row = 0; row < getRowCount(); row++) {
                    int rowHeight = getRowHeight();

                    for (int column = 0; column < getColumnCount(); column++) {
                        Component comp = prepareRenderer(getCellRenderer(row, column), row, column);
                        rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
                    }

                    setRowHeight(row, rowHeight);
                }
            }

            @Override
            public void componentMoved(ComponentEvent e) {

            }

            @Override
            public void componentShown(ComponentEvent e) {

            }

            @Override
            public void componentHidden(ComponentEvent e) {

            }
        });
    }

    @Override
    public String getToolTipText(MouseEvent event) {
        String tip = null;
        Point p = event.getPoint();

        // Locate the renderer under the event location
        int hitColumnIndex = columnAtPoint(p);
        int hitRowIndex = rowAtPoint(p);

        if (hitColumnIndex != -1 && hitRowIndex != -1) {
            TableCellRenderer renderer = getCellRenderer(hitRowIndex, hitColumnIndex);
            Component component = prepareRenderer(renderer, hitRowIndex, hitColumnIndex);
            Rectangle cellRect = getCellRect(hitRowIndex, hitColumnIndex, false);
            component.setBounds(cellRect);
            component.validate();
            component.doLayout();
            p.translate(-cellRect.x, -cellRect.y);
            Component comp = component.getComponentAt(p);
            if (comp instanceof JComponent) {
                return ((JComponent) comp).getToolTipText();
            }
        }

        // No tip from the renderer get our own tip
        tip = getToolTipText();

        return tip;
    }

    public void reset() {
        tableModel = tableModel.copy(ctrl);
        HorarioTable.super.setModel(tableModel);
    }
}
