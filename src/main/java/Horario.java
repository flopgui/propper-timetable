import java.util.*;

/**
 * Clase que representa un horario ya finalizado, es decir, representa unalista de asociaciones de {@link HoraClase} con
 * un {@link BloqueET} (p. e. La {@link HoraClase} (sesión de teoría de BD de los grupos 11 y 12) se realizará en el
 * {@link BloqueET} (aula A5S102 el lunes a las 10). Además, el horario lleva asociada una ``fitness'' que es un número
 * que representa lo bueno que es el horario (cuanto más grande mejor es el horario).
 */
public class Horario {

    /**
     * Lista de las asociaciones de {@link HoraClase} y {@link BloqueET}.
     */
    HashMap<BloqueET, HoraClase> asignaciones;

    /**
     * Valor númerico que representa como de bueno es el {@link Horario}.
     */
	private Double fitness;

    /**
     * Constructor básico. Esta función asume que el número de sesiones y el número de bloques pasados es el mismo.
     * @param sesiones Las {@link HoraClase} que hay que colocar en el {@link Horario}.
     * @param bloques El conjunto de {@link BloqueET} al que van asignadas las {@code sesiones}.
     */
	public Horario(BloqueET[] bloques, HoraClase[] sesiones) {
		fitness = null;
		asignaciones = new HashMap<BloqueET, HoraClase>();
		for (int i=0; i<sesiones.length; ++i) {
			asignaciones.put(bloques[i], sesiones[i]);
		}
	}


    /**
     * Esta función ``reproduce'' dos {@link Horario}s, creando un nuevo {@link Horario} con características de los dos.
     * @param h2 El {@link Horario} con el que nos reproduciremos.
     * @param rnd Instancia de {@link java.util.Random} para generar números aleatorios.
     * @param mutationProbability La probabilidad de que en el proceso de reproducción se produzca una mutación.
     * @return Devuelve un nuevo {@link Horario} resultado de la reproducción.
     */
	public Horario[] reproduce(Horario h2, Random rnd, double crossoverProbability, double mutationProbability) {
		BloqueET[] bloques = asignaciones.keySet().toArray(new BloqueET[asignaciones.size()]);
		HoraClase[] hc1 = new HoraClase[bloques.length];
        HoraClase[] hc2 = new HoraClase[bloques.length];
        boolean which = false;
		for (int i = 0; i < bloques.length; ++i) {
			double p = rnd.nextDouble();
            if (which) {
                hc1[i] = asignaciones.get(bloques[i]);
                hc2[i] = asignaciones.get(bloques[i]);
            } else {
                hc2[i] = asignaciones.get(bloques[i]);
                hc1[i] = asignaciones.get(bloques[i]);
            }
            if (p < crossoverProbability) {
            	which = !which;
			}
		}
		for (int i=0; i<bloques.length; ++i) {
            double p = rnd.nextDouble();
            if (p < mutationProbability) {
                int j = rnd.nextInt(bloques.length);
                HoraClase temp = hc1[j];
                hc1[j] = hc1[i];
                hc1[i] = temp;
            }
            if (1-p < mutationProbability) {
                int j = rnd.nextInt(bloques.length);
                HoraClase temp = hc2[j];
                hc2[j] = hc2[i];
                hc2[i] = temp;
            }
        }
		return new Horario[]{new Horario(bloques,hc1), new Horario(bloques,hc2)};
	}

    /**
     * Calcula el valor númerico que representa como de bueno es el {@link Horario}.
     * @param restricciones Una lista de las {@link Restriccion}es para calcular el valor.
     */
	private void calculateFitness(List<Restriccion> restricciones) {
		fitness = 1000.0 * asignaciones.size();

		for (Restriccion r : restricciones) {
			if (!r.isActive()) continue;
			fitness -= r.getPeso() * r.evalua(this);
		}
		fitness = Double.max(0.0,fitness);
	}
		
	/**
	 * @return Devuelve la fitness del {@link Horario}.
	 */
	public double getFitness() {
		if (fitness == null) {
			calculateFitness(CtrlDominio.getCtrlDominio().getRestriccionesAlgoritmo());
		}
		return fitness;
	}

	public boolean swapAsignacion(int oldHora, int oldDia, Aula oldAula, int newHora, int newDia, Aula newAula) {
		BloqueET oldb = new BloqueET(oldAula, new BloqueHorario(DiaSemana.fromNumber(oldDia), oldHora), 1);
		BloqueET newb = new BloqueET(newAula, new BloqueHorario(DiaSemana.fromNumber(newDia), newHora), 1);
		HoraClase oldh = asignaciones.get(oldb);
		HoraClase newh = asignaciones.get(newb);
		for (Map.Entry<BloqueET,HoraClase> me : asignaciones.entrySet()) {
			if (me.getKey().equals(oldb)) {
				oldb = me.getKey();
				oldh = me.getValue();
			}
			if (me.getKey().equals(newb)) {
				newb = me.getKey();
				newh = me.getValue();
			}
		}
		asignaciones.replace(oldb, newh);
		asignaciones.replace(newb, oldh);
		return true;
	}

	/**
	 * @return Devuelve las asignaciones del {@link Horario}.
	 */
	public HashMap<BloqueET, HoraClase> getAsignaciones() {
		return asignaciones;
	}

	public void resetFitness() {
	    fitness = null;
    }
}
