/**
 * Clase que representa a un grupo. El grupo viene representado por un nombre (p.e. 41, 32, ...).
 */
public class Grupo {
	
	/**
	 * Nombre del {@link Grupo}.
	 */
	private String nombre;

         /**
        * Constructor básico.
        * @param nombre Nombre del {@link Grupo}.
        */
        public Grupo(String nombre) {
            if(nombre.isEmpty())
                throw new java.lang.IllegalArgumentException("nombre del grupo no puede estar vacio");
            this.nombre = nombre;    
        }
        
        /**
         * @return Nombre del {@link Grupo}.
        */
        public String getNombre(){
            return nombre;
        }

        /**
         * Cambia el nombre de {@link Grupo}.
         * @param n Nuevo nombre.
        */
        public void setNombre(String n) {
            this.nombre = n;
        }

    /**
     * Comparador de la clase {@link Grupo}.
     * @param o Objeto con el que se quiere comparar.
     * @return Devuelve true sii {@code o} es un {@link Aula} y los nombres coinciden.
     */
    @Override
        public boolean equals(Object o) {
            if (o instanceof Grupo)
                return ((Grupo) o).nombre.equals(nombre);
            return false;
        }
        
}
