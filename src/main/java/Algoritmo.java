
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

/**
 * Clase que realiza un algoritmp genético sobre la clase {@link Horario}. Esta clase mantiene una
 * población de {@link Horario}s y va iterando durante el número de generaciones dado hasta
 * @author ernesto
 */
public class Algoritmo {

    private static final double PROBABILIDAD_MUTACION = 0.01;
    private static final double PROBABILIDAD_CRUZAMIENTO = 0.05;
    private static final double ELITE = 0.1;
    private static final double STEEPNESS = 2;

    /**
     * Generador de números aleatorio, usado para elegir a los miembros de la población que sobreviven.
     */
    private Random random;

    /**
     * Lista de las {@link Restriccion}es que hay en la clase.
     */
    private ArrayList<Restriccion> restricciones;

    /**
     * Constructor básico de la clase.
     * @param random Instancia de {@link java.util.Random} para generar números aleatorios.
     */
    public Algoritmo(Random random) {
        this.random = random;

        restricciones = new ArrayList<Restriccion>();
        restricciones.add(new Restriccion("SOLAPAMIENTOS",
        		"No puede haber dos clases en la misma aula al mismo tiempo.",
        		2, Algoritmo::restSolapamiento));
        restricciones.add(new Restriccion("DISPERSION_HORAS",
                "",
                0.05, Algoritmo::restDispersionHoras));
        restricciones.add(new Restriccion("ACABAR_A_HORA",
                "",
                0.3, Algoritmo::restAcabarAHora));
        restricciones.add(new Restriccion("CAPACIDAD_EXCEDIDA",
                "",
                0.2, Algoritmo::restCapacidadExcedida));
        restricciones.add(new Restriccion("CAPACIDAD_DESAPROVECHADA",
                "",
                0.02, Algoritmo::restCapacidadDesaprovechada));
        restricciones.add(new Restriccion("RECURSOS",
        		"Una aula debe tener los recursos necesarios.",
        		0.3, Algoritmo::restRecursos));
        restricciones.add(new Restriccion("CUATRIMESTRES",
                "",
                0.1, Algoritmo::restCuatrimestres));
        restricciones.add(new Restriccion("MISMA_ASIGNATURA",
                "",
                0.2, Algoritmo::restMismaAsignatura));
        restricciones.add(new Restriccion("MISMA_ASIGNATURA_DIA",
                "",
                0.1, Algoritmo::restMismaAsignaturaDia));
    }

    public Horario generate(Horario initialpopulation[], int maxGenerations) {
        return generate(initialpopulation, maxGenerations, null);
    }

    /**
     * Aplica el algoritmo genético en una población inicial hasta llegar a <code>maxGenerations</code>.
     * @param initialpopulation Población inicial sobre la que aplicar el algoritmo.
     * @param maxGenerations Numero de generaciones a realizar.
     * @return Devuelve al mejor individuo de todas las generaciones que ha habido.
     */
    public Horario generate(Horario initialpopulation[], int maxGenerations, BlockingQueue<Integer> blockingDeque) {
        ArrayList<Horario> population = new ArrayList<Horario>();
        population.addAll(Arrays.asList(initialpopulation));
        Collections.sort(population, new Comparator<Horario>() {
            @Override
            public int compare(Horario horario, Horario t1) {
                return Double.compare(horario.getFitness(), t1.getFitness());
            }
        });
        Horario best = population.get(population.size()-1);
        int generations = 0;
        while (generations++ < maxGenerations) {

            if (blockingDeque != null)
                blockingDeque.offer(generations);
            population = doGeneration(population);
            Collections.sort(population, new Comparator<Horario>() {
                @Override
                public int compare(Horario horario, Horario t1) {
                    return Double.compare(horario.getFitness(), t1.getFitness());
                }
            });
            Horario generationBest = population.get(population.size() - 1);
            if (generationBest.getFitness() > best.getFitness()) {
                best = generationBest;
            }

            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                return null;
            }
        }

        if (blockingDeque != null) {
            try {
                blockingDeque.put(generations);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return best;
    }

    /**
     * Calcula la siguiente generación a partir de la población dada.
     * @param population La población actual.
     * @return Devuelve la nueva generación.
     */
    private ArrayList<Horario> doGeneration(ArrayList<Horario> population) {
        double total = 0;
        for (Horario ind : population) {
            total += ind.getFitness();
        }

        ArrayList<Horario> newPop = new ArrayList<Horario>();

        for (int i = 0; i < population.size()*0.5-population.size()*ELITE; i++) {
            double p1, p2;
            Horario i1, i2;
            do p1 = 1-Math.abs(random.nextGaussian()/STEEPNESS);
            while (p1 < 0 || p1 > 1);
            do p2 = 1-Math.abs(random.nextGaussian()/STEEPNESS);
            while (p2 < 0 || p2 > 1);
            i1 = population.get((int) Math.floor(population.size()*p1));
            i2 = population.get((int) Math.floor(population.size()*p2));

            Horario[] ind = i1.reproduce(i2, random, PROBABILIDAD_CRUZAMIENTO, PROBABILIDAD_MUTACION);
            newPop.add(ind[0]);
            newPop.add(ind[1]);
        }

        for (int i = 0; i < population.size()*ELITE; ++i) {
            newPop.add(population.get(population.size()-1-i));
        }

        return newPop;
    }

    /**
     * Función que calcula el número de solapamientos. Esto nos servirá para crear una {@link Restriccion}.
     * @param h El {@link Horario} del cual calcula el número de solapamientos.
     * @return El número de solapamientos total.
     */
    private static double restSolapamiento(Horario h) {
    	int cnt = 0;
    	for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            if (m.getValue().getDuracion() == 1) continue;
			for (Map.Entry<BloqueET,HoraClase> m2 : h.getAsignaciones().entrySet()) {
                if (m2.getValue() == null) continue;
			    if (m.getValue().equals(m2.getValue())) continue;
                if (m2.getValue().getDuracion() == 1) continue;
				BloqueET b1 = new BloqueET(m.getKey().getAula(), m.getKey().getBloqueHorario(), m.getValue().getDuracion());
                BloqueET b2 = new BloqueET(m2.getKey().getAula(), m2.getKey().getBloqueHorario(), m2.getValue().getDuracion());
                if (b1.solapa(b2)) ++cnt;
			}
    	}
    	return cnt;
    }

    private static double restDispersionHoras(Horario h) {
        int cnt = 0;
        for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            for (Map.Entry<BloqueET,HoraClase> m2 : h.getAsignaciones().entrySet()) {
                if (m2.getValue() == null) continue;
                if (m.getValue().equals(m2.getValue())) continue;
                if (m.getKey().getBloqueHorario().equals(m2.getKey().getBloqueHorario())) {
                    ++cnt;
                }
            }
        }
        return cnt;
    }

    private static double restAcabarAHora(Horario h) {
        int cnt = 0;
        for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            BloqueET bet = m.getKey();
            Sesion s = m.getValue().getSesion();
            if (bet.getBloqueHorario().getHoraInicio() + s.getDuracion() > CtrlDominio.getCtrlDominio().getHoraMax())
                cnt += bet.getBloqueHorario().getHoraInicio() + s.getDuracion() - CtrlDominio.getCtrlDominio().getHoraMax();
        }
        return cnt;
    }

    private static double restCapacidadExcedida(Horario h) {
        int cnt = 0;
        for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            BloqueET bet = m.getKey();
            Sesion s = m.getValue().getSesion();
            if (s.getnGrupos() * s.getAsignatura().getCapacidadGrupo() - bet.getAula().getCapacidad() > 0)
                cnt += s.getnGrupos() * s.getAsignatura().getCapacidadGrupo() - bet.getAula().getCapacidad();
        }
        return cnt;
    }

    private static double restCapacidadDesaprovechada(Horario h) {
        int cnt = 0;
        for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            BloqueET bet = m.getKey();
            Sesion s = m.getValue().getSesion();
            if (s.getnGrupos() * s.getAsignatura().getCapacidadGrupo() - bet.getAula().getCapacidad() < 0)
                cnt -= s.getnGrupos() * s.getAsignatura().getCapacidadGrupo() - bet.getAula().getCapacidad();
        }
        return cnt;
    }

    /**
     * Función que comprueba que cada {@link Sesion} de {@code h} se haga en un aula con
     * los {@link Recurso}s necesarios.
     * @param h El {@link Horario} sobre el que calcular la penalización.
     * @return Penalización que recibirá {@code h} por incumplir la necesidad de recursos.
     */
    private static double restRecursos(Horario h) {
    	int cnt = 0;
    	for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
    		HoraClase hc = m.getValue();
			BloqueET bet = m.getKey();
			for (Recurso r : hc.getSesion().getRecursos()) {
				if (!bet.getAula().hasRecurso(r)) ++cnt;
			}
    	}
    	return cnt;
    }

    private static double restCuatrimestres(Horario h) {
        int cnt = 0;
        for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            for (Map.Entry<BloqueET,HoraClase> m2 : h.getAsignaciones().entrySet()) {
                if (m2.getValue() == null) continue;
                if (m.getValue().equals(m2.getValue())) continue;
                if (!m.getKey().getBloqueHorario().equals(m2.getKey().getBloqueHorario())) continue;
                for (String t : CtrlDominio.getCtrlDominio().getNombresTitulaciones()) {
                    boolean found = false;
                    for (int i=0; i<CtrlDominio.getCtrlDominio().getNombresCuatrimestres(t).length; ++i) {
                        HashSet<String> hs = new HashSet<String>(Arrays.asList(CtrlDominio.getCtrlDominio().getSiglasAsignaturasCuatrimestre(t, i)));
                        if (hs.contains(m.getValue().getSesion().getAsignatura().getSiglas())) {
                            if (hs.contains(m2.getValue().getSesion().getAsignatura().getSiglas())) ++cnt;
                        }
                    }
                }
            }
        }
        return cnt;
    }

    private static double restMismaAsignatura(Horario h) {
        int cnt = 0;
        for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            for (Map.Entry<BloqueET,HoraClase> m2 : h.getAsignaciones().entrySet()) {
                if (m2.getValue() == null) continue;
                if (m.getValue().equals(m2.getValue())) continue;
                if (!m.getKey().getBloqueHorario().equals(m2.getKey().getBloqueHorario())) continue;
                if (!m.getValue().getSesion().getAsignatura().equals(m2.getValue().getSesion().getAsignatura())) continue;
                if (m.getValue().getSesion().getAsignatura().equals(m2.getValue().getSesion().getAsignatura())) {
                    ++cnt;
                }
            }
        }
        return cnt;
    }

    private static double restMismaAsignaturaDia(Horario h) {
        int cnt = 0;
        for (Map.Entry<BloqueET,HoraClase> m : h.getAsignaciones().entrySet()) {
            if (m.getValue() == null) continue;
            for (Map.Entry<BloqueET,HoraClase> m2 : h.getAsignaciones().entrySet()) {
                if (m2.getValue() == null) continue;
                if (m.getValue().equals(m2.getValue())) continue;
                if (!m.getValue().getSesion().getAsignatura().equals(m2.getValue().getSesion().getAsignatura())) continue;
                if (m.getKey().getBloqueHorario().getDs().toString().equals(m2.getKey().getBloqueHorario().getDs().toString())) ++cnt;
            }
        }
        return cnt;
    }
    
    /**
     * @return Lista de todas las {@link Restriccion}es.
     */
    public Restriccion[] getRestricciones() {
    	return restricciones.toArray(new Restriccion[restricciones.size()]);
    }
    
    /**
     * Activa o desactiva la {@link Restriccion} con nombre {@code h}. Esta función asume que
     * existe una {@link Restriccion} con nombre {@code h}.
     * @param nombre Nombre de la {@link Restriccion}.
     * @return Nuevo estado de la {@link Restriccion} {@code nombre}.
     */
    public boolean toggleRestriccion(String nombre) {
    	for (Restriccion r : restricciones) {
    		if (r.getNombre().equals(nombre)) {
    			return r.toggleActive();
    		}
    	}
    	return false;
    }

    public void setRestriccionPeso(String s, double p) {
        for (Restriccion r : restricciones) {
            if (r.getNombre().equals(s))
                r.setPeso(p);
        }
    }

    public void setRestriccionActive(String s, boolean b) {
        for (Restriccion r : restricciones) {
            if (r.getNombre().equals(s))
                r.setActive(b);
        }
    }

    /**
     * @return Devuelve una lista con todas las {@link Restriccion}es.
     */
    public ArrayList<Restriccion> getRestriccionesAL() {
        return restricciones;
    }
}
