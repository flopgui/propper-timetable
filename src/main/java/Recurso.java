/**
 * Representa un recurso que puede ofrecer un aula o necesitar una asignatura, como por ejemplo
 * un proyector, o un servidor para BD, ...
 */
public class Recurso {
	
    /**
     * Nombre del {@link Recurso}.
    */
    private String nombre;

     /**
    * Constructor básico.
    * @param nombre Nombre del {@link Recurso}.
    */
    public Recurso(String nombre) {
        if(nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("Nombre del recurso no puede estar vacio =)");
        this.nombre = nombre;
    }

    /**
    * @return Nombre del {@link Recurso}.
    */
    public String getNombre() {
        return nombre;
    }

    /**
     * Cambia el nombre del {@link Recurso}.
     * @param nombre Nuevo nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Comparador de la clase {@link Recurso}.
     * @param o Objeto con el que se quiere comparar.
     * @return Devuelve {@code true} sii {@code o} es un {@link Recurso} y los nombres coinciden.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof  Recurso)
            return ((Recurso) o).nombre.equals(nombre);
        return false;
    }
}
