import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Arrays;

public class MultiLineTableCellRenderer extends AbstractCellEditor implements TableCellRenderer, TableCellEditor {

    CtrlPresentacion ctrl;

    MultiLineTableCellRenderer(CtrlPresentacion ctrlPresentacion) {
        this.ctrl = ctrlPresentacion;
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        return generateList((String[]) value, isSelected);
    }

    private JLabel generateList(String[] value, boolean isSelected) {
        JLabel l = new JLabel();
        //make multi line where the cell value is String[]
        l.setText(String.join(",", value));
        //cell backgroud color when selected
        if (isSelected) {
            l.setBackground(UIManager.getColor("Table.selectionBackground"));
        } else {
            l.setBackground(UIManager.getColor("Table.background"));
        }

        return l;
    }

    String[] s;

    @Override
    public Component getTableCellEditorComponent(JTable jTable, Object o, boolean b, int i, int i1) {
        DefaultTableModel dtm = (DefaultTableModel)jTable.getModel();
        String aula = (String)dtm.getValueAt(i,0);
        s = (String[]) o;
        String[] r = VistaRecursos.gestionaRecursos(ctrl.getrecursos(), Arrays.asList(s));
        s = r;
        ctrl.setRecursosAula(aula,s);
        return new JLabel(String.join(",", s));
    }

    @Override
    public Object getCellEditorValue() {
        return s;
    }
}