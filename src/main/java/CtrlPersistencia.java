import com.google.gson.Gson;

import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import javax.imageio.*;

public class CtrlPersistencia {

    private static final Path PREFERENCES_FILE = Paths.get(".preferences.properties");
    private static final Path AUTOSAVE_FILE = Paths.get(".autosave.ttb");

    private static BufferedWriter open2Write(String path) throws FileNotFoundException, Exception {
        return new BufferedWriter(new FileWriter(path)); //crear nuevo
    }

    private static BufferedReader open2Read(String path) throws FileNotFoundException, Exception {
        return new BufferedReader(new FileReader(path)); //crear nuevo
    }

    public static void exportTo(String path, String contents) throws Exception {
        BufferedWriter bw = open2Write(path);
        bw.write(contents);
        bw.flush();
        bw.close();
    }

    public static ArrayList<String> openTitulacion(String path) throws Exception {
        ArrayList<String> tits = new ArrayList<>();
        BufferedReader br = open2Read(path);
        String line;
        while ((line = br.readLine()) != null)
            tits.add(line);
        return tits;
    }

    public static ArrayList<String> openAula(String path) throws Exception {
        ArrayList<String> tits = new ArrayList<>();
        BufferedReader br = open2Read(path);
        String line;
        while ((line = br.readLine()) != null)
            tits.add(line);
        return tits;
    }

    public static void autoSave(String[] horario, String ud) throws Exception {
        File f = new File(AUTOSAVE_FILE.toString());
        f.mkdirs();
        save(AUTOSAVE_FILE.toString(), horario, ud);
    }

    public static void save(String path, String[] horario, String ud) throws Exception {
        deleteAutosaved();
        BufferedWriter bw = open2Write(path);
        StringBuilder s = new StringBuilder();
        s.append(ud);
        s.append("\n");
        s.append(horario[0]);
        s.append("\n");
        s.append(horario[1]);
        bw.write(s.toString());
        bw.flush();
        bw.close();
    }

    public static boolean existsAutosaved() {
        return new File(AUTOSAVE_FILE.toString()).exists();
    }

    public static String[] openAutosaved() throws Exception {
        return open(AUTOSAVE_FILE.toString());
    }

    public static String[] open(String path) throws Exception{
        BufferedReader br = open2Read(path);
        String[] s = new String[3];
        s[0] = br.readLine();
        s[1] = br.readLine();
        s[2] = br.readLine();
        return s;
    }

    public static void savePreferencias(String[] prefs) {
        Gson gson = new Gson();
        File preferences = new File(PREFERENCES_FILE.toString());
        try (FileWriter fw = new FileWriter(preferences)) {
            preferences.mkdirs();
            gson.toJson(prefs,fw);
        } catch (IOException e){}
    }

    public static String[] openPreferencias() {
        Gson gson = new Gson();
        File preferences = new File(PREFERENCES_FILE.toString());
        try (FileReader fr = new FileReader(preferences)) {
            return gson.fromJson(fr, String[].class);
        } catch (IOException e){return null;}
    }

    public static void saveImage(String path, BufferedImage bi) throws IOException {
        ImageIO.write(bi,"jpeg", new File(path));
    }

    public static void deleteAutosaved() {
        try {Files.delete(FileSystems.getDefault().getPath(AUTOSAVE_FILE.toString()));}
        catch (IOException ignored) {
        }
    }

}
