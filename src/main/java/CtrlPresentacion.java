import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;

public class CtrlPresentacion {

    private static final double THRESHOLD = Math.sqrt(1.05 * 0.05) - 0.05;


    private CtrlDominio ctrlDominio;
    private MainWindow mainwindow;

    public CtrlPresentacion(){
        ctrlDominio = CtrlDominio.getCtrlDominio();
        if (ctrlDominio.existsAutosaved()) {
            int b = JOptionPane.showConfirmDialog(null, Lang.getString("existeautosaved"),
                    Lang.getString("recuperar"), JOptionPane.YES_NO_OPTION);
            if (b == JOptionPane.YES_OPTION) {
                try {
                    ctrlDominio.openAutosaved();
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, Lang.getString("errorinesperado"),
                            Lang.getString("errorinesperado"), JOptionPane.ERROR_MESSAGE);
                    ctrlDominio.deleteAutosaved();
                }
            }
        }
        ctrlDominio.init();
        mainwindow = new MainWindow(this);
    }

    public void deleteAutosaved() {
        ctrlDominio.deleteAutosaved();
    }


    public void setHorasLactivas(int horaMin, int horaMax) {
        ctrlDominio.changeHorasLectivas(horaMin, horaMax);
    }

    public String getNombreUnidadDocente() {
        return ctrlDominio.getNombreUnidadDocente();
    }

    public String[] getrecursos(){return ctrlDominio.getRecursos();}

    public boolean addAula(String name, int capacidad) {
        return ctrlDominio.addAula(name, capacidad);
    }

    public String[] getAulas() {
        return ctrlDominio.getAulas();
    }

    public String[] getAsignaturas(String titulacion) {
        return ctrlDominio.getAsignaturas(titulacion);
    }

    public boolean deleteAula(String name) {return ctrlDominio.deleteAula(name);}

    public boolean hasAula(String name) {return ctrlDominio.hasAula(name);}

    public void setname(String oname, String nname) {ctrlDominio.changenameAula(oname,nname);}

    public void setCapacidadAula(String name, int cap) {ctrlDominio.setCapacidadAula(name,cap);}

    public Object[][] getAulasData() {
        Object[][] d = new Object[ctrlDominio.getNumeroAulas()][3];
        for (int i=0; i<d.length; ++i) {
            String nombre = ctrlDominio.getNombreAula(i);
            d[i][0] = nombre;
            d[i][1] = ctrlDominio.getCapacidadAula(i);
            d[i][2] = ctrlDominio.getRecursosAula(nombre);
        }
        return d;
    }

    public String[] getrecusosAula(String name){ return ctrlDominio.getRecursosAula(name); }

    public boolean addRecursoAula (String name, String Recurso){ return ctrlDominio.addRecursoAula(name,Recurso); }

    public boolean removeRecursoAula (String name, String Recurso) {return ctrlDominio.removeRecursoAula(name,Recurso);}

    public void setRecursosAula (String name, String[] recursos) {
        for (String s: ctrlDominio.getRecursosAula(name)) {
            ctrlDominio.removeRecursoAula(name,s);
        }
        for (String s: recursos) {
            ctrlDominio.addRecursoAula(name,s);
        }
    }

    public void renameRecurso(String oldname, String newname) {
        ctrlDominio.renameRecurso(oldname,newname);
    }

    /**
     * Anade una {@link Titulacion}
     * @param name Nombre de la tutulacion a añadir.
     * @return Devuelve {@code true} si la titulacion se ha añadido
     */
    public boolean addTitulacion(String name) {
        return ctrlDominio.addTitulacion(name);
    }

    /**
     * Cambia el nombre de una {@link Titulacion}.
     * @param oldName Nombre viejo de la {@link Titulacion}.
     * @param newName Nombre nuevo de la {@link Titulacion}.
     * @return Devuelve {@code true} si se ha podido cambiar el nombre de la {@link Titulacion} y {@code false} si ha
     * ocurrido un error como que ya exista una {@link Titulacion} con nombre {@code newName}.
     */
    public boolean renameTitulacion(String oldName, String newName) { return ctrlDominio.renameTitulacion(oldName,newName); }

    /**
     * @return Lista de nombres de las {@link Titulacion}
     */
    public String[] getNombresTitulaciones() {
        return ctrlDominio.getNombresTitulaciones();
    }

    /**
     * Elimina una {@link Titulacion} de la {@link UnidadDocente}.
     * @param name Nombre de la {@link Titulacion} a eliminar.
     * @return Devuelve {@code true} si se ha eliminado, es decir, si la titulación existía.
     */
    public boolean deleteTitulacion(String name) {
        return ctrlDominio.deleteTitulacion(name);
    }

    public Object[][] getTitulacionData(String t) {
        Object[][] d = new Object[ctrlDominio.getNombresCuatrimestres(t).length][2];
        for (int i=0; i<d.length; ++i) {
            d[i][0] = ctrlDominio.getNombresCuatrimestres(t)[i];
            d[i][1] = ctrlDominio.getSiglasAsignaturasCuatrimestre(t,i);
        }
        return d;
    }

    public void gestionError(String s){
        JOptionPane.showMessageDialog(new JFrame(Lang.getString("error")),
                s, Lang.getString("error"), JOptionPane.ERROR_MESSAGE);

    }

    public boolean addCuatrimestre(String titulacion, String nombre) {
        return ctrlDominio.addCuatrimestre(titulacion, nombre);
    }

    public boolean removeCuatrimestre(String titulacion, String nombre) {
        for (String a: ctrlDominio.getSiglasAsignaturasCuatrimestre(titulacion, Integer.parseInt(nombre)-1)) {
            this.deleteAsignatura(titulacion,a);
        }
        return ctrlDominio.removeCuatrimestre(titulacion, nombre);
    }

    public int getCuatrimestreAsignatura(String titulacion, String siglas) {
        return Integer.parseInt(ctrlDominio.getCuatrimestreAsignatura(titulacion, siglas)) - 1;
    }

    public boolean addAsignatura(String titulacion, String siglas, String nombre, int capacidadGrupo, int numeroAlunnos) {
        if (!ctrlDominio.addAsignatura(titulacion,nombre,siglas,capacidadGrupo,numeroAlunnos)) return false;
        return true;
    }

    public boolean addAsignaturaToCuatrimestre(String titulacion, int cuatrimestre, String siglas) {
        return ctrlDominio.addAsignaturaCuatrimestre(titulacion, cuatrimestre, siglas);
    }

    public Map<String, String> getAsignaturaData(String titulacion, String siglas) {
        Map<String, String> d = new HashMap<String, String>();
        d.put("siglas", siglas);
        d.put("nombre", ctrlDominio.getNombreAsignatura(titulacion,siglas));
        d.put("numeroalumnos", Integer.toString(ctrlDominio.getNumeroAlumnosAsignatura(titulacion,siglas)));
        d.put("capacidadgrupo", Integer.toString(ctrlDominio.getCapacidadGrupoAsignatura(titulacion,siglas)));
        return d;
    }

    public void editAsignatura(String titulacion, String oldsiglas, Map<String, String> d) {
        ctrlDominio.setSiglasAsignatura(titulacion,oldsiglas,d.get("siglas"));
        ctrlDominio.setNombreAsignatura(titulacion,d.get("siglas"),d.get("nombre"));
        ctrlDominio.setNumeroAlumnosAsignatura(titulacion,d.get("siglas"),Integer.parseInt(d.get("numeroalumnos")));
        ctrlDominio.setCapacidadGrupoAsignatura(titulacion,d.get("siglas"),Integer.parseInt(d.get("capacidadgrupo")));
    }

    public boolean cloneAsignatura(String titulacion, int cuatri, String siglas, String newsiglas) {
        if (!ctrlDominio.cloneAsignatura(titulacion,siglas,newsiglas)) return false;
        ctrlDominio.addAsignaturaCuatrimestre(titulacion,cuatri,newsiglas);
        return true;
    }

    public boolean moveAsignaturaCuatrimestre(String titulacion, int newCuatri, String siglas) {
        int oldcuatri = getCuatrimestreAsignatura(titulacion,siglas);
        return ctrlDominio.moveAsignaturaCuatrimestre(titulacion,oldcuatri,newCuatri,siglas);
    }

    public void deleteAsignatura(String titulacion, String siglas) {
        ctrlDominio.deleteAsignatura(titulacion, siglas);
    }

    public Color getColorAsignatura(String titulacion, String asignatura) {
        return ctrlDominio.getColorAsignatura(titulacion, asignatura);
    }

    public void setColorAsignatura(String titulacion, String asignatura, Color color) {
        ctrlDominio.setColorAsignatura(titulacion, asignatura, color);
    }

    public String[] getSesiones(String titulacion, String asignatura) {
        return ctrlDominio.getSesiones(titulacion,asignatura);
    }

    public Map<String,String> getInfoSesion(String titulacion, String asignatura, String sesion) {
        Map<String, String> d = new HashMap<String, String>();
        d.put("nombre", sesion);
        d.put("duracion", Integer.toString(ctrlDominio.getDuracionSesion(titulacion,asignatura,sesion)));
        d.put("ngrupos", Integer.toString(ctrlDominio.getNGruposSesion(titulacion, asignatura, sesion)));
        d.put("recursos", ctrlDominio.getRecursosSesion(titulacion, asignatura, sesion));
        return d;
    }

//    public void editSesion(String titulacion, String asignatura, String oldsesion, Map<String,String> d) {
//        ctrlDominio.setDuracionSesion(titulacion, asignatura, oldsesion, Integer.parseInt(d.get("duracion")));
//        ctrlDominio.setNGruposSesion(titulacion, asignatura, oldsesion, Integer.parseInt(d.get("ngrupos")));
//        ctrlDominio.renameSesion(titulacion,asignatura,oldsesion,d.get("nombre"));
//        ctrlDominio.setRecursosSesion(titulacion,asignatura,oldsesion,d.get("recursos"));
//    }

    public void setSesiones(String titulacion, String asignatura, String[][] data) {
        for (String s: getSesiones(titulacion, asignatura))
            ctrlDominio.removeSesion(titulacion,asignatura,s);
        for (String[] s: data) {
            ctrlDominio.addSesion(titulacion,asignatura,s[0], Integer.parseInt(s[1]), Integer.parseInt(s[2]),
                    s[3].trim().equals("") ? new String[0] : s[3].split(","));
        }
    }

    public String[] getRestricciones() {
        return ctrlDominio.getRestricciones();
    }

    public void setRestriccionPeso(String r, double p) {
        ctrlDominio.setRestriccionPeso(r, p);
    }
    public void setRestriccionActiva(String s, boolean b) {
        ctrlDominio.setRestriccionActiva(s, b);
    }

    public int generaHorario(BlockingQueue<Integer> blockingDeque) {
        return ctrlDominio.generaHorario(blockingDeque);
    }

    public double getRestriccionPeso(String n) {
        return ctrlDominio.getRestriccionPeso(n);
    }

    public boolean hasHorario() {
        return ctrlDominio.hasHorario();
    }

    public int getHoraMin() {
        return ctrlDominio.getHoraMin();
    }

    public int getHoraMax() {
        return ctrlDominio.getHoraMax();
    }

    public int getGenerationCount() {
        return ctrlDominio.getGenerationCount();
    }

    public boolean moveHoraClase(int oldHora, int oldDia, String oldAula, int newHora, int newDia, String newAula) {
        return ctrlDominio.moveHoraClase(oldHora, oldDia, oldAula, newHora, newDia, newAula);
    }

    public ArrayList<ArrayList<ArrayList<String>>> getHorarioData() {
        return ctrlDominio.getHorarioData();
    }

    public void setPreferences(String language, boolean autosave, int seconds){
        ctrlDominio.setPreferencias(language, autosave, seconds);
    }

    public String[] getPreferences() {
        return  ctrlDominio.getPreferencias();
    }

    public void save(String path) {
        try{
            ctrlDominio.save(path);
        } catch (IOException E){gestionError(Lang.getString("errorguardando"));
        } catch (Exception E){
            gestionError(Lang.getString("errorguardando"));
            E.printStackTrace();
        }
    }

    public void importTitulacion(String path, boolean force) throws Exception {
        ctrlDominio.importTitulacion(path, force);
    }

    public void importAula(String path, boolean force) throws Exception {
        ctrlDominio.importAula(path, force);
    }

    public void exportaTitulacion(String path) throws Exception {
        ctrlDominio.exportaTitulacion(path);
    }

    public void exportaAula(String path) throws Exception {
        ctrlDominio.exportaAula(path);
    }


    public void open(String path) {
        try{
            ctrlDominio = ctrlDominio.reset();
            System.gc();
            ctrlDominio.open(path);
            mainwindow.dispose();
            mainwindow = new MainWindow(this);
        } catch (IOException E){gestionError(Lang.getString("errorabriendo"));
        } catch (Exception E){
            gestionError(Lang.getString("errorabriendo"));
            E.printStackTrace();
        }
    }

    public void saveImage(String path, BufferedImage bi) throws IOException {
        ctrlDominio.saveImage(path, bi);
    }

    public void reset() {
        ctrlDominio = ctrlDominio.reset();
        System.gc();
        mainwindow.dispose();
        mainwindow = new MainWindow(this);
    }


    public String getActualPath() { return ctrlDominio.getActualPath(); }

    public MainWindow getMainwindow() {
        return mainwindow;
    }

    public static Color randomPastelColor() {
        double h = Math.random();
        double s = 0.35 + 0.6 * Math.random();
        double l = 0.65 + 0.25 * Math.random();

        double q = l < 0.5 ? (l * (1 + s)) : (l + s - l * s);
        double p = 2 * l - q;
        double r = hue2rgb(p, q, h + 1.0f / 3);
        double g = hue2rgb(p, q, h);
        double b = hue2rgb(p, q, h - 1.0f / 3);

        return new Color((int) Math.floor(r * 255), (int) Math.floor(g * 255), (int) Math.floor(b * 255));
    }

    public static Color getForeground(Color c) {
        double luma = getLuma(c);
        if (luma > THRESHOLD)
            return Color.BLACK;
        else
            return Color.WHITE;
    }

    private static double getLuma(Color c) {
        double r = c.getRed() / 255.0;
        r = r <= 0.03928 ? r/12.92 : Math.pow((r+0.055)/1.055, 2.4);
        double g = c.getGreen() / 255.0;
        g = g <= 0.03928 ? g/12.92 : Math.pow((g+0.055)/1.055, 2.4);
        double b = c.getBlue() / 255.0;
        b = b <= 0.03928 ? b/12.92 : Math.pow((b+0.055)/1.055, 2.4);
        return 0.2126 * r + 0.7152 * g + 0.0722 * b;
    }

    private static double hue2rgb(double p, double q, double h) {
        if (h < 0)
            h += 1;
        if (h > 1)
            h -= 1;

        if (6 * h < 1)
            return p + ((q - p) * 6 * h);

        if (2 * h < 1)
            return q;

        if (3 * h < 2)
            return p + ((q - p) * 6 * ((2.0f / 3.0f) - h));

        return p;
    }
}
