import javax.swing.*;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Array;

public class HorarioCellRenderer extends AbstractCellEditor implements TableCellRenderer, TableCellEditor{

    CtrlPresentacion ctrl;

    public HorarioCellRenderer(CtrlPresentacion ctrl) {
        this.ctrl = ctrl;
    }

    @Override
    public Component getTableCellRendererComponent(JTable jTable, Object o, boolean b, boolean b1, int i, int i1) {
        return getList((String[]) o, b, jTable, i, i1);
    }

    @Override
    public Component getTableCellEditorComponent(JTable jTable, Object o, boolean b, int i, int i1) {
        return getList((String[]) o, true, jTable, i, i1);
    }

    private JPanel getList(String[] ass, boolean isSelected, JTable table, int row, int col) {
        JPanel panel = new JPanel();
       panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        for (int i = 0; i < ass.length; i+=7) {
            JLabel l = new JLabel("<html><div style='text-align: center;'>" + ass[i].split(":")[0] + " - " + ass[i+2] +
                    "<br>#" + String.join(" #", ass[i+4].split(":")) +
                    "<br>" + ass[i+3] + "</div></html>", SwingConstants.CENTER);
            l.setToolTipText(ass[i+1]);
            l.setOpaque(true);
            l.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
            Color c = Color.decode(ass[i+5]);
            l.setBackground(c);
            l.setForeground(CtrlPresentacion.getForeground(c));

            int finalI = i;
            if (!ass[i+6].equals("-1")) {
                l.setTransferHandler(new TransferHandler() {

                    public int getSourceActions(JComponent c) {
                        return MOVE;
                    }

                    protected Transferable createTransferable(JComponent c) {
                        JLabel label = (JLabel) c;

                        // Formato: dia(int), hora(numero, diferencia con horamin), aula
                        StringBuilder s = new StringBuilder();
                        s.append(col).append(",").append(row).append(",").append(ass[finalI + 3]);

                        return new StringSelection(s.toString());
                    }
                });

                l.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mousePressed(MouseEvent mouseEvent) {
                        if (mouseEvent.isPopupTrigger()) {
                            popup(mouseEvent);
                        } else {
                            TransferHandler th = l.getTransferHandler();
                            th.exportAsDrag(l, mouseEvent, th.MOVE);
                        }
                    }

                    @Override
                    public void mouseReleased(MouseEvent mouseEvent) {
                        if (mouseEvent.isPopupTrigger())
                            popup(mouseEvent);
                    }

                    public void popup(MouseEvent e) {
                        JPopupMenu menu = new JPopupMenu();
                        JMenuItem item = new JMenuItem(Lang.getString("moveraula"));
                        menu.add(item);
                        item.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent actionEvent) {
                                String[] aulas = ((HorarioTableModel) table.getModel()).getAulas();
                                String s = (String)JOptionPane.showInputDialog(null, Lang.getString("moveraula"),
                                        Lang.getString("movera"), JOptionPane.PLAIN_MESSAGE, null, aulas, aulas[0]);
                                ctrl.moveHoraClase(row, col-1, ass[finalI + 3], row, col-1, s);
                                ((HorarioTable) table).reset();
                            }
                        });
                        menu.show(e.getComponent(), e.getX(), e.getY());
                    }
                });
            }
            panel.add(l);
            if (i != ass.length - 5)
                panel.add(Box.createVerticalStrut(5));
        }
        panel.setBorder(BorderFactory.createEmptyBorder(10, 5, 10, 5));
        if (isSelected)
            panel.setBackground(table.getSelectionBackground());
        else
            panel.setBackground(table.getBackground());
        return panel;
    }



    @Override
    public Object getCellEditorValue() {
        return null;
    }
}
