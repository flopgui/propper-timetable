import java.util.Random;

/**
 * Clase que representa un bloque horario, es decir, representa una combinación de {@link DiaSemana}
 * y una hora.
 */
public class BloqueHorario {

    /**
     * El {@link DiaSemana} en el cual se sitúa el {@link BloqueHorario}.
     */
    private DiaSemana ds;
    
    /**
     * La hora de inicio del {@link BloqueHorario}.
     */
    private int horaInicio;

    /**
     * Constructor del {@link BloqueHorario}.
     * @param ds El {@link DiaSemana} del bloque horario.
     * @param horaInicio Hora de inicio.
     */
    public BloqueHorario(DiaSemana ds, int horaInicio) {
    	this.horaInicio = horaInicio;
    	this.ds = ds;
    }
    
     /**
      * Constructor de un {@link BloqueHorario} aleatorio.
      * @param rnd Instancia de {@link java.util.Random} para generar números aleatorios.
      * @param horaMin Hora mínima de inicio de una clase.
      * @param horaMax Hora máxima de finalización de una clase.
      */
    public BloqueHorario(Random rnd, int horaMin, int horaMax) {
    	this.horaInicio = rnd.nextInt(horaMax-horaMin)+horaMin;
    	this.ds = DiaSemana.randomDia(rnd);
    }

	/**
	 * Comparador de la clase {@link BloqueHorario}.
	 * @param o Objeto con el que comparar.
	 * @return Devuelve cierto si {@code o} es un {@link BloqueHorario} que empieza
	 * en el mismo {@link DiaSemana} a la misma hora.
	 */
	@Override
	public boolean equals(Object o) {
    	if (!(o instanceof  BloqueHorario))
    		return false;
    	BloqueHorario other = (BloqueHorario) o;
		return (ds.equals(other.ds) && horaInicio == other.horaInicio);
	}
    
	/**
	 * @return Devuelve el {@link DiaSemana}.
	 */
	public DiaSemana getDs() {
		return ds;
	}

	/**
     * Cambia el {@link DiaSemana} del {@link BloqueHorario}.
	 * @param ds El nuevo dia de la semana.
	 */
	public void setDs(DiaSemana ds) {
		this.ds = ds;
	}

	/**
	 * @return Devuelve la horaInicio.
	 */
	public int getHoraInicio() {
		return horaInicio;
	}

	/**
     * Cambia la hora de inicio del {@link BloqueHorario}
	 * @param horaInicio El nuevo horaInicio.
	 */
	public void setHoraInicio(int horaInicio) {
		this.horaInicio = horaInicio;
	}
    
    
    
}
