import java.util.ArrayList;

/**
 * Clase que representa una hora de clase, es decir, la impartición de una {@link Sesion}. Una hora de clase
 * tiene un listado de grupos que asisten y la sesión que se imparte (p. e.
 * clase de 2 horas de teoría a la que acudirán el grupo 21 y el grupo 22). No confundir con {@link Sesion}.
 */
public class HoraClase {
	
    /**
     * Duración de la {@link HoraClase}.
     */
    private int duracion;

    /**
     * Lista de {@link Grupo}s que asisten a la {@link HoraClase}.
     */
    private ArrayList<Grupo> grupos;

    /**
     * {@link Sesion} a la que pertenece la {@link HoraClase}.
     */
    private Sesion sesion;

    /**
     * Constructor básico.
     * @param sesion La {@link Sesion} que se impartirá.
     */
    public HoraClase(Sesion sesion, ArrayList<Grupo> grupos) {
    	this.sesion = sesion;
    	this.duracion = sesion.getDuracion();
        this.grupos = grupos;
    }
        
    /**
     * @return Devuelve la duración de la {@link HoraClase}.
     */
    public int getDuracion() {
        return duracion;
    }

    /**
     * @return Devuelve la {@link Sesion} que se impartirá durante la {@link HoraClase}.
     */
    public Sesion getSesion() {
	    return sesion;
    }

    public String getGrupos() {
        StringBuilder s = new StringBuilder();
        for (Grupo g : grupos)
            s.append(g.getNombre()).append(":");
        return s.toString();
    }

}
