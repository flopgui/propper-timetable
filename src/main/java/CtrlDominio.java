import com.google.gson.Gson;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.BlockingQueue;
/**
 * Controllador de la capa de Dominio.

 */
public class  CtrlDominio {

    /**
     * Instancia de la clase para poder acceder de forma estática.
     */
    private static CtrlDominio instance;

    private String path;

    /**
     * Número de generaciones que tendrá que realizar el algoritmo.
     */
    private static final int MAX_GENERACIONES = 2500;

    /**
     * Tamaño de la población que tendrá el algoritmo genético.
     */
    private static final int POPULATION_SIZE = 400;

    /**
     * La {@link UnidadDocente} que se está manejando.
     */
    private UnidadDocente ud;

    /**
     * El mejor {@link Horario} encontrado hasta la fecha.
     */
    private Horario hr;

    /**
     * Clase del tipo {@link Algoritmo} para poder generar {@link Horario}s.
     */
    private Algoritmo alg;

    /**
     * Instancia de {@link java.util.Random}, para generar número aleatorios.
     */
    private Random random;

    /**
     * Lista de todos los {@link Recurso}s introduciodos hasta la fecha.
     */
    private ArrayList<Recurso> recursos;

    Timer autosavetimer;

    private class Preferencias {
        public Lang.Language language;
        public boolean autosave;
        public int seconds;
        public Preferencias(Lang.Language l, boolean b, int i){ language = l; autosave = b; seconds = i; }
    }

    private Preferencias prefs;

    /**
     * @return Devuleve la instacia de sí mismo.
     */
    public static CtrlDominio getCtrlDominio() {
        if (instance == null)
            instance = new CtrlDominio("", 8, 20); //TODO
        return instance;
    }

    /**
     * Constructor básico, que construye la {@link UnidadDocente}.
     * @param name Nombre de la {@link UnidadDocente}.
     * @param min Hora mínima a la que se puede empezar las clases en la {@link UnidadDocente}.
     * @param max Horar máxima hasta la cual se pueden hacer las clases en la {@link UnidadDocente}.
     */
    private CtrlDominio(String name, int min, int max) {
        //instance = this;
        random = new Random();
        ud = new UnidadDocente(name, min, max);
        alg = new Algoritmo(random);
        hr = null;
        recursos = new ArrayList<Recurso>();
        autosavetimer = new Timer();
        path = null;
//        if (CtrlPersistencia.existsAutosaved()) {
//            try {
//                openAutosaved();
//            }
//            catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        if (!loadPreferencias()) {
            setPreferencias("Català",true,60);
        }
    }

    /**
     * Constructor básico, que construye la {@link UnidadDocente}.
     * @param name Nombre de la {@link UnidadDocente}.
     * @param min Hora mínima a la que se puede empezar las clases en la {@link UnidadDocente}.
     * @param max Horar máxima hasta la cual se pueden hacer las clases en la {@link UnidadDocente}.
     * @param seed Semilla a utilizar para el generador de números aleatorios.
     */
    private CtrlDominio(String name, int min, int max, long seed) {
        instance = this;
        random = new Random(seed);
        ud = new UnidadDocente(name, min, max);
        alg = new Algoritmo(random);
        hr = null;
        recursos = new ArrayList<Recurso>();
    }

    /**
     * Constructor que toma texto serializado y lo convierte un datos.
     * @param ud String que contiene una {@link UnidadDocente} serializadad.
     * @param horario String que contiene un {@link Horario} serializado.
     * @param recursos String que contiene una lista de  {@link Recurso}s serializada.
     * @param seed Semilla a utilizar para el generador de números aleatorios.
     */
    private CtrlDominio(String ud, String horario, String recursos[], long seed) {
        Gson gson = new Gson();
        this.ud = gson.fromJson(ud, UnidadDocente.class);
        this.hr = gson.fromJson(horario, Horario.class);
        random = new Random(seed);
        alg = new Algoritmo(random);
        this.recursos = new ArrayList<Recurso>();
        for (int i = 0; i < recursos.length; i++) {
            this.recursos.add(new Recurso(recursos[i]));
        }
    }

    /**
     * Cambia el nombre de la {@link UnidadDocente}.
     * @param name Nuevo nombre.
     */
    public void renameUnidadDocente(String name) {
        ud.setNombre(name);
    }

    public CtrlDominio reset() {
        CtrlPersistencia.deleteAutosaved();
        instance = new CtrlDominio("", 8, 20);
        instance.setPreferencias(prefs.language.toString(), prefs.autosave, prefs.seconds);
        return instance;
    }

    public void init() {
        if (!loadPreferencias())
            setPreferencias("Català", true, 60);
    }

    public int getHoraMin() {
        return ud.getHoraMin();
    }

    public int getHoraMax() {
        return ud.getHoraMax();
    }

    /**
     * Cambia el rango de horas lectivas de la {@link UnidadDocente}.
     * @param min Hora mínima a la que se pueden comenzar las clases.
     * @param max Hora máxima a la que se pueden acabar las clases.
     */
    public void changeHorasLectivas(int min, int max) {
        ud.setHoraMin(min);
        ud.setHoraMax(max);
    }

    /**
     * Añade un {@link Aula} con nombre {@code name}, y capacidad {@code capacidad}.
     * @param name Nombre del {@link Aula}.
     * @param capacidad Capacidad del {@link Aula}.
     * @return Devuelve {@code true} sii no existía ya un {@link Aula} con el mismo nombre.
     */
    public boolean addAula(String name, int capacidad) {
        Aula a = new Aula(name, capacidad);
        hr = null;
        return ud.addAula(a);
    }

    /**
     * Cambia la capacidad de un {@link Aula}.
     * @param name Nombre del {@link Aula}.
     * @param newCapacidad Nueva cacpacidad.
     */
    public void changeCapacidadAula(String name, int newCapacidad) {
        ud.getAula(name).setCapacidad(newCapacidad);
    }

    /**
     * Elimina un {@link Aula} de la {@link UnidadDocente}.
     * @param aula Nombre del {@link Aula} a eliminar.
     * @return Devuelve {@code true} si el {@link Aula} ha sido eliminada, es decir, si existía.
     */
    public boolean deleteAula(String aula) {
        hr=null; return ud.removeAula(aula);
    }

    /**
     * Cambia el nombre de un {@link Aula} de la {@link UnidadDocente}.
     * @param oldName Nombre viejo del {@link Aula}.
     * @param newName Nombre nuevo del {@link Aula}.
     * @return Devuelve {@code true} si la operación se ha podido realizar y {@code false} si falla debido a
     * que ya hay un {@link Aula} con el nombre {@code newName}.
     */
    public boolean renameAula(String oldName, String newName) {
        if (ud.getAula(newName) != null)
            return false;
        ud.getAula(oldName).setNombre(newName);
        return true;
    }

    /**
     * @return Numero de {@link Aula} de la {@link UnidadDocente}.
     */
    public int getNumeroAulas() {
        return ud.getNumeroAulas();
    }

    /**
     * @param i Indice
     * @return Nombres del {@link Aula} i de la {@link UnidadDocente}.
     */
    public String getNombreAula(int i) {
        return ud.getAula(i).getNombre();
    }

    public String[] getAulas() {
        String r[] = new String[ud.getNumeroAulas()];
        for (int i = 0; i < ud.getNumeroAulas(); i++)
            r[i] = ud.getAula(i).getNombre();
        return r;
    }

    /**
     * @param i Indice
     * @return Capacidad del {@link Aula} i de la {@link UnidadDocente}.
     */
    public int getCapacidadAula(int i) {
        return ud.getAula(i).getCapacidad();
    }

    /**
     * Cambia el nombre del {@link Aula} i de la {@link UnidadDocente}.
     * @param i Indice
     * @param s Nuevo nombre
     */
    public void setNombreAula(int i, String s) {
        ud.getAula(i).setNombre(s);
    }

    /**
     * Cambia la capacidad de un {@link Aula} de la {@link UnidadDocente}.
     * @param s Nombre del aula
     * @param n Nueva capacidad
     */
    public void setCapacidadAula(String s, int n) {
        hr = null;
        ud.getAula(s).setCapacidad(n);
    }

    /**
     * @return Devuelve una lista de todos los recursos definidos hasta la fecha.
     */
    public String[] getRecursos() {
        String r[] = new String[recursos.size()];
        for (int i = 0; i < recursos.size(); i++) {
            r[i] = recursos.get(i).getNombre();
        }
        return r;
    }

    public void changenameAula(String oldname, String newname){ ud.getAula(oldname).setNombre(newname); }

    /**
     * Añade un {@link Recurso} a un {@link Aula}.
     * @param aula Nombre del {@link Aula}.
     * @param recurso Nombre del {@link Recurso}.
     * @return Devuelve {@code true} si el {@link Aula} no tenía ya un {@link Recurso} con el mismo nombre.
     */
    public boolean addRecursoAula(String aula, String recurso) {
        Recurso rec= null;
        for (Recurso r : recursos) {
            if (recurso.equals(r.getNombre()))
                rec = r;
        }
        if (rec == null) {
            rec = new Recurso(recurso);
            recursos.add(rec);
        }
        return ud.getAula(aula).addRecurso(rec);
    }

    public String[] getRecursosAula(String s) {
        String[] rec = new String[ud.getAula(s).getNumeroRecursos()];
        for (int i=0; i<rec.length; ++i) {
            rec[i] = ud.getAula(s).getRecurso(i).getNombre();
        }
        return rec;
    }

    public boolean hasAula(String s){ return (ud.getAula(s)!=null);}

    /**
     * Elimina un {@link Recurso} de un {@link Aula}.
     * @param aula Nombre del {@link Aula} de la que se elimina el {@link Recurso}.
     * @param recurso Nombre del {@link Recurso} que se elimina.
     */
    public boolean removeRecursoAula(String aula, String recurso) {
        return ud.getAula(aula).removeRecurso(recurso);
    }

    public void renameRecurso(String oldname, String newname) {
        recursos.get(recursos.indexOf(new Recurso(oldname))).setNombre(newname);
    }

    /**
     * Añade una {@link Titulacion} a la {@link UnidadDocente}.
     * @param name Nombre de la {@link Titulacion} a añadir.
     * @return Devuelve {@code true} si la {@link Titulacion} se ha añadido, es decir, si no existía previamente
     * una {@link Titulacion} con el mismo nombre.
     */
    public boolean addTitulacion(String name) {
        hr = null;
        Titulacion t = new Titulacion(name);
        return ud.addTitulacio(t);
    }

    /**
     * Cambia el nombre de una {@link Titulacion}.
     * @param oldName Nombre viejo de la {@link Titulacion}.
     * @param newName Nombre nuevo de la {@link Titulacion}.
     * @return Devuelve {@code true} si se ha podido cambiar el nombre de la {@link Titulacion} y {@code false} si ha
     * ocurrido un error como que ya exista una {@link Titulacion} con nombre {@code newName}.
     */
    public boolean renameTitulacion(String oldName, String newName) {
        if (ud.getTitulacion(newName) != null)
            return false;
        ud.getTitulacion(oldName).setNombre(newName);
        return true;
    }

    /**
     * @return Lista de nombres de las {@link Titulacion}
     */
    public String[] getNombresTitulaciones() {
        String[] nt = new String[ud.getNumeroTitulaciones()];
        for (int i=0; i<nt.length; ++i) {
            nt[i] = ud.getTitulacion(i).getNombre();
        }
        return nt;
    }

    /**
     * Elimina una {@link Titulacion} de la {@link UnidadDocente}.
     * @param name Nombre de la {@link Titulacion} a eliminar.
     * @return Devuelve {@code true} si se ha eliminado, es decir, si la titulación existía.
     */
    public boolean deleteTitulacion(String name) {
        hr = null;
        return ud.removeTitulacion(name);
    }

    public String[] getAsignaturas(String titulacion) {
        Titulacion t = ud.getTitulacion(titulacion);
        String asignaturas[] = new String[t.getNumeroAsignatura()];
        for (int i = 0; i < asignaturas.length; i++)
            asignaturas[i] = t.getAsignatura(i).getSiglas();
        return asignaturas;
    }

    /**
     * Añade una {@link Asignatura} a una {@link Titulacion}.
     * @param titulacion Nombre de la {@link Titulacion}.
     * @param nombre Nombre de la {@link Asignatura}.
     * @param s Siglas de la {@link Asignatura}.
     * @param c Capacidad de un grupo de la {@link Asignatura}.
     * @param n Numero de alumnos de la {@link Asignatura}.
     * @return Devuelve {@code true} si no existía ninguna {@link Asignatura} con las mismas siglas.
     */
    public boolean addAsignatura(String titulacion, String nombre, String s, int c, int n) {
        hr = null;
        Asignatura as = new Asignatura(nombre, s, c, n);
        return ud.getTitulacion(titulacion).addAsignatura(as);
    }

    public boolean addAsignaturaCuatrimestre(String titulacion, int cuatrimestre, String siglas) {
        Asignatura a = ud.getTitulacion(titulacion).getAsignatura(siglas);
        return ud.getTitulacion(titulacion).getCuatrimestre(cuatrimestre).addAsignatura(a);
    }

    public String getCuatrimestreAsignatura(String titulacion, String siglas) {
        Titulacion t = ud.getTitulacion(titulacion);
        for (int i=0; i<t.getNumeroCuatrimestres(); ++i) {
            for (int j=0; j<t.getCuatrimestre(i).getNumeroAsignaturas(); ++j)
            if (t.getCuatrimestre(i).getAsignatura(j).equals(t.getAsignatura(siglas))) {
                return t.getCuatrimestre(i).getNombre();
            }
        }
        return null;
    }

    public boolean cloneAsignatura(String titulacion, String siglas, String newsiglas) {
        Asignatura olda = ud.getTitulacion(titulacion).getAsignatura(siglas);
        Asignatura a = olda.clone();
        a.setSiglas(newsiglas);
        a.setNombre(newsiglas);
        return ud.getTitulacion(titulacion).addAsignatura(a);
    }

    /**
     * Elimina una {@link Asignatura} de una {@link Titulacion}.
     * @param titulacion Nombre de la {@link Titulacion}.
     * @param siglas Siglas de la {@link Asignatura} a eliminar.
     * @return Devuelve {@code true} si existía una {@link Asignatura} con las siglas {@code siglas}.
     */
    public boolean deleteAsignatura(String titulacion, String siglas) {
        hr = null;
        for (int i=0; i<ud.getTitulacion(titulacion).getNumeroCuatrimestres(); ++i) {
            ud.getTitulacion(titulacion).getCuatrimestre(i).removeAsignatura(siglas);
        }
        return ud.getTitulacion(titulacion).removeAsignatura(siglas);
    }

    public boolean removeAsignaturaCuatrimestre(String titulacion, int cuatrimestre, String siglas) {
        return ud.getTitulacion(titulacion).getCuatrimestre(cuatrimestre).removeAsignatura(siglas);
    }

    /**
     * Cambia una {@link Asignatura} de {@link Titulacion}-
     * @param oldTitulacion Nombre de la {@link Titulacion} vieja.
     * @param newTitulacion Nombre de la {@link Titulacion} nueva.
     * @param siglas Siglas de la {@link Asignatura}.
     * @return Devuelve {@code true} si la {@link Asignatura} se ha podido cambiar.
     */
    public boolean moveAsignatura(String oldTitulacion, String newTitulacion, String siglas) {
        Asignatura as = ud.getTitulacion(oldTitulacion).getAsignatura(siglas);
        if (ud.getTitulacion(newTitulacion).addAsignatura(as)) {
            ud.getTitulacion(oldTitulacion).removeAsignatura(siglas);
            return true;
        }
        return false;
    }

    public boolean moveAsignaturaCuatrimestre(String titulacion, int oldCuatri, int newCuatri, String siglas) {
        Asignatura as = ud.getTitulacion(titulacion).getAsignatura(siglas);
        if (ud.getTitulacion(titulacion).getCuatrimestre(oldCuatri).removeAsignatura(siglas)) {
            ud.getTitulacion(titulacion).getCuatrimestre(newCuatri).addAsignatura(as);
            return true;
        }
        return false;
    }


    public String getNombreAsignatura(String titulacion, String siglas) {
        return ud.getTitulacion(titulacion).getAsignatura(siglas).getNombre();
    }

    public int getNumeroGruposAsignatura(String titulacion, String siglas) {
        return ud.getTitulacion(titulacion).getAsignatura(siglas).getNumeroGrupos();
    }

    public int getCapacidadGrupoAsignatura(String titulacion, String siglas) {
        return ud.getTitulacion(titulacion).getAsignatura(siglas).getCapacidadGrupo();
    }

    public Color getColorAsignatura(String titulacion, String asignatura) {
        return ud.getTitulacion(titulacion).getAsignatura(asignatura).getColor();
    }

    public void setColorAsignatura(String titulacion, String asignatura, Color color) {
        ud.getTitulacion(titulacion).getAsignatura(asignatura).setColor(color);
    }

    /**
     * Cambia el Nombre de una {@link Asignatura}.
     * @param titulacion Nombre de la {@link Titulacion} en la que está la {@link Asignatura}.
     * @param siglas Sigals de la {@link Asignatura}.
     * @param newName Nuevo nombre de la {@link Asignatura}.
     */
    public void setNombreAsignatura(String titulacion, String siglas, String newName) {
        Asignatura a = ud.getTitulacion(titulacion).getAsignatura(siglas);
        a.setNombre(newName);
        ud.getTitulacion(titulacion).getAsignatura(siglas).setNombre(newName);
    }

    /**
     * Cambia el número de grupos de una {@link Asignatura}.
      * @param titulacion Nombre de la {@link Titulacion} de la {@link Asignatura}.
     * @param siglas Siglas de la {@link Asignatura}.
     * @param p Nuevo número de alumnos de la {@link Asignatura}.
     */
    public void setNumeroAlumnosAsignatura(String titulacion, String siglas, int p) {
        ud.getTitulacion(titulacion).getAsignatura(siglas).setNumeroAlumnos(p);
    }

    public int getNumeroAlumnosAsignatura(String titulacion, String siglas) {
        return ud.getTitulacion(titulacion).getAsignatura(siglas).getNumeroAlumnos();
    }

    /**
     * Cambia las siglas de una {@link Asignatura}.
     * @param titulacion Nombre de la {@link Titulacion} de la {@link Asignatura}.
     * @param oldSiglas Viejas siglas de la {@link Asignatura}.
     * @param newSiglas Nuevas siglas de la {@link Asignatura}.
     * @return Devuelve {@code true} si se ha podido realizar el cambio de siglas.
     */
    public boolean setSiglasAsignatura(String titulacion, String oldSiglas, String newSiglas) {
        Asignatura a = ud.getTitulacion(titulacion).getAsignatura(newSiglas);
        if (a != null)
            return false;
        ud.getTitulacion(titulacion).getAsignatura(oldSiglas).setSiglas(newSiglas);
        return true;
    }

    /**
     * Cambia la capacidad de un grupo de una {@link Asignatura}.
     * @param titulacion Nombre de la {@link Titulacion} de la {@link Asignatura}.
     * @param siglas Siglas de la {@link Asignatura}.
     * @param p Nueva capacidad de un grupo de la {@link Asignatura}.
     */
    public void setCapacidadGrupoAsignatura(String titulacion, String siglas, int p) {
        ud.getTitulacion(titulacion).getAsignatura(siglas).setCapacidadGrupo(p);
    }

    public String[] getNombresCuatrimestres(String titulacion) {
        Titulacion t = ud.getTitulacion(titulacion);
        String[] n = new String[t.getNumeroCuatrimestres()];
        for (int i=0; i<n.length; ++i) {
            n[i] = t.getCuatrimestre(i).getNombre();
        }
        return n;
    }

    public boolean addCuatrimestre(String titulacion, String nombre) {
        Cuatrimestre c = new Cuatrimestre(nombre);
        return ud.getTitulacion(titulacion).addCuatrimestre(c);
    }

    public boolean removeCuatrimestre(String titulacion, String nombre) {
        hr = null;
        Cuatrimestre c = ud.getTitulacion(titulacion).getCuatrimestre(nombre);
        for (int i=0; i<c.getNumeroAsignaturas(); ++i) {
            Asignatura a = c.getAsignatura(i);
            c.removeAsignatura(a.getNombre());
            ud.getTitulacion(titulacion).removeAsignatura(a.getNombre());
        }
        return ud.getTitulacion(titulacion).removeCuatrimestre(nombre);
    }

    public String[] getSiglasAsignaturasCuatrimestre(String titulacion, int nc) {
        Cuatrimestre c = ud.getTitulacion(titulacion).getCuatrimestre(nc);
        String[] n = new String[c.getNumeroAsignaturas()];
        for (int i=0; i<n.length; ++i) {
            n[i] = c.getAsignatura(i).getSiglas();
        }
        return n;
    }

    public String[] getSesiones(String titulacion, String asignatura) {
        Asignatura a = ud.getTitulacion(titulacion).getAsignatura(asignatura);
        String[] s = new String[a.getSesiones().size()];
        for (int i=0; i<s.length; ++i) {
            s[i] = a.getSesiones().get(i).getNombre();
        }
        return s;
    }

    /**
     * Añade una {@link Sesion} a una {@link Asignatura}.
     * @param titulacion Nombre de la {@link Titulacion} de la {@link Asignatura}.
     * @param asignatura Siglas de la {@link Asignatura}.
     * @param nombre Nombre de la {@link Sesion}.
     * @param duracion Duración de la {@link Sesion}.
     * @param nGrupos Número de grupos máximo que pueden asistir a la {@link Sesion}.
     */
    public boolean addSesion(String titulacion, String asignatura, String nombre, int duracion, int nGrupos, String[] recs) {
        hr = null;
        Asignatura a = ud.getTitulacion(titulacion).getAsignatura(asignatura);
        Sesion s = new Sesion(nombre,titulacion,a,duracion,nGrupos);
        for (String rec : recs) {
            Recurso recurso = null;
            for (Recurso r : recursos) {
                if (rec.equals(r.getNombre()))
                    recurso = r;
            }
            if (recurso == null) {
                recurso = new Recurso(rec);
                recursos.add(recurso);
            }
            s.addRecurso(recurso);
        }
        return a.addSesion(s);
    }

    public boolean removeSesion(String titulacion, String asignatura, String nombre) {
        hr = null;
        return ud.getTitulacion(titulacion).getAsignatura(asignatura).deleteSesion(nombre);
    }

    public void renameSesion(String titulacion, String asignatura, String sesion, String newname) {
        ud.getTitulacion(titulacion).getAsignatura(asignatura).getSesion(sesion).setNombre(newname);
    }

    public int getDuracionSesion(String titulacion, String asignatura, String sesion) {
        return ud.getTitulacion(titulacion).getAsignatura(asignatura).getSesion(sesion).getDuracion();
    }

    public int getNGruposSesion(String titulacion, String asignatura, String sesion) {
        return ud.getTitulacion(titulacion).getAsignatura(asignatura).getSesion(sesion).getnGrupos();
    }

    public String getRecursosSesion(String titulacion, String asignatura, String sesion)  {
        ArrayList<Recurso> res = ud.getTitulacion(titulacion).getAsignatura(asignatura).getSesion(sesion).getRecursos();
        String s = "";
        for (Recurso r : res)
            s += r.getNombre() + ",";
        return s.equals("") ? s : s.substring(0, s.length()-1);
    }

    public void setDuracionSesion(String titulacion, String asignatura, String sesion, int duracion) {
        hr = null;
        ud.getTitulacion(titulacion).getAsignatura(asignatura).getSesion(sesion).setDuracion(duracion);
    }

    public void setNGruposSesion(String titulacion, String asignatura, String sesion, int n) {
        hr = null;
        ud.getTitulacion(titulacion).getAsignatura(asignatura).getSesion(sesion).setnGrupos(n);
    }


    /**
     * @return Devuleve una lista de las {@link Restriccion}es del {@link Algoritmo}.
     */
    public ArrayList<Restriccion> getRestriccionesAlgoritmo() {
        return alg.getRestriccionesAL();
    }

    /**
     * @return Devuelve una lista de todos los nombres de las {@link Restriccion}es.
     */
    public String[] getRestricciones() {
        Restriccion restricciones[] = alg.getRestricciones();
        String r[] = new String[restricciones.length];
        for (int i = 0; i < restricciones.length; i++) {
            r[i] = restricciones[i].getNombre();
        }
        return r;
    }

    /**
     * Consulta la descripción de una {@link Restriccion}.
     * @param nombre Nombre de la {@link Restriccion}.
     * @return Devuelve la descripcción de la {@link Restriccion}.
     */
    public double getRestriccionPeso(String nombre) {
        Restriccion restricciones[] = alg.getRestricciones();
        for (int i = 0; i < restricciones.length; i++) {
            if (restricciones[i].getNombre().equals(nombre))
                return restricciones[i].getPeso();
        }
        return -1.0d;
    }

    public void setRestriccionPeso(String r, double p) {
        alg.setRestriccionPeso(r, p);
    }

    public void setRestriccionActiva(String s, boolean b) {
        alg.setRestriccionActive(s, b);
    }

    /**
     * Genera un {@link Horario} tratando de ajustarse a las restricciones que estén activas
     * con los datos introducidos hasta el momento.
     * @return Devuelve true si se ha podido generar un horario.
     */
    public int generaHorario(BlockingQueue<Integer> blockingDeque) {
        ArrayList<Sesion> s = new ArrayList<>();
        for (int i = 0; i < ud.getNumeroTitulaciones(); i++) {
            for (int j = 0; j < ud.getTitulacion(i).getNumeroAsignatura(); j++) {
                s.addAll(ud.getTitulacion(i).getAsignatura(j).getSesiones());
            }
        }
        ArrayList<HoraClase> hca = new ArrayList<HoraClase>();
        for (int i = 0; i < s.size(); i++) {
            int amount = (int) Math.ceil(((double) s.get(i).getAsignatura().getNumeroGrupos()) / ((double) s.get(i).getnGrupos()));
            int ngrupo = 1;
            for (int j=0; j<amount; ++j) {
                ArrayList<Grupo> grupos = new ArrayList<>();
                for (int k = 0; k < s.get(i).getnGrupos(); k++) { grupos.add(new Grupo(Integer.toString(ngrupo++)));
                }
                hca.add(new HoraClase(s.get(i), grupos));
            }
        }

        ArrayList<BloqueET> beta = new ArrayList<BloqueET>();
        for (DiaSemana d: DiaSemana.values()) {
            for (int h = ud.getHoraMin(); h < ud.getHoraMax(); ++h) {
                BloqueHorario b = new BloqueHorario(d,h);
                for (int i = 0; i < ud.getNumeroAulas(); ++i) {
                    beta.add(new BloqueET(ud.getAula(i), b, 1));
                }
            }
        }
        if (beta.size() < hca.size()) return -1;
        while (hca.size() < beta.size()) {
            hca.add(null);
        }

        BloqueET[] bet = beta.toArray(new BloqueET[beta.size()]);
        Horario hrs[] = new Horario[POPULATION_SIZE];
        for (int i = 0; i < POPULATION_SIZE; i++) {
            Collections.shuffle(hca);
            HoraClase[] hc = hca.toArray(new HoraClase[hca.size()]);
            hrs[i] = new Horario(bet, hc);
        }

        hr = alg.generate(hrs, MAX_GENERACIONES, blockingDeque);
        return hr == null ? 0 : 1;
    }

    /**
     * @return Devuelve la {@link UnidadDocente}.
     */
    public UnidadDocente getUnidadDocente() {
        return ud;
    }

    /**
     * @return Devuelve el nombre de la {@link UnidadDocente}.
     */
    public String getNombreUnidadDocente() {
        return ud.getNombre();
    }

    /**
     * Genera un {@link Aula} aleatoria entre todas las posibles.
     * @return Devuelve un {@link Aula} aleatoria.
     */
    public Aula randomAula() {

        return ud.getAula(random.nextInt(ud.getNumeroAulas()));
    }

    public boolean moveHoraClase(int oldHora, int oldDia, String oldAula, int newHora, int newDia, String newAula) {
	        return hr.swapAsignacion(oldHora+ud.getHoraMin(), oldDia, ud.getAula(oldAula), newHora+ud.getHoraMin(), newDia, ud.getAula(newAula));
    }

    public ArrayList<ArrayList<ArrayList<String>>> getHorarioData() {

        ArrayList<ArrayList<ArrayList<String>>> a = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            a.add(new ArrayList<>());
            for (int j = 0; j < ud.getHoraMax() - ud.getHoraMin(); j++) {
                a.get(i).add(new ArrayList<String>());
            }
        }
        for (Map.Entry<BloqueET, HoraClase> entry : hr.getAsignaciones().entrySet()) {
            if (entry.getValue() == null) continue;
            // siglas:titulacion, nombreasig, nombresesion, aula, grupos, color, duracion
            String e = entry.getValue().getSesion().getAsignatura().getSiglas() + ":" +
                    getNombreTitulación(entry.getValue().getSesion().getAsignatura()) + "," +
                    entry.getValue().getSesion().getAsignatura().getNombre() + "," +
                    entry.getValue().getSesion().getNombre() + "," + //todo grupos
                    entry.getKey().getAula().getNombre() + "," +
                    entry.getValue().getGrupos() + "," + "#" +
                    Integer.toHexString(entry.getValue().getSesion().getAsignatura().getColor().getRGB()).substring(2);
            ArrayList<ArrayList<String>> tmp = a.get(entry.getKey().getBloqueHorario().getDs().toNumber());
            tmp.get(entry.getKey().getBloqueHorario().getHoraInicio() - ud.getHoraMin()).add(e + "," + entry.getValue().getDuracion());

            for (int i = 1; i < entry.getValue().getDuracion(); i++) {
                tmp.get(entry.getKey().getBloqueHorario().getHoraInicio() + i - ud.getHoraMin()).add(e + ",-1");
            }
        }
        return a;
    }

    private String getNombreTitulación(Asignatura a) {
        for (int  i = 0; i < ud.getNumeroTitulaciones(); i++) {
            Titulacion t = ud.getTitulacion(i);
            if (t.containsAsignatura(a))
                return t.getNombre();
        }
        return "";
    }

    /**
     * Pasa la {@link UnidadDocente} a una {@code String} para su correcto almacenamiento y comunicación.
     * @return Devulve la {@link UnidadDocente} serializada.
     */
    public String serializeUnidadDocente() {
        Gson gson = new Gson();
        return gson.toJson(ud);
    }

    public UnidadDocente deserializeUnidadDocente(String raw){
        Gson gson = new Gson();
        UnidadDocente ud = gson.fromJson(raw, UnidadDocente.class);
        for (int i = 0; i < ud.getNumeroTitulaciones(); i++) {
            Titulacion t = ud.getTitulacion(i);
            for (int j = 0; j < t.getNumeroAsignatura(); j++) {
                t.getAsignatura(j).asociaSesiones();
                for (Sesion ses: t.getAsignatura(j).getSesiones()) {
                    for (Recurso r : ses.getRecursos()) {
                        if (!this.recursos.contains(r))
                            this.recursos.add(r);
                    }
                }
            }
        }
        return ud;
    }

    /**
     * Pasa el mejor {@link Horario} a una {@code String} para su correcto almacenaciento y comunicación.
     * @return Devuelve el {@link Horario} serializado.
     */
    public String[] serializeHorario() {
        Gson gson = new Gson();
        if (!hasHorario()) return new String[]{"null", "null"};
        BloqueET[] bet = hr.getAsignaciones().keySet().toArray(new BloqueET[hr.getAsignaciones().size()]);
        HoraClase[] hc = new HoraClase[bet.length];
        for (int i=0; i<bet.length; ++i) {
            hc[i] = hr.getAsignaciones().get(bet[i]);
        }
        return new String[]{gson.toJson(bet), gson.toJson(hc)};
    }

    public Horario deserializeHorario(String[] hr) {
        Gson gson = new Gson();
        if (hr[0].equals("null")) return null;
        HoraClase[] hc = gson.fromJson(hr[1], HoraClase[].class);
        for (HoraClase h: hc) {
            if (h == null) continue;
            Sesion s = h.getSesion();
            s.setAsignatura(ud.getTitulacion(s.getTitulacion()).getAsignatura(s.getSiglasAsignatura()));
        }
        return new Horario(gson.fromJson(hr[0], BloqueET[].class), hc);
    }

    public boolean hasHorario() {
        return hr != null;
    }

    public int getGenerationCount() {
        return MAX_GENERACIONES;
    }

    public void importTitulacion(String path, boolean force) throws Exception {
        ArrayList<String> s = CtrlPersistencia.openTitulacion(path);
        Gson gson = new Gson();
        if (!force) {
            for (String t : s) {
                Titulacion tit = gson.fromJson(t, Titulacion.class);
                if (ud.getTitulacion(tit.getNombre()) != null)
                    throw new DuplicateFormatFlagsException("Ya existe la titulacion");
            }
        }
        for (String tit : s) {
            Titulacion t = gson.fromJson(tit, Titulacion.class);
            for (int j = 0; j < t.getNumeroAsignatura(); j++) {
                t.getAsignatura(j).asociaSesiones();
                for (Sesion ses: t.getAsignatura(j).getSesiones()) {
                    for (Recurso r : ses.getRecursos()) {
                        if (!this.recursos.contains(r))
                            this.recursos.add(r);
                    }
                }
            }
            ud.removeTitulacion(t.getNombre());
            ud.addTitulacio(t);
        }

    }

    public void exportaTitulacion(String path) throws Exception {
        StringBuilder b = new StringBuilder();
        Gson gson = new Gson();
        for (int i = 0; i < ud.getNumeroTitulaciones(); i++) {
            b.append(gson.toJson(ud.getTitulacion(i))).append("\n");
        }
        CtrlPersistencia.exportTo(path, b.toString());
    }

    public void exportaAula(String path) throws Exception {
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < ud.getNumeroAulas(); i++) {
            b.append(ud.getAula(i).getNombre()).append(",")
                    .append(ud.getAula(i).getCapacidad()).append(",")
                    .append(ud.getAula(i).getRecursos()).append("\n");
        }
        CtrlPersistencia.exportTo(path, b.toString());
    }

    public void importAula(String path, boolean force) throws Exception {
        ArrayList<String> s = CtrlPersistencia.openAula(path);
        if (!force) {
            for (String a : s) {
                String nombre = a.split(",")[0];
                if (ud.getAula(nombre) != null)
                    throw new DuplicateFormatFlagsException("Ya existe el aula");
            }
        }
        for (String a : s) {
            String[] arr = a.split(",");
            String nombre = arr[0];
            int cap = Integer.parseInt(arr[1]);
            ArrayList<Recurso> recs = new ArrayList<Recurso>();
            for (String rec : arr[2].split(":")) {
                boolean found = false;
                for (Recurso r : recursos) {
                    if (r.getNombre().equals(rec)) {
                        found = true;
                        recs.add(r);
                    }
                }
                if (!found) {
                    Recurso r = new Recurso(rec);
                    recs.add(r);
                    recursos.add(r);
                }
            }
            ud.removeAula(nombre);
            ud.addAula(new Aula(nombre, cap, recs));
        }
    }

    public void save(String path) throws IOException, Exception {
        this.path = path;
        CtrlPersistencia.save(path, serializeHorario(), serializeUnidadDocente());
        CtrlPersistencia.deleteAutosaved();
    }

    public boolean existsAutosaved() {
        return CtrlPersistencia.existsAutosaved();
    }

    public void autoSave() throws Exception {
        CtrlPersistencia.autoSave(serializeHorario(), serializeUnidadDocente());
    }

    public void deleteAutosaved() {
        CtrlPersistencia.deleteAutosaved();
    }

    public void openAutosaved() throws Exception {
        String[] s = CtrlPersistencia.openAutosaved();
        deserialize(s);
    }

    public void open(String path) throws Exception {
        String[] s = CtrlPersistencia.open(path);
        deserialize(s);
    }

    public void deserialize(String[] s) throws Exception {
        this.ud = deserializeUnidadDocente(s[0]);
        this.hr = deserializeHorario(new String[]{s[1], s[2]});

        /*for (int i = 0; i < ud.getNumeroAulas(); ++i) {
            Aula a = ud.getAula(i);
            for (int j = 0; j < a.getNumeroRecursos(); ++i) {
                if (!this.recursos.contains(a.getRecurso(j)))
                    this.recursos.add(a.getRecurso(j));
            }
        }*/
        for (String t : ud.getTitulaciones()) {
                for (int i = 0; i < ud.getTitulacion(t).getNumeroAsignatura(); ++i){
                Asignatura a = ud.getTitulacion(t).getAsignatura(i);
                for (Sesion ses: a.getSesiones()) {
                    for (Recurso r : ses.getRecursos()) {
                        if (!this.recursos.contains(r))
                            this.recursos.add(r);
                    }
                }
            }
        }
        this.path = path;
    }

    public void saveImage(String path, BufferedImage bi) throws IOException {
        CtrlPersistencia.saveImage(path, bi);
    }

    public void setPreferencias(String language, boolean autosave, int seconds){
        Lang.Language lang = Lang.Language.fromString(language);
        prefs = new Preferencias(lang, autosave, seconds);
        Lang.changeLang(lang);

        autosavetimer.cancel();
        autosavetimer.purge();
        autosavetimer = new Timer();
        TimerTask autosavetask = new TimerTask() {
            public void run() {
                try {
                    if (prefs.autosave && path != null)
                        save(path);
                    else
                        autoSave();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        autosavetimer.scheduleAtFixedRate(autosavetask, 0, prefs.seconds*1000);
        CtrlPersistencia.savePreferencias(new String[]{language, Boolean.toString(autosave), Integer.toString(seconds)});
    }

    public boolean loadPreferencias() {
        String[] s = CtrlPersistencia.openPreferencias();
        if (s != null) {
            setPreferencias(s[0], Boolean.parseBoolean(s[1]), Integer.parseInt(s[2]));
            return true;
        }
        return false;
    }

    public String[] getPreferencias() {
        return new String[]{prefs.language.toString(), Boolean.toString(prefs.autosave), Integer.toString(prefs.seconds)};
    }

    public String getActualPath() {return path;}

}
