import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;

public class VistaPreferencia extends JDialog implements LangListener{

    private CtrlPresentacion ctrl;
    private JLabel languagechoose, autosave, timeautosave;
    private JComboBox<String> languages;
    private JSpinner time;
    private JCheckBox cb;
    private JButton cancel, accept;

    public VistaPreferencia(CtrlPresentacion ctrl){
        this.ctrl = ctrl;
        Lang.registerListener(this);

        languagechoose = new JLabel();
        autosave = new JLabel();
        timeautosave = new JLabel();
        SpinnerNumberModel snm = new SpinnerNumberModel(20, 0, 600, 1);
        snm.setStepSize(10);
        time = new JSpinner(snm);

        cancel = new JButton();

        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                dispose();
            }
        });

        accept = new JButton();

        JPanel p2 = new JPanel(new FlowLayout());
        p2.add(accept);
        p2.add(cancel);

        ArrayList<String> cho = new ArrayList<String>();
        Lang.Language[] aux = Lang.Language.values();
        for(Lang.Language l : aux) cho.add(l.toString());

        languages = new JComboBox<String>();
        languages.setModel(new DefaultComboBoxModel<String>(cho.toArray(new String[cho.size()])));

        cb = new JCheckBox();
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        c.gridx = 0;
        c.gridy = 0;
        panel.add(languagechoose, c);

        c.gridx = 1;
        c.gridy = 0;
        panel.add(languages, c);

        c.gridx = 0;
        c.gridy = 1;
        panel.add(autosave, c);

        c.gridx = 1;
        panel.add(cb, c);

        c.gridx = 0;
        c.gridy = 2;
        panel.add(timeautosave, c);

        c.gridx = 1;
        c.gridy = 2;
        panel.add(time, c);

        c.anchor = GridBagConstraints.PAGE_END;
        c.gridx = 1;
        c.gridy = 3;
        c.insets = new Insets(10,0,0,0);  //top padding
        panel.add(p2,c);

        this.setSize(400,130);
        this.add(panel);

        String[] prefs = ctrl.getPreferences();
        time.setValue(Integer.parseInt(prefs[2]));
        cb.setSelected(Boolean.parseBoolean(prefs[1]));
        languages.setSelectedItem(prefs[0]);

        accept.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    String lan = (String) languages.getSelectedItem();
                    int i = (Integer) time.getValue();
                    if ((Integer) time.getValue() < 1) {
                        throw new IllegalArgumentException("");
                    }
                    ctrl.setPreferences(lan, cb.isSelected(), i);
                    dispose();
                } catch (IllegalArgumentException e) {
                    ctrl.gestionError(Lang.getString("errorargumento"));
                } catch (Exception e) {
                    ctrl.gestionError(Lang.getString("errorinesperado"));
                }
            }
        });
        onLanguageChanged();
    }

    @Override
    public void onLanguageChanged() {
        timeautosave.setText(Lang.getString("tiempoautoguardado"));
        autosave.setText(Lang.getString("autoguardado"));
        languagechoose.setText(Lang.getString("lengua"));
        this.setTitle(Lang.getString("preferencias"));
        accept.setText(Lang.getString("confirmar"));
        cancel.setText(Lang.getString("cancelar"));
    }
}
