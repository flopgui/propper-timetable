import java.util.Random;

/**
 * Clase que representa un bloque espacio-temporal, es decir, una combinación de un {@link BloqueHorario} y de
 * un {@link Aula}.
 */
public class  BloqueET {
	
	/**
	 * El {@link BloqueHorario} del {@link BloqueET}.
	 */
	private BloqueHorario bloqueHorario;
	
	/**
	 * El {@link Aula} del {@link BloqueET}.
	 */
	private Aula aula;

	/**
	 * Duración del {@link BloqueET}.
	 */
	private int duracion;
	
	/**
	 * Constructor de {@link BloqueET}.
	 * @param aula Nombre del {@link Aula} del {@link BloqueET}.
	 * @param bloqueHorario El {@link BloqueHorario} del {@link BloqueET}.
	 * @param duracion Duración del {@link BloqueET}.
	 */
	public BloqueET(Aula aula, BloqueHorario bloqueHorario, int duracion) {
		this.bloqueHorario = bloqueHorario;
		this.aula = aula;
		this.duracion = duracion;
	}
	
	/**
	 * Constructor de un {@link BloqueET} aleatorio.
	 * @param rnd Instancia de {@link java.util.Random} para generar números aleatorios.
	 * @param horaMin Hora de inicio mínima de las clases.
	 * @param horaMax Hora de finalización máxima de las clases.
	 * @param duracion Duración del {@link BloqueET}.
	 */
	public BloqueET(Random rnd, int horaMin, int horaMax, int duracion) {
		this.bloqueHorario = new BloqueHorario(rnd, horaMin, horaMax);
		this.duracion = duracion;
		this.aula = CtrlDominio.getCtrlDominio().randomAula();
	}

	/**
	 * Función que mira si el {@link BloqueET} se solapa con otro.
	 * @param other El {@link BloqueET} con el que chequear los solapamientos.
	 * @return Devuelve cierto si los dos {@link BloqueET} se solapas.
	 */
	public boolean solapa(BloqueET other) {
		if (!aula.equals(other.aula)) return false;
		if (!bloqueHorario.getDs().equals(other.bloqueHorario.getDs())) return false;
        return !(
                bloqueHorario.getHoraInicio() > other.bloqueHorario.getHoraInicio() + other.duracion ||
                        other.bloqueHorario.getHoraInicio() > bloqueHorario.getHoraInicio() + duracion);
	}

	/**
	 *Comparador de la clase {@link BloqueET}.
	 * @param o Objeto con el que comparar.
	 * @return true sii {@code o} es un {@link BloqueET} en el mismo {@link BloqueHorario},
	 * en la misma {@link Aula} y con la mismaduración-.
	 */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof BloqueET))
			return false;
		BloqueET other = (BloqueET) o;
		if (!aula.equals(other.aula)) return false;
		if (!(duracion == other.duracion)) return false;
		return (bloqueHorario.equals(other.bloqueHorario));
	}

	/**
	 * @return El {@link BloqueHorario}.
	 */
	public BloqueHorario getBloqueHorario() {
		return bloqueHorario;
	}

	/**
	 * Cambia el {@link BloqueHorario} del {@link BloqueET}.
	 * @param bloqueHorario El nuevo {@link BloqueHorario}.
	 */
	public void setBloqueHorario(BloqueHorario bloqueHorario) {
		this.bloqueHorario = bloqueHorario;
	}

	/**
	 * @return Devuelve el {@link Aula} del {@link BloqueET}.
	 */
	public Aula getAula() {
		return aula;
	}

	/**
	 * Cambia el {@link Aula}.
	 * @param aula La nueva {@link Aula}.
	 */
	public void setAula(Aula aula) {
		this.aula = aula;
	}

	public int getDuracion() {return duracion;}
}
