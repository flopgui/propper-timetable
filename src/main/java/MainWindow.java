import com.google.gson.Gson;
import jdk.nashorn.internal.scripts.JO;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.net.URL;
import java.util.DuplicateFormatFlagsException;
import java.util.Random;
import java.io.File;
import java.util.UUID;

public class MainWindow extends JFrame implements LangListener, ActionListener {

    private JMenuBar menuBar;
    private JMenu file, helpmenu;
    private JMenuItem itemnew, open, recent, save, saveas, importar, exportar, preferences, quit, help, about;
    private VistaAula aulasTab;
    private VistaTitulacion titulacionesTab;
    private VistaHorario horarioTab;
    private JTabbedPane tabbedPanel;
    private CtrlPresentacion ctrl;
    private JPanel jPanel;

    public MainWindow(CtrlPresentacion ctrl) {
        super();

        this.ctrl = ctrl;

        Lang.registerListener(this);

        this.setSize(800, 600);

        jPanel = new JPanel();
        jPanel.setLayout(new BorderLayout());


        menuBar = new JMenuBar();
        file = new JMenu();
        helpmenu = new JMenu();

        itemnew = new JMenuItem();
        open = new JMenuItem();
        recent = new JMenu();
        save = new JMenuItem();
        saveas = new JMenuItem();
        importar = new JMenuItem();
        exportar = new JMenuItem();
        preferences = new JMenuItem();
        quit = new JMenuItem();
        help = new JMenuItem();
        about = new JMenuItem();

        itemnew.setAccelerator(KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        open.setAccelerator(KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        save.setAccelerator(KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        saveas.setAccelerator(KeyStroke.getKeyStroke('S', InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
        importar.setAccelerator(KeyStroke.getKeyStroke('I', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        exportar.setAccelerator(KeyStroke.getKeyStroke('E', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        preferences.setAccelerator(KeyStroke.getKeyStroke('P', InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
        quit.setAccelerator(KeyStroke.getKeyStroke('Q', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        help.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1,0));

        recent.setEnabled(false);

        file.add(itemnew);
        file.add(open);
        file.add(recent);
        file.addSeparator();
        file.add(save);
        file.add(saveas);
        file.addSeparator();
        file.add(importar);
        file.add(exportar);
        file.addSeparator();
        file.add(preferences);
        file.add(quit);
        helpmenu.add(help);
        helpmenu.add(about);

        itemnew.addActionListener(this);
        itemnew.setActionCommand("new");
        open.addActionListener(this);
        open.setActionCommand("open");
        save.addActionListener(this);
        save.setActionCommand("save");
        saveas.addActionListener(this);
        saveas.setActionCommand("saveas");
        importar.addActionListener(this);
        importar.setActionCommand("importar");
        exportar.addActionListener(this);
        exportar.setActionCommand("exportar");
        quit.addActionListener(this);
        quit.setActionCommand("quit");
        preferences.addActionListener(this);
        preferences.setActionCommand("preferences");
        help.addActionListener(this);
        help.setActionCommand("help");
        about.addActionListener(this);
        about.setActionCommand("about");

        menuBar.add(file);
        menuBar.add(helpmenu);
        jPanel.add(menuBar, BorderLayout.NORTH);

        titulacionesTab = new VistaTitulacion(ctrl);
        aulasTab = new VistaAula(ctrl);
        horarioTab = new VistaHorario(ctrl);

        tabbedPanel = new JTabbedPane();
        tabbedPanel.addTab("", titulacionesTab);
        tabbedPanel.addTab("", aulasTab);
        tabbedPanel.addTab("", horarioTab);
        tabbedPanel.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                JTabbedPane p = (JTabbedPane) changeEvent.getSource();
                if (p.getSelectedComponent() == horarioTab) {
                    horarioTab.onTabSelected();
                }
            }
        });
        jPanel.add(tabbedPanel,BorderLayout.CENTER);

        tabbedPanel.setSelectedIndex(0);

        this.add(jPanel);
        this.setVisible(true);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                String[] options = new String[]{Lang.getString("si"), Lang.getString("no"), Lang.getString("cancelar")};
                int n = JOptionPane.showOptionDialog(save, Lang.getString("guardarsalir"),
                        Lang.getString("seguro"), JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,
                        null, options, options[2]);
                if (n == JOptionPane.YES_OPTION) {
                    actionPerformed(new ActionEvent(save, 221, "save")); // Silly reference to XC because of yes
                } else if (n == JOptionPane.NO_OPTION) {
                    ctrl.deleteAutosaved();
                } else if (n == JOptionPane.CANCEL_OPTION) {
                    return;
                }
                System.exit(0);
            }
        });

        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        setText();
    }

    private JComponent makeTextPanel(String text) {
        JPanel panel = new JPanel(false);
        JLabel filler = new JLabel(text);
        filler.setHorizontalAlignment(JLabel.CENTER);
        panel.setLayout(new GridLayout(1, 1));
        panel.add(filler);
        return panel;
    }

    @Override
    public void onLanguageChanged() {
        setText();
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().equals("new")) {
            int n = JOptionPane.showOptionDialog(null, Lang.getString("preguntarguardar"), Lang.getString("save"),
                    JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE,null, null,JOptionPane.YES_OPTION);
            if (n == JOptionPane.CANCEL_OPTION) return;
            if (n == JOptionPane.YES_OPTION){
                actionPerformed(new ActionEvent(ae.getSource(), new Random().nextInt(), "save"));
            }
            if(n == -1) return;
            ctrl.reset();
        } else if (ae.getActionCommand().equals("open")) {
            int n = JOptionPane.showOptionDialog(null, Lang.getString("preguntarguardar"), Lang.getString("save"),
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,null, null,JOptionPane.YES_OPTION);
            if (n == JOptionPane.YES_OPTION){
                actionPerformed(new ActionEvent(ae.getSource(), new Random().nextInt(), "save"));
            }
            if(n == -1) return;
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle(Lang.getString("open"));
            fc.setCurrentDirectory(new java.io.File("."));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("ttb","ttb");
            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileFilter(filter);
            int p = fc.showOpenDialog(open);
            try {
                if (p == JFileChooser.APPROVE_OPTION) {
                    String path = fc.getSelectedFile().getPath();
                    ctrl.open(path);
                }
            } catch (Exception ex) {
                ctrl.gestionError(Lang.getString("error"));
                ex.printStackTrace();
            }
        } else if (ae.getActionCommand().equals("save")) {
            if(ctrl.getActualPath()==null) actionPerformed(new ActionEvent(ae.getSource(), new Random().nextInt(), "saveas"));
            else ctrl.save(ctrl.getActualPath());
        } else if (ae.getActionCommand().equals("saveas")) {
            JFrame parentFrame = new JFrame();
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle(Lang.getString("saveas"));
            fc.setCurrentDirectory(new java.io.File("."));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("ttb", "ttb");
            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileFilter(filter);
            int userSelection = fc.showSaveDialog(parentFrame);
            if (userSelection == JFileChooser.APPROVE_OPTION) {
                String fileToSave = fc.getSelectedFile().getPath();
                if (fileToSave.endsWith(".ttb")) ctrl.save(fileToSave);
                else ctrl.save(fileToSave + ".ttb");
            }
        } else if (ae.getActionCommand().equals("preferences")) {
            VistaPreferencia VP = new VistaPreferencia(ctrl);
            VP.setLocationRelativeTo(jPanel);
            VP.setModal(true);
            VP.setVisible(true);
        } else if (ae.getActionCommand().equals("quit")) {
            JLabel text = new JLabel(Lang.getString("confirmarsalir"));
            JPanel leave = new JPanel(new BorderLayout());
            leave.add(text, BorderLayout.CENTER);
            int result = JOptionPane.showConfirmDialog(null,text,Lang.getString("quit"),JOptionPane.OK_CANCEL_OPTION);
            if(result == 0) dispose();
        } else if (ae.getActionCommand().equals("help")) {
            JEditorPane jep = new JEditorPane();
            try {
                jep.setContentType("text/html");
                jep.setPage(getClass().getResource("MANUAL.html"));
            }catch (Exception e) {
                jep.setContentType("text/html");
                jep.setText("<html>Couldn't load</html>");
            }

            JScrollPane scrollPane = new JScrollPane(jep);
            JFrame f = new JFrame(Lang.getString("help"));
            f.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            f.getContentPane().add(scrollPane);
            f.setPreferredSize(new Dimension(800,600));
            f.pack();
            f.setVisible(true);
        } else if (ae.getActionCommand().equals("about")) {
            JOptionPane.showMessageDialog(this, Lang.getString("sobretexto"));
        } else if (ae.getActionCommand().equals("importar")) {
            if (tabbedPanel.getSelectedIndex() == 0) {
                JFileChooser fc = new JFileChooser();
                fc.setDialogTitle(Lang.getString("import"));
                fc.setCurrentDirectory(new java.io.File("."));
                FileNameExtensionFilter filter = new FileNameExtensionFilter("ttbt", "ttbt");
                fc.setAcceptAllFileFilterUsed(true);
                fc.setFileFilter(filter);
                int p = fc.showOpenDialog(open);
                try {
                    if (p == JFileChooser.APPROVE_OPTION) {
                        String path = fc.getSelectedFile().getPath();
                        try {
                            ctrl.importTitulacion(path, false);
                        } catch (DuplicateFormatFlagsException e) {
                            String[] options = new String[]{Lang.getString("sobreescribe"), Lang.getString("cancelar")};
                            int n = JOptionPane.showOptionDialog(null, Lang.getString("colisiondetectada"),
                                    Lang.getString("colision"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                                    null, options, options[1]);
                            if (n == JOptionPane.YES_OPTION) {
                                ctrl.importTitulacion(path, true);
                            }
                        }
                        titulacionesTab.reloadEditor();
                    }
                } catch (Exception ex) {
                    ctrl.gestionError(Lang.getString("error"));
                    ex.printStackTrace();
                }
            } else if (tabbedPanel.getSelectedIndex() == 1) {
                JFileChooser fc = new JFileChooser();
                fc.setDialogTitle(Lang.getString("import"));
                fc.setCurrentDirectory(new java.io.File("."));
                FileNameExtensionFilter filter = new FileNameExtensionFilter("ttba", "ttba");
                fc.setAcceptAllFileFilterUsed(true);
                fc.setFileFilter(filter);
                int p = fc.showOpenDialog(open);
                try {
                    if (p == JFileChooser.APPROVE_OPTION) {
                        String path = fc.getSelectedFile().getPath();
                        try {
                            ctrl.importAula(path, false);
                        } catch (DuplicateFormatFlagsException e) {
                            String[] options = new String[]{Lang.getString("sobreescribe"), Lang.getString("cancelar")};
                            int n = JOptionPane.showOptionDialog(null, Lang.getString("colisiondetectada"),
                                    Lang.getString("colision"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
                                    null, options, options[1]);
                            if (n == JOptionPane.YES_OPTION) {
                                ctrl.importAula(path, true);
                            }
                        } finally {
                            aulasTab.updateTable();
                        }
                    }
                } catch (Exception ex) {
                    ctrl.gestionError(Lang.getString("error"));
                    ex.printStackTrace();
                }
            }
        } else if (ae.getActionCommand().equals("exportar")) {
            JFileChooser fc = new JFileChooser();
            fc.setDialogTitle(Lang.getString("export"));
            fc.setCurrentDirectory(new java.io.File("."));
            FileNameExtensionFilter filter;
            if (tabbedPanel.getSelectedIndex() == 0)
                filter = new FileNameExtensionFilter("ttbt", "ttbt");
            else if (tabbedPanel.getSelectedIndex() == 1)
                filter = new FileNameExtensionFilter("ttba", "ttba");
            else
                filter = new FileNameExtensionFilter("JPEG", "jpg", "jpeg");
            fc.setAcceptAllFileFilterUsed(false);
            fc.setFileFilter(filter);
            int userSelection = fc.showSaveDialog(null);
            if (userSelection == JFileChooser.APPROVE_OPTION) {
                String fileToSave = fc.getSelectedFile().getPath();
                try {
                    if (tabbedPanel.getSelectedIndex() == 0) {
                        if (fileToSave.endsWith(".ttbt")) ctrl.exportaTitulacion(fileToSave);
                        else ctrl.exportaTitulacion(fileToSave + ".ttbt");
                    } else if (tabbedPanel.getSelectedIndex() == 1) {
                        if (fileToSave.endsWith(".ttba")) ctrl.exportaAula(fileToSave);
                        else ctrl.exportaAula(fileToSave + ".ttba");
                    } else {
                        BufferedImage img = horarioTab.exportarImagen();
                        if (fileToSave.endsWith(".jpg") || fileToSave.endsWith(".jpeg"))
                            ctrl.saveImage(fileToSave, img);
                        else
                            ctrl.saveImage(fileToSave + ".jpg", img);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, Lang.getString("errorinesperado"),
                            Lang.getString("error"), JOptionPane.ERROR_MESSAGE);
                }

            }
        }
    }

    private void setText() {
        this.setTitle(Lang.getString("wtitle"));
        itemnew.setText(Lang.getString("new"));
        file.setText(Lang.getString("files"));
        open.setText(Lang.getString("open"));
        recent.setText(Lang.getString("oprecent"));
        save.setText(Lang.getString("save"));
        saveas.setText(Lang.getString("saveas"));
        importar.setText(Lang.getString("import"));
        exportar.setText(Lang.getString("export"));
        preferences.setText(Lang.getString("pref"));
        quit.setText(Lang.getString("quit"));
        helpmenu.setText(Lang.getString("helpmenu"));
        help.setText(Lang.getString("help"));
        about.setText(Lang.getString("about"));
        tabbedPanel.setTitleAt(0, Lang.getString("titulaciones"));
        tabbedPanel.setTitleAt(1, Lang.getString("aulas"));
        tabbedPanel.setTitleAt(2, Lang.getString("horario"));
    }
}
