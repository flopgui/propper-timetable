import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HorarioTableModel extends AbstractTableModel {

    private int horaMin, horaMax;
    private ArrayList<ArrayList<ArrayList<String>>> data;
    private Map<String, Boolean> aulas;
    private Map<String, Boolean> asignaturas;

    private boolean displayAulas;

    public HorarioTableModel(int horaMin, int horaMax, String[] aulas, ArrayList<String> asignaturas,
                             ArrayList<ArrayList<ArrayList<String>>> data) {
        this.horaMin = horaMin;
        this.horaMax = horaMax;
        this.data = data;
        this.displayAulas = true;
        this.aulas = new HashMap<>();
        for (String aula : aulas)
            this.aulas.put(aula, false);
        this.asignaturas = new HashMap<>();
        for (String asignatura : asignaturas)
            this.asignaturas.put(asignatura, false);
    }

    @Override
    public Class<?> getColumnClass(int column) {
        if (column == 0)
            return Integer.class;
        return String[].class;
    }


    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return Lang.getString("hora");
            case 1:
                return Lang.getString("lunes");
            case 2:
                return Lang.getString("martes");
            case 3:
                return Lang.getString("miercoles");
            case 4:
                return Lang.getString("jueves");
            case 5:
                return Lang.getString("viernes");
            default:
                return "";
        }
    }

    @Override
    public int getRowCount() {
        return horaMax - horaMin;
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int i, int j) {
        if (j == 0)
            return horaMin + i;
        ArrayList<String> al = data.get(j-1).get(i);
        ArrayList<String> a = new ArrayList<>();

        for (String anAl : al) {
            String[] tmp = anAl.split(",");
            if (displayAulas && !aulas.get(tmp[3]))
                continue;
            if (!displayAulas && !asignaturas.get(tmp[0]))
                continue;
            a.add(tmp[0]);
            a.add(tmp[1]);
            a.add(tmp[2]);
            a.add(tmp[3]);
            a.add(tmp[4]);
            a.add(tmp[5]);
            a.add(tmp[6]);
        }
        String[] r = new String[a.size()];
        a.toArray(r);
        return r;
        //return data[j-1][i];
    }

    @Override
    public boolean isCellEditable(int i, int j) {
        if (j == 0)
            return false;
        return true;
    }

    public void displayAula(String a, boolean b) {
        aulas.put(a, b);
        this.fireTableDataChanged();
    }

    public void displayAsignatura(String a, boolean b) {
        asignaturas.put(a, b);
        this.fireTableDataChanged();
    }

    public void displayAulas() {
        displayAulas = true;
        this.fireTableDataChanged();
    }

    public void displayAsignaturas() {
        displayAulas = false;
        this.fireTableDataChanged();
    }

    public String[] getAulas() {
        String[] a = new String[aulas.size()];
        int i = 0;
        for (String au : aulas.keySet())
            a[i++] = au;
        return a;
    }

    public HorarioTableModel copy(CtrlPresentacion ctrl) {
        HorarioTableModel model = new HorarioTableModel(horaMin, horaMax, new String[]{}, new ArrayList<>(), ctrl.getHorarioData());
        model.aulas = this.aulas;
        model.asignaturas = this.asignaturas;
        return model;
    }
}
