import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.AbstractTableModel;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;

public class VistaGenerar extends JDialog implements LangListener {

    private CtrlPresentacion ctrl;
    private VistaHorario parent;
    private JLabel horaInLabel, horaFiLabel;
    private JSpinner horaIn, horaFi;
    private JScrollPane rests;
    private JButton gen;
    private JTable table;
    private RestTableModel model;
    private JCheckBox all;

    public VistaGenerar(VistaHorario parent, CtrlPresentacion ctrl) {
        super(ctrl.getMainwindow(), "tmp", true);
        Lang.registerListener(this);
        this.ctrl = ctrl;
        this.parent = parent;

        this.setSize(500, 300);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {

            }
        });

        this.setLayout(new BorderLayout());
        JPanel p1 = new JPanel(new FlowLayout());
        horaInLabel = new JLabel();
        horaFiLabel = new JLabel();
        horaIn = new JSpinner(new SpinnerNumberModel(8, 0, 22, 1) {
            @Override
            public Object getNextValue() {
                int x = (Integer) super.getNextValue();
                return Math.min(x, (Integer) horaFi.getValue() - 1);
            }
        });
        horaFi = new JSpinner(new SpinnerNumberModel(20, 1, 23, 1) {
            @Override
            public Object getPreviousValue() {
                int x = (Integer) super.getPreviousValue();
                return Math.max(x, (Integer) horaIn.getValue() + 1);
            }
        });

        p1.add(horaInLabel);
        p1.add(horaIn);
        p1.add(horaFiLabel);
        p1.add(horaFi);
        this.add(p1, BorderLayout.NORTH);

        rests = new JScrollPane();
        model = new RestTableModel();
        table = new JTable(model);
        table.setPreferredScrollableViewportSize(table.getPreferredSize());
        rests.setViewportView(table);
        //rests.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 20));
        all = new JCheckBox();
        all.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                if (all.isSelected())
                    model.selectAll();
                else
                    model.unselectAll();
            }
        });

        JPanel p2 = new JPanel(new BorderLayout());
        p2.add(rests, BorderLayout.CENTER);
        p2.add(all, BorderLayout.NORTH);
        this.add(p2, BorderLayout.CENTER);

        JPanel p3 = new JPanel(new FlowLayout());
        gen = new JButton();
        gen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                generarHorario();
            }
        });
        p3.add(gen);
        this.add(p3, BorderLayout.SOUTH);

        onLanguageChanged();
    }

    public void generarHorario() {
        for (int i = 0; i < model.nomb.length; i++) {
            ctrl.setRestriccionActiva(model.nomb[i], model.act[i]);
            ctrl.setRestriccionPeso(model.nomb[i], model.peso[i]);
        }
        ctrl.setHorasLactivas((Integer) horaIn.getValue(), (int) horaFi.getValue());
        Window ancestor = SwingUtilities.getWindowAncestor(this);
        ProgressMonitor monitor = new ProgressMonitor(VistaGenerar.this, Lang.getString("generando"), "",
                0, ctrl.getGenerationCount());
        ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(4);
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    int i  = ctrl.generaHorario(queue);
                    if (i == -1)
                        throw new IllegalArgumentException("horas*aulas < sesiones");
                    else if (i == 0)
                        return;
                    ancestor.dispose();
                    parent.generateVistaHorario();
                } catch (IllegalArgumentException e) {
                    ctrl.gestionError(Lang.getString("errordemasiadassesiones"));
                }catch (Exception e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(ancestor, Lang.getString("errorinesperado"),
                            Lang.getString("error"), JOptionPane.ERROR_MESSAGE);
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                int i = 0;
                while (i != -1) {
                    try {
                        if (monitor.isCanceled()) {
                            t.interrupt();
                            t.join(10);
                            i = -1;
                        } else {
                            i = queue.take();
                            monitor.setProgress(i);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        i = -1;
                        monitor.setMaximum(-2);
                    }
                }
            }
        });
        t.start();
        t2.start();

    }

    private double boolToDouble(boolean a) {
        return a ? 1d : 0d;
    }

    @Override
    public void onLanguageChanged() {
        horaInLabel.setText(Lang.getString("horain"));
        horaFiLabel.setText(Lang.getString("horafi"));
        gen.setText(Lang.getString("generar"));
        //rests.setViewportView(generateRestricciones());
        all.setText(Lang.getString("seleccionartodo"));
        this.setTitle(Lang.getString("generar"));
    }

    private class RestTableModel extends AbstractTableModel {

        private final Class<?> columnClass[] = {Boolean.class, String.class, Double.class};

        private boolean act[];
        private String nomb[];
        private double peso[];

        public RestTableModel() {
            nomb = ctrl.getRestricciones();
            act = new boolean[nomb.length];
            peso = new double[nomb.length];
            for (int i = 0; i < nomb.length; i++) {
                peso[i] = ctrl.getRestriccionPeso(nomb[i]);
            }
        }

        @Override
        public String getColumnName(int column) {
            switch (column) {
                case 0:
                    return Lang.getString("activa");
                case 1:
                    return Lang.getString("nombre");
                case 2:
                    return Lang.getString("peso");
                default:
                    return "";
            }
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            return columnClass[columnIndex];
        }

        @Override
        public int getRowCount() {
            return nomb.length;
        }

        @Override
        public int getColumnCount() {
            return 3;
        }

        @Override
        public boolean isCellEditable(int i, int j) {
            if (j == 1)
                return false;
            return true;
        }

        @Override
        public Object getValueAt(int i, int j) {
            switch (j) {
                case 0:
                    return act[i];
                case 1:
                    return nomb[i]; //TODO nombres pero bien
                case 2:
                    return peso[i];
            }
            return null;
        }

        @Override
        public void setValueAt(Object aValue, int i, int j) {
            if (j == 0) {
                act[i] = (boolean) aValue;
            }
            if (j == 2) {
                peso[i] = (double) aValue;
            }
        }

        public void selectAll() {
            boolean t[] = new boolean[nomb.length];
            Arrays.fill(t, true);
            this.act = t;
            fireTableDataChanged();
        }

        public void unselectAll() {
            boolean f[] = new boolean[nomb.length];
            Arrays.fill(f, false);
            this.act = f;
            fireTableDataChanged();
        }
    }
}
