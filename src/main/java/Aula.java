import java.util.ArrayList;

/**
 * Clase que representa un aula. Un aula tiene un nombre, una capacidad y una lista de
 * {@link Recurso}s que tiene a su disposición.
 */
public class Aula {
	
    /**
     * Nombre del {@link Aula}.
     */
    private String nombre;
    
    /**
     * Capacidad máxima del {@link Aula}.
     */
    private int capacidad;

    /**
     * Lista de recursos de los que dispone el {@link Aula}.
     */
    private ArrayList<Recurso> recursos;

    /**
     * Constructor del {@link Aula}.
     * @param nombre Nombre del {@link Aula}.
     * @param capacidad Capacidad del {@link Aula}.
     */
    public Aula(String nombre, int capacidad) {
        this(nombre, capacidad, new ArrayList<Recurso>());
    }

      
    /**
    * Constructor básico.
    * @param nombre Nombre de la {@link Aula}.
    * @param capacidad Capacidad de la {@link Aula}.
    * @param recursos Recursos de la {@link Aula}.
    */
    public Aula(String nombre, int capacidad, ArrayList<Recurso> recursos){
        if(nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("El nombre no puede ser vacio");
        this.nombre = nombre;
        if(capacidad <= 0)
            throw new java.lang.IllegalArgumentException("La capacidad de una aula tiene que ser mayor que 0");
        this.capacidad = capacidad;
        this.recursos = recursos;
    }

    /**
     * @return Nombre del {@link Aula}.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Cambia el nombre del {@link Aula}.
     * @param n Nuevo nombre.
     */
    public void setNombre(String n)  {
        this.nombre = n;
    }
    
    /**
     * @return Capacidad del {@link Aula}.
     */
    public int getCapacidad() {
        return capacidad;
    }

    /**
     * Cambia la capacidad del {@link Aula}.
     * @param capacidad Nueva capacidad.
     */
    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    /**
     * @return Devuelve el número de {@link Recurso}s de los cuales dispone el {@link Aula}.
     */
    public int getNumeroRecursos() {
        return recursos.size();
    }

    /**
     * Función que devuelve el {@link Recurso} número {@code i}.
     * @param i Número del {@link Recurso} que se quiere obtener.
     * @return El {@link Recurso} número {@code i}.
     */
    public Recurso getRecurso(int i) {
        return recursos.get(i);
    }
    
    /**
     * Busca si el {@link Aula} tiene un {@link Recurso}.
     * @param r El {@link Recurso} a buscar
     * @return Devuelve {@code true} sii el aula tiene el recurso {@code r}.
     */
    public boolean hasRecurso(Recurso r) {
    	return recursos.contains(r);
    }
    
    /**
     * Añade un {@link Recurso} a la {@link Aula}.
     * @param r El {@link Recurso} a añadir.
     * @return Devuelve {@code true} sii no existía ya un {@link Recurso} igual a {@code r} en el {@link Aula}.
     */
    public boolean addRecurso(Recurso r){
        if (recursos.contains(r)) return false;
        recursos.add(r);
        return true;
    }

    /**
     * Borra el {@link Recurso} con el nombre {@code r} del {@link Aula}.
     * @param r El nombre del {@link Recurso} a borrar.
     * @return true sii el {@link Aula} tenía previamente un {@link Recurso} con nombre {@code r}.
     */
    public boolean removeRecurso(String r) {
        for (int i = 0; i < recursos.size(); i++) {
            if (recursos.get(i).getNombre().equals(r)) {
                recursos.remove(i);
                return true;
            }
        }
        return false;
    }

    public String getRecursos() {
        StringBuilder b = new StringBuilder();
        for (Recurso r : recursos)
            b.append(r.getNombre()).append(":");
        return b.toString();
    }

    /**
     * Comparador de la clase {@link Aula}.
     * @param o Objeto con el que se quiere comparar.
     * @return true sii {@code o} es un {@link Aula} y los nombres coinciden.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Aula)
            return ((Aula) o).nombre.equals(nombre);
        return false;
    }
}