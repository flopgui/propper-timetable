import java.util.ArrayList;

/**
 * Clase que representa un cuatrimestre. Un cuatrimestre se identifica por un nombre (p.e. Q1, Q2, ...).
 */
public class Cuatrimestre {
    
    /**
     * Nombre del {@link Cuatrimestre}.
     */
    private String nombre;
    
    /**
     * Lista de {@link Asignatura}s del {@link Cuatrimestre}.
     */
    private ArrayList<Asignatura> asignaturas;

    /**
     * Constructor del {@link Cuatrimestre}.
     * @param nombre Nombre del {@link Cuatrimestre}.
     */
    Cuatrimestre(String nombre) {
        if (nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("nombre no puede estar vacio");
        this.nombre = nombre;
        asignaturas = new ArrayList<>();
    }
    
    /**
     * @return Nombre del {@link Cuatrimestre}.
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * Cambia el nombre del {@link Cuatrimestre}.
     * @param nombre Nuevo nombre.
     */
    public void setNombre(String nombre) {
        if (nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("nombre no puede estar vacio");
        this.nombre = nombre;
    }

    public boolean addAsignatura(Asignatura a) {
        if (asignaturas.contains(a)) {
            return false;
        }
        return asignaturas.add(a);
    }

    public boolean removeAsignatura(String s) {
        for (int i = 0; i < asignaturas.size(); i++) {
            if (asignaturas.get(i).getSiglas().equals(s)) {
                asignaturas.remove(i);
                return true;
            }
        }
        return false;
    }

    public int getNumeroAsignaturas() {
        return asignaturas.size();
    }

    public Asignatura getAsignatura(int i) {
        try {
            return asignaturas.get(i);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Comparador de la clase {@link Cuatrimestre}.
     * @param o Objeto con el que comparar.
     * @return Devuelve cierto sii {@code o} es un {@link Cuatrimestre} y los nombres coinciden.
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof Cuatrimestre)
            return ((Cuatrimestre) o).nombre.equals(nombre);
        return false;
    }
}
