import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.List;

public class VistaAula extends JPanel implements LangListener {

    private CtrlPresentacion ctrl;
    private JTable table;
    private JPopupMenu popup;
    private JButton newButton;
    private JMenuItem mi,gr,duplicate;
    private JPanel tablePanel;
    private JTextField Filtro = new JTextField(20);

    public VistaAula(CtrlPresentacion ctrl) {

        Lang.registerListener(this);

        this.ctrl = ctrl;
        this.setLayout(new BorderLayout());

        popup = new JPopupMenu();
        mi = new JMenuItem();
        gr = new JMenuItem();
        duplicate = new JMenuItem();
        duplicate.addActionListener(DUPLICAR);
        gr.addActionListener(GESTIONRECURSOS);
        mi.addActionListener(DELETE);
        popup.add(mi);
        popup.add(duplicate);
        popup.add(gr);

        tablePanel = new JPanel(new BorderLayout());
        String[] columnNames = {Lang.getString("nombre"),
                Lang.getString("capacidad"),
                Lang.getString("recursos")};

        Object[][] data = ctrl.getAulasData();

        String[] recursos = ctrl.getrecursos();
        MultiLineTableCellRenderer renderer = new MultiLineTableCellRenderer(ctrl);
        //Table
        table = new JTable(new MyTableModel(data, columnNames));
        table.setDefaultRenderer(String[].class,renderer);
        table.setDefaultEditor(String[].class, renderer);

        updateTable();
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints g = new GridBagConstraints();

        table.getTableHeader().setReorderingAllowed(true);
        table.getTableHeader().setReorderingAllowed(false);
        table.setRowHeight(table.getRowHeight()+9);
        table.setBorder(BorderFactory.createEmptyBorder(2,2,2,2));
        table.getColumnModel().getColumn(2).setCellRenderer(new MultiLineTableCellRenderer(ctrl));
        table.getColumnModel().getColumn(2).setCellEditor(new MultiLineTableCellRenderer(ctrl));
        table.setShowGrid(true);
        tablePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));

        tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
        tablePanel.add(table, BorderLayout.CENTER);
        this.add(tablePanel, BorderLayout.CENTER);
        newButton = new JButton();
        newButton.addActionListener(NUEVO);


        JPanel panelt = new JPanel(new BorderLayout());
        panelt.add(new JLabel(Lang.getString("busca")),
                BorderLayout.WEST);
        panelt.add(Filtro, BorderLayout.CENTER);

        TableRowSorter<TableModel> rowSorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(rowSorter);

        Action action = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
                TableCellListener tcl = (TableCellListener)e.getSource();
                if(tcl.getColumn() == 0 && tcl.getOldValue()!=tcl.getNewValue()){
                    try{
                        if(ctrl.hasAula((String)tcl.getNewValue())) {
                            table.getModel().setValueAt(tcl.getOldValue(),tcl.getRow(),tcl.getColumn());
                            updateTable();
                            throw new DuplicateFormatFlagsException("duplicar");
                        }
                        else ctrl.setname((String)tcl.getOldValue(),(String)tcl.getNewValue());
                    } catch(DuplicateFormatFlagsException E) {ctrl.gestionError("duplicado");}
                }
                else if(tcl.getColumn()==1){
                    try{
                        if(Integer.parseInt(tcl.getNewValue().toString())<=0) {
                            table.getModel().setValueAt(tcl.getOldValue(),tcl.getRow(),tcl.getColumn());
                            updateTable();
                            throw new NumberFormatException("nope");
                        }
                        else {
                            ctrl.setCapacidadAula(table.getModel().getValueAt(tcl.getRow(),0).toString(),
                                    Integer.parseInt(tcl.getNewValue().toString())); }
                    } catch (NumberFormatException E) {
                        table.getModel().setValueAt(tcl.getOldValue(),tcl.getRow(),tcl.getColumn());
                        updateTable();
                        ctrl.gestionError(Lang.getString("erroraulacapacidad"));

                    }
                }
            }
        };

        TableCellListener tcl = new TableCellListener(table, action);

        Filtro.getDocument().addDocumentListener(new DocumentListener(){

            @Override
            public void insertUpdate(DocumentEvent e) {
                String text = Filtro.getText();
                if (text.trim().length() == 0) { rowSorter.setRowFilter(null); }
                else { rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text,0)); }
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                String text = Filtro.getText();

                if (text.trim().length() == 0) { rowSorter.setRowFilter(null); }
                else { rowSorter.setRowFilter(RowFilter.regexFilter("(?i)" + text,0)); }
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.

            }

        });

        JPanel search = new JPanel(new CardLayout());
        search.add(panelt);
        search.setVisible(false);

        JPanel cl = new JPanel(new CardLayout());
        //cl.add(recursocombo);

        JPanel buttonnew = new JPanel(new FlowLayout());
        buttonnew.add(newButton);
        g.fill = GridBagConstraints.HORIZONTAL;
        g.gridx = 0;
        g.gridy = 0;
        panel.add(search,g);
        g.fill = GridBagConstraints.HORIZONTAL;
        g.gridx = 0;
        g.gridy = 1;
        panel.add(cl,g);
        g.fill = GridBagConstraints.HORIZONTAL;
        g.gridx = 0;
        g.gridy = 2;
        panel.add(buttonnew,g);

        table.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                mayShowPopup(e);
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                mayShowPopup(e);
            }

            @Override
            public void mouseClicked(MouseEvent e){
            }

            private void mayShowPopup(MouseEvent e) {
                if (e.isPopupTrigger())
                {
                    JTable source = (JTable)e.getSource();
                    int row = source.rowAtPoint( e.getPoint() );
                    int column = source.columnAtPoint( e.getPoint() );

                    if (! source.isRowSelected(row))
                        source.changeSelection(row, column, false, false);
                    if(row != -1) popup.show(e.getComponent(), e.getX(), e.getY());
                }
            }

        });

        tablePanel.add(panel,BorderLayout.SOUTH);

        Action SEARCH = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(search.isVisible()) search.setVisible(false);
                else search.setVisible(true);
            }
        };

        this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_F, InputEvent.CTRL_MASK),"search");
        this.getActionMap().put("search",SEARCH);

        onLanguageChanged();
        updateRowHeights();
    }

    ActionListener DELETE = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            JPanel panel = new JPanel(new GridLayout(0,1));
            panel.add(new JLabel(Lang.getString("confirmacioneliminar")));
            DefaultTableModel model = (DefaultTableModel) table.getModel();
            int[] row = table.getSelectedRows();
            int result = JOptionPane.showConfirmDialog(null,Lang.getString("confirmacioneliminar"),Lang.getString("eliminar"),
                    JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            if(result == JOptionPane.YES_OPTION) {
                for(int r : row) { ctrl.deleteAula(table.getValueAt(r,0).toString());}
                updateTable();
            }
        }
    };

    private ActionListener DUPLICAR = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int r = table.getSelectedRow();
            int capacitat = Integer.parseInt((String) table.getModel().getValueAt(r,1));
            String[] recursos = (String[]) table.getModel().getValueAt(r,2);
            JDialog ask = new JDialog();
            JPanel panel = new JPanel(new GridBagLayout());
            GridBagConstraints z = new GridBagConstraints();

            JLabel introduce = new JLabel(Lang.getString("introducenames"));
            JTextArea names = new JTextArea();
            JPanel down = new JPanel(new BorderLayout());
            down.add(introduce, BorderLayout.NORTH);
            down.add(new JScrollPane(names), BorderLayout.CENTER);

            JButton accept = new JButton(Lang.getString("confirmar"));
            JButton cancel = new JButton(Lang.getString("cancelar"));
            JPanel button = new JPanel(new FlowLayout());
            button.add(accept);
            button.add(cancel);

            ActionListener ACCEPT = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    ArrayList<String> nocreats, creats;
                    creats = new ArrayList<String>();
                    nocreats = new ArrayList<String>();
                    String input = names.getText();
                    String[] parts = input.split("\n");
                    HashSet<String> demanats = new HashSet<String>();
                    Collections.addAll(demanats, parts);
                    for (String s : demanats){
                        while (ctrl.hasAula(s)) {
                            s = s+"*";
                        }
                        creats.add(s);
                        ctrl.addAula(s,capacitat);
                        for(String r : recursos) ctrl.addRecursoAula(s,r);
                    }
                    updateTable();

                    ask.dispose();
                }
            };
            accept.addActionListener(ACCEPT);

            ActionListener CANCEL = new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent actionEvent) {
                    ask.dispose();
                }
            };
            cancel.addActionListener(CANCEL);

            z.weightx = 2;
            z.ipady=150;
            z.gridx = 0;
            z.gridy = 0;
            panel.add(down,z);

            z.ipady=0;
            z.weightx = 2;
            z.gridx = 0;
            z.gridy = 1;
            panel.add(button,z);

            ask.add(panel);
            ask.setSize(700,250);

            ask.setModal(true);
            ask.setVisible(true);

        }
    };

    private ActionListener GESTIONRECURSOS = new ActionListener() {

        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            int row = table.getSelectedRow();
            String nameaula = (String) table.getValueAt(row,0);
            String[] tmp = ctrl.getrecusosAula(nameaula);
            String[] r = VistaRecursos.gestionaRecursos(ctrl.getrecursos(), Arrays.asList(tmp));
            ctrl.setRecursosAula(nameaula.toString(),r);
            updateTable();
        };

    };

    ActionListener NUEVO = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            JPanel panel = new JPanel(new GridLayout(0, 1));
            panel.add(new JLabel(Lang.getString("nombre")));
            JTextField newaula = new JTextField(10);
            JTextField capaula = new JTextField(5);
            panel.add(newaula);
            panel.add(new JLabel(Lang.getString("capacidad")));
            panel.add(capaula);
            int result = JOptionPane.showConfirmDialog(null, panel, Lang.getString("crearaula"),
                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
            if (result == JOptionPane.OK_OPTION) {
                try{
                    if(Integer.parseInt(capaula.getText()) <= 0) throw new NumberFormatException("es negatiu");
                    if(!ctrl.addAula(newaula.getText(), Integer.parseInt(capaula.getText()))) throw new DuplicateFormatFlagsException("error");
                    updateTable();
                } catch(DuplicateFormatFlagsException DFE) {
                    ctrl.gestionError(Lang.getString("errorduplicado"));
                } catch(NumberFormatException NFE) {
                    ctrl.gestionError(Lang.getString("erroraulacapacidad"));
                } catch(IllegalArgumentException IAE) {
                    ctrl.gestionError(Lang.getString("errorargumento"));
                } catch (Exception ex) {
                    ex.printStackTrace();
                    ctrl.gestionError(Lang.getString("errorinesperado"));
                }
            }

        }
    };

    public void updateTable() {
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.setRowCount(0);
        Object[][] auxi = ctrl.getAulasData();
        for (Object[] anAuxi : auxi) { model.addRow(anAuxi); }
        table.setModel(model);
        updateRowHeights();
    }

    private void updateRowHeights() {
        for (int row = 0; row < table.getRowCount(); row++) {
            int rowHeight = table.getRowHeight();
            for (int column = 0; column < table.getColumnCount(); column++) {
                Component comp = table.prepareRenderer(table.getCellRenderer(row, column), row, column);
                rowHeight = Math.max(rowHeight, comp.getPreferredSize().height);
            }
            table.setRowHeight(row, rowHeight);
        }
    }

    public class MyTableModel extends DefaultTableModel {

        private MyTableModel(Object[][] tableData, Object[] colNames) {
            super(tableData, colNames);
        }

        public boolean isCellEditable(int row, int column) { return true; }

    }

    public void onLanguageChanged(){
        //tr.setText(Lang.getString("newname"));
        mi.setText(Lang.getString("eliminar"));
        gr.setText(Lang.getString("gestionrecursos"));
        duplicate.setText(Lang.getString("duplicar"));
        newButton.setText(Lang.getString("nueva"));
    }
}