import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.DuplicateFormatFlagsException;

public class VistaTitulacion extends JPanel implements ActionListener, LangListener {

    private CtrlPresentacion ctrl;
    private JPanel list,editor,listButtons,editorButtons;
    private JPanel[] cuatris;
    private JScrollPane listScroll,editorScroll;
    private JList<String> titulaciones;
    private JSplitPane splitPane;
    private JButton newTitulacion,addCuatrimestre,removeCuatrimestre;
    private String currentTitulacion;
    private int currentTitulacionIndex;

    public VistaTitulacion(CtrlPresentacion ctrl) {
        Lang.registerListener(this);

        this.ctrl = ctrl;
        this.setLayout(new BorderLayout());

        splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setOneTouchExpandable(false);
        splitPane.setDividerLocation(0.12);

        titulaciones = new JList<String>();
        titulaciones.addMouseListener(listMouseListener);
        listScroll = new JScrollPane(titulaciones);
        listButtons = new JPanel();
        newTitulacion = new JButton();
        newTitulacion.addActionListener(this);
        newTitulacion.setActionCommand("newtitulacion");
        listButtons.add(newTitulacion);

        list = new JPanel();
        list.setLayout(new BorderLayout());
        list.add(listScroll, BorderLayout.CENTER);
        list.add(listButtons, BorderLayout.SOUTH);
        splitPane.setLeftComponent(list);

        add(splitPane, BorderLayout.CENTER);

        updateTitulaciones();
        onLanguageChanged();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("newtitulacion")) {
            addTitulacion();
        } else if (e.getActionCommand().equals("addcuatri")) {
            Object[][] data = ctrl.getTitulacionData(currentTitulacion);
            ctrl.addCuatrimestre(currentTitulacion,Integer.toString(data.length+1));
            reloadEditor();
        } else if (e.getActionCommand().equals("removecuatri")) {
            Object[][] data = ctrl.getTitulacionData(currentTitulacion);
            boolean confirmed = true;
            if (((String[]) data[data.length-1][1]).length > 0) {  //Si no esta vacio
                confirmed = removeCuatrimestre();
            }
            if (confirmed) {
                ctrl.removeCuatrimestre(currentTitulacion, (String) data[data.length-1][0]);
                reloadEditor();
            }
        } else if (e.getActionCommand().startsWith("editasignatura")) {
            String siglas = e.getActionCommand().split("\\s")[1];
            JDialog dialog = new VistaAsignatura(ctrl, currentTitulacion, siglas);
            dialog.pack();
            dialog.setVisible(true);
            reloadEditor();
        } else if (e.getActionCommand().startsWith("addasignatura")) {
            int cuatri = Integer.parseInt(e.getActionCommand().split("\\s")[1]);
            VistaAsignatura dialog = new VistaAsignatura(ctrl, currentTitulacion);
            dialog.pack();
            dialog.setVisible(true);
            String siglas = dialog.getSiglas();
            if (!siglas.equals("")) ctrl.addAsignaturaToCuatrimestre(currentTitulacion,cuatri,siglas);
            reloadEditor();
        }
    }

    private MouseListener listMouseListener = new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent event) {
            if (event.isPopupTrigger()) {
                maybeShowPopUp(event);
            }
        }

        @Override
        public void mouseReleased(MouseEvent event) {
            if (event.isPopupTrigger()) {
                maybeShowPopUp(event);
            }
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getClickCount() == 1)
                return;
            if (titulaciones.getLeadSelectionIndex() < 0) return;
            loadEditor(titulaciones.getLeadSelectionIndex());
        }

        public void maybeShowPopUp(MouseEvent event) {
            if (!event.isPopupTrigger()) return;
            if (titulaciones.getLeadSelectionIndex() < 0) return;
            UDPopUp pop = new UDPopUp();
            pop.show(event.getComponent(), event.getX(), event.getY());
        }

        class UDPopUp extends JPopupMenu {
            public UDPopUp() {
                JMenuItem itemRename = new JMenuItem(Lang.getString("renombrar"));
                JMenuItem itemDelete = new JMenuItem(Lang.getString("eliminar"));

                itemRename.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        int i = titulaciones.getLeadSelectionIndex();
                        JPanel panel = new JPanel(new GridLayout(0, 2));
                        panel.add(new JLabel(Lang.getString("nombre")));
                        JTextField name = new JTextField(20);
                        panel.add(name);
                        int result = JOptionPane.showConfirmDialog(null, panel, Lang.getString("renombrartitulacion"),
                                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                        if (result == JOptionPane.OK_OPTION) {
                            try {
                                if (!ctrl.renameTitulacion((String) titulaciones.getModel().getElementAt(i), name.getText()))
                                    throw new DuplicateFormatFlagsException("Titulacion duplicada");
                                updateTitulaciones();
                            } catch (DuplicateFormatFlagsException DFE) {
                                ctrl.gestionError(Lang.getString("errorduplicado"));
                            } catch (IllegalArgumentException IAE) {
                                ctrl.gestionError(Lang.getString("errorargumento"));
                            } catch (Exception ex) {
                                ctrl.gestionError(Lang.getString("errorinesperado"));
                            }
                        }
                    }
                });
                this.add(itemRename);

                itemDelete.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        int i = titulaciones.getLeadSelectionIndex();
                        JPanel panel = new JPanel(new GridLayout(0,1));
                        panel.add(new JLabel(Lang.getString("confirmacioneliminar")));
                        Object[] options = {Lang.getString("si"),
                                Lang.getString("no")};
                        int result = JOptionPane.showConfirmDialog(null,Lang.getString("confirmacioneliminar"),Lang.getString("eliminar"),
                                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                        if(result == JOptionPane.YES_OPTION) {
                            try {
                                ctrl.deleteTitulacion((String) titulaciones.getModel().getElementAt(i));
                                updateTitulaciones();
                            } catch (Exception ex) {
                                ctrl.gestionError(Lang.getString("errorinesperado"));
                            }
                        }
                    }
                });
                this.add(itemDelete);
            }
        }
    };

    private MouseListener asignaturaMouseListener = new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent event) {
            if (event.isPopupTrigger()) {
                maybeShowPopUp(event);
            }
        }

        @Override
        public void mouseReleased(MouseEvent event) {
            if (event.isPopupTrigger()) {
                maybeShowPopUp(event);
            }
        }

        @Override
        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getClickCount() == 1)
                return;
            if (titulaciones.getLeadSelectionIndex() < 0) return;
            loadEditor(titulaciones.getLeadSelectionIndex());
        }

        public void maybeShowPopUp(MouseEvent event) {
            if (!event.isPopupTrigger()) return;
            UDPopUp pop = new UDPopUp(((JButton) event.getComponent()).getText());
            pop.show(event.getComponent(), event.getX(), event.getY());
        }

        class UDPopUp extends JPopupMenu {
            public UDPopUp(String siglas) {
                JMenuItem itemClone = new JMenuItem(Lang.getString("duplicar"));
                JMenuItem itemMoveTo = new JMenuItem(Lang.getString("movera"));
                JMenuItem itemDelete = new JMenuItem(Lang.getString("eliminar"));

                itemClone.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        try {
                            JPanel panel = new JPanel(new GridLayout(0, 1));
                            panel.add(new JLabel(Lang.getString("siglas")));
                            JTextField name = new JTextField(20);
                            panel.add(name);
                            int result = JOptionPane.showConfirmDialog(null, panel, Lang.getString("renombrartitulacion"),
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                            if (result == JOptionPane.OK_OPTION) {
                                try {
                                    int cuatri = ctrl.getCuatrimestreAsignatura(currentTitulacion,siglas);
                                    if (!ctrl.cloneAsignatura(currentTitulacion,cuatri, siglas, name.getText()))
                                        throw new DuplicateFormatFlagsException("Asignatura duplicada");
                                    ctrl.setColorAsignatura(currentTitulacion,name.getText(),ctrl.randomPastelColor());
                                    updateTitulaciones();
                                } catch (DuplicateFormatFlagsException DFE) {
                                    ctrl.gestionError(Lang.getString("errorduplicado"));
                                } catch (IllegalArgumentException IAE) {
                                    ctrl.gestionError(Lang.getString("errorargumento"));
                                } catch (Exception ex) {
                                    ctrl.gestionError(Lang.getString("errorinesperado"));
                                    ex.printStackTrace();
                                }
                            }
                            reloadEditor();
                        } catch (Exception ex) {
                            ctrl.gestionError(Lang.getString("errorinesperado"));
                        }
                    }
                });
                this.add(itemClone);
                itemMoveTo.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        try {
                            JPanel panel = new JPanel(new GridLayout(0, 1));
                            panel.add(new JLabel(Lang.getString("cuatrimestre")));
                            ArrayList<String> cu = new ArrayList<String>();
                            for (Object[] o: ctrl.getTitulacionData(currentTitulacion)) {
                                cu.add((String) o[0]);
                            }
                            JComboBox<String> cuatriselector = new JComboBox<String>();
                            cuatriselector.setModel(new DefaultComboBoxModel<String>(cu.toArray(new String[cu.size()])));
                            panel.add(cuatriselector);
                            int result = JOptionPane.showConfirmDialog(null, panel, Lang.getString("renombrartitulacion"),
                                    JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
                            if (result == JOptionPane.OK_OPTION) {
                                try {
                                    ctrl.moveAsignaturaCuatrimestre(currentTitulacion,cuatriselector.getSelectedIndex(),siglas);
                                    updateTitulaciones();
                                } catch (DuplicateFormatFlagsException DFE) {
                                    ctrl.gestionError(Lang.getString("errorduplicado"));
                                } catch (IllegalArgumentException IAE) {
                                    ctrl.gestionError(Lang.getString("errorargumento"));
                                } catch (Exception ex) {
                                    ctrl.gestionError(Lang.getString("errorinesperado"));
                                    ex.printStackTrace();
                                }
                            }
                            reloadEditor();
                        } catch (Exception ex) {
                            ctrl.gestionError(Lang.getString("errorinesperado"));
                        }
                    }
                });
                this.add(itemMoveTo);
                itemDelete.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent actionEvent) {
                        try {
                            ctrl.deleteAsignatura(currentTitulacion, siglas);
                            reloadEditor();
                        } catch (Exception ex) {
                            ctrl.gestionError(Lang.getString("errorinesperado"));
                        }
                    }
                });
                this.add(itemDelete);
            }
        }
    };

    private void addTitulacion() {
        JPanel panel = new JPanel(new GridLayout(0, 1));
        panel.add(new JLabel(Lang.getString("nombre")));
        JTextField name = new JTextField(20);
        panel.add(name);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                name.requestFocus();
            }
        });  //TODO Aixo hauria de posar el cursor en name, pero no va
        int result = JOptionPane.showConfirmDialog(null, panel, Lang.getString("creartitulacion"),
                JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
        if (result == JOptionPane.OK_OPTION) {
            try {
                if (!ctrl.addTitulacion(name.getText()))
                    throw new DuplicateFormatFlagsException("Titulacion duplicada");
                updateTitulaciones();
            } catch (DuplicateFormatFlagsException DFE) {
                ctrl.gestionError(Lang.getString("errorduplicado"));
            } catch (IllegalArgumentException IAE) {
                ctrl.gestionError(Lang.getString("errorargumento"));
            } catch (Exception ex) {
                ctrl.gestionError(Lang.getString("errorinesperado"));
            }
        }
    }

    private boolean removeCuatrimestre() {
        String[] options = {Lang.getString("eliminarigual"), Lang.getString("no")};
        int result = JOptionPane.showOptionDialog(null,
                Lang.getString("confirmacioneliminarcuatrimestre"),
                Lang.getString("eliminar"),
                JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null,
                options, options[0]);
        return (result == JOptionPane.OK_OPTION);
    }

    private void loadEditor(int index) {
        currentTitulacion = ctrl.getNombresTitulaciones()[index];
        currentTitulacionIndex = index;
        editor = new JPanel();
        editor.setLayout(new BoxLayout(editor, BoxLayout.Y_AXIS));
        Object[][] data = ctrl.getTitulacionData(currentTitulacion);
        editor.add(new JLabel(currentTitulacion));
        cuatris = new JPanel[data.length];

        for (int i=0; i<data.length; ++i) {
            Object[] cuatri = data[i];
            cuatris[i] = new JPanel();
            cuatris[i].setBorder(BorderFactory.createCompoundBorder(
                    BorderFactory.createEmptyBorder(8, 12, 8, 12),
                    BorderFactory.createEtchedBorder(EtchedBorder.LOWERED)));
            cuatris[i].setLayout(new BoxLayout(cuatris[i], BoxLayout.X_AXIS));

            cuatris[i].add(new JLabel((String) cuatri[0]));
            Dimension minSize = new Dimension(0, 0);
            Dimension prefSize = new Dimension(8, 20);
            Dimension maxSize = new Dimension(8, 20);
            cuatris[i].add(new Box.Filler(minSize, prefSize, maxSize));

            for (String as: (String[]) cuatri[1]) {
                JButton but = new JButton(as);
                but.addActionListener(this);
                but.setBackground(ctrl.getColorAsignatura(currentTitulacion,as));
                but.setForeground(ctrl.getForeground(but.getBackground()));
                but.setActionCommand("editasignatura "+as);
                but.addMouseListener(asignaturaMouseListener);
                cuatris[i].add(but);
            }
            JButton but = new JButton("+");
            but.setActionCommand("addasignatura "+Integer.toString(i));
            but.addActionListener(this);
            cuatris[i].add(but);
            cuatris[i].add(Box.createHorizontalGlue());
            editor.add(cuatris[i]);
        }
        editorButtons = new JPanel();
        editorButtons.setBorder(BorderFactory.createEmptyBorder(8, 12, 8, 12));
        editorButtons.setLayout(new BoxLayout(editorButtons, BoxLayout.X_AXIS));
        addCuatrimestre = new JButton("+");
        addCuatrimestre.addActionListener(this);
        addCuatrimestre.setActionCommand("addcuatri");
        editorButtons.add(addCuatrimestre);
        removeCuatrimestre = new JButton("-");
        removeCuatrimestre.addActionListener(this);
        removeCuatrimestre.setActionCommand("removecuatri");
        editorButtons.add(removeCuatrimestre);
        editorButtons.add(Box.createHorizontalGlue());
        editor.add(editorButtons);

        editorScroll = new JScrollPane(editor);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(0.15);
        splitPane.setRightComponent(editorScroll);
        add(splitPane, BorderLayout.CENTER);
    }

    void reloadEditor() {
        loadEditor(currentTitulacionIndex);
    }

    void updateTitulaciones() {
        DefaultListModel<String> model = new DefaultListModel<String>();
        for (String s : ctrl.getNombresTitulaciones()) {
            model.addElement(s);
        }
        titulaciones.setModel(model);
    }

    private JScrollPane generateListPanel() {
        String[] titus = ctrl.getNombresTitulaciones();
        JList<String> list = new JList<>(titus);
        return new JScrollPane(list);
    }

    @Override
    public void onLanguageChanged() {
        newTitulacion.setText(Lang.getString("nueva"));
    }
}
