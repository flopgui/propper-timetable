import java.util.Locale;

public class Main {
    public static void main(String args[]) {
        javax.swing.SwingUtilities.invokeLater(
                new Runnable() {
                    @Override
                    public void run() {
                        Locale.setDefault(new Locale("es"));
                        CtrlPresentacion ctp = new CtrlPresentacion();
                    }
                }
        );
    }
}
