import java.util.TreeMap;
import java.util.function.Function;

/**
 * Clase que representa una restricción. Una restricción tiene un nombre, una descripción y un peso.
 * Además, una restricción puede estar activa o inactiva.
 */
public class Restriccion {

	/**
	 * Nombre de la {@link Restriccion}.
	 */
	private String nombre;

	/**
	 * Descripccion de la {@link Restriccion}.
	 */
	private String descripcion;

	/**
	 * Función que evalua a un {@link Horario} con una puntuación numérica siendo mayor cuanto ``más se cumpla la
	 * restricción''.
	 */
	private Function<Horario, Double> fEval;

	/**
	 * Peso que tiene la {@link Restriccion}. Se trata de un factor de escalado, con la finalidad
	 * de poder darle más peso a unas {@link Restriccion}es que a otras.
	 */
	private double peso;

	/**
	 * Indica si la {@link Restriccion} está activa.
	 */
	private boolean active;
	
	/**
	 * Constructor básico.
	 * @param nombre Nombre de la {@link Restriccion}.
	 * @param descripcion Descripción de la {@link Restriccion}.
	 * @param fEval Función que evalua cómo de bueno es un {@link Horario} cumpliendo la {@link Restriccion}.
	 */
	public Restriccion(String nombre, String descripcion, double peso, Function<Horario, Double> fEval) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.fEval = fEval;
		this.active = true;
		this.peso = peso;
	}
	
	/**
	 * Evalua la restricción sobre un {@link Horario}.
	 * @param asignaciones El {@link Horario} que se debe evaluar.
	 * @return Devuelve la nota del {@link Horario}.
	 */
	public double evalua(Horario asignaciones) {
		return fEval.apply(asignaciones);
	}

	/**
	 * @return Devuelve el nombre del {@link Horario}.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * @return Devuelve la descripcion del {@link Horario}.
	 */
	public String getDescripcion() {
		return descripcion;
	}

	/**
	 * @return Devuelve {@code true} si la restricción está activa.
	 */
	public boolean isActive() {
		return active;
	}

	public void setActive(boolean b) {
		this.active = b;
	}

	/**
	 * Cambia el estado de la {@link Restriccion}. Es decir, si estaba activa pasa
	 * a estar inactiva y viceversa, si estaba inactiva pasa a estar activa.
	 * @return Devuelve el estado final de la {@link Restriccion}.
	 */
	public boolean toggleActive() {
		this.active = !this.active;
		return this.active;
	}

	/**
	 * @return Devuelve el peso de la {@link Restriccion}.
	 */
	public double getPeso() {
		return peso;
	}

	/**
	 * Cambia el peso de la {@link Restriccion}.
	 * @param peso El nuevo peso.
	 */
	public void setPeso(double peso) {
		this.peso = peso;
	}	
	
}
