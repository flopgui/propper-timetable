import java.util.ArrayList;

/**
 * Clase que representa una unidad docente (p. e. la FIB). Una unidad docenete contiene una serie de {@link Aula}s
 * en las que se puede dar clase desde una hora mínima hasta una hora máxima y una lista de {@link Titulacion}es.
 */
public class UnidadDocente {

    /**
     * Nombre de la {@link UnidadDocente}.
     */
    private String nombre;

    /**
     * Hora mínima y máxima a la que deben comenzar las clases.
     */
    private int horaMin, horaMax;

    /**
     * Lista de las {@link Titulacion}es de la {@link UnidadDocente}.
     */
    private ArrayList<Titulacion> titulacions;

    /**
     * Lista de las {@link Aula}s de la unidad docente.
     */
    private ArrayList<Aula> aulas;

    /**
     * Constructor básico.
     * @param nombre Nombre de la {@link UnidadDocente}.
     * @param horaMin Hora mínima a la que se puede empezar a hacer clases.
     * @param horaMax Hopra máxima hasta la que se pueden seguir dando clases.
     */
    public UnidadDocente(String nombre, int horaMin, int horaMax) {
        this.nombre = nombre;
        this.horaMin = horaMin;
        this.horaMax = horaMax;
        this.aulas = new ArrayList<Aula>();
        this.titulacions = new ArrayList<Titulacion>();
    }

    /**
     * Añade una {@link Titulacion} a la {@link UnidadDocente}.
     * @param t La {@link Titulacion} a añadir.
     */
    public boolean addTitulacio(Titulacion t) {
        if (titulacions.contains(t))
            return false;
        return titulacions.add(t);
    }

    /**
     * @return Devuelve una lista con los nombres de las {@link Titulacion}es.
     */
    public String[] getTitulaciones() {
        String s[] = new String[titulacions.size()];
        int i = 0;
        for (Titulacion t : titulacions) {
            s[i++] = t.getNombre();
        }
        return s;
    }

    /**
     * @return Devuelve el número de {@link Titulacion}es de la {@link UnidadDocente}.
     */
    public int getNumeroTitulaciones() {
        return titulacions.size();
    }

    /**
     * Consulta la {@link Titulacion} en {@code i}-ésima posición. Muy útil para iterar sobre las {@link Titulacion}a.
     * @param i El número de la {@link Titulacion} a la que se quiere acceder.
     * @return Devuelve la {@link Titulacion} en la posición {@code i}.
     */
    public Titulacion getTitulacion(int i) {
        return titulacions.get(i);
    }

    /**
     * Consulta la {@link Titulacion} cuyo nombre es {@code name}.
     * @param name Nombre de la {@link Titulacion} a consultar.
     * @return Devuelve la {@link Titulacion} con nombre {@code name}.
     */
    public Titulacion getTitulacion(String name) {
        Titulacion t = new Titulacion(name);
        int i = titulacions.indexOf(t);
        if (i == -1)
            return null;
        return titulacions.get(i);
    }

    /**
     * Elimina una {@link Titulacion} de la {@link UnidadDocente}.
     * @param name Nombre de la {@link Titulacion}.
     * @return Devuelve {@code true} sii la {@link Titulacion} existía.
     */
    public boolean removeTitulacion(String name) {
        Titulacion t = getTitulacion(name);
        return titulacions.remove(t);
    }

    /**
     * Añade un {@link Aula} a la {@link UnidadDocente}.
     * @param a {@link Aula} a añadir.
     * @return Devuelve {@code true} sii el {@link Aula} se ha añadido (no existia ya).
     */
    public boolean addAula(Aula a) {
        if (aulas.contains(a)) {
            return false;
        }
        aulas.add(a);
        return true;
    }

    /**
     * @return Devuelve el número total de {@link Aula}s.
     */
    public int getNumeroAulas() {
        return aulas.size();
    }

    /**
     * Consulta el {@link Aula} en {@code i}-ésima posición. Muy útil para iterar sobre las {@link Aula}a.
     * @param i El número del {@link Aula} a la que se quiere acceder.
     * @return Devuelve el {@link Aula} en la posición {@code i}.
     */
    public Aula getAula(int i) {
        return aulas.get(i);
    }

    /**
     * Consulta el {@link Aula} cuyo código es {@code s}.
     * @param s Nombre del {@link Aula} a consultar.
     * @return Devuelve el {@link Aula} con código {@code s}.
     */
    public Aula getAula(String s) {
        for (Aula a : aulas) {
            if (a.getNombre().equals(s))
                return a;
        }
        return null;
    }

    /**
     * Elimina un {@link Aula} de la {@link UnidadDocente}.
     * @param aula Codigo del {@link Aula}.
     * @return Devuelve {@code true} sii el {@link Aula} existía.
     */
    public boolean removeAula(String aula) {
        return aulas.remove(getAula(aula));
    }

    /**
     * @return Nombre de la {@link UnidadDocente}.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Cambia el nombre de la {@link UnidadDocente}.
     * @param n Nuevo nombre.
     */
    public void setNombre(String n) {
        this.nombre = n;
    }

    /**
     * @return Devuelve la hora mínima a la que pueden empezar las clases.
     */
    public int getHoraMin() {
        return horaMin;
    }

    /**
     * Cambia la hora mínima a laa que pueden empezar las clases.
     * @param horaMin Nueva hora.
     */
    public void setHoraMin(int horaMin) {
        this.horaMin = horaMin;
    }

    /**
     * @return Devuelve la hora mácima hasta la cual se pueden estar dando clases.
     */
    public int getHoraMax() {
        return horaMax;
    }

    /**
     * Cambia la hora máxima hasta la que se pueden estar dando clases.
     * @param horaMax Nueva hora.
     */
    public void setHoraMax(int horaMax) {
        this.horaMax = horaMax;
    }
}
