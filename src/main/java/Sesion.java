import java.util.ArrayList;

/**
 * Clase que representa una sesión que se tiene que impartir a todos los grupos (p. e. todos los grupos de AC
 * reciben dos sesiones de teoria de 2 horas cada semana). No confundir con {@link HoraClase}. Además, para identificar
 * a cada sesión, le asignaremos un nombre.
 */
public class Sesion {

    /**
     * Nombre de la {@link Sesion}.
     */
    private String nombre;

    private String titulacion, siglasAsignatura;

    /**
     * Duración de la {@link Sesion}.
     */
    private int duracion;

    /**
     * Número de grupos máximo que pueden asistir a la {@link Sesion}.
     */
    private int nGrupos;

    /**
     * Lista de los {@link Recurso}s necesarios para realizar la {@link Sesion}.
     */
    private ArrayList<Recurso> recursos;

    /**
     * {@link Asignatura} a la que pertenece la {@link Sesion}.
     */
    private transient Asignatura asignatura;

    /**
     * Constructor básico.
     * @param nombre Nombre de la {@link Sesion}.
     * @param asignatura Asignatura de la {@link Sesion}.
     * @param duracion Duración de la {@link Sesion}.
     * @param nGrupos Número de grupos máximos de la {@link Sesion}.
     */
    public Sesion(String nombre, String titulacion, Asignatura asignatura, int duracion, int nGrupos) {
        this(nombre, titulacion, asignatura, duracion, nGrupos, new ArrayList<Recurso>());
    }

    /**
     * Constructor básico.
     * @param nombre Nombre de la {@link Sesion}.
     * @param asignatura Asignatura de la {@link Sesion}.
     * @param duracion Duración de la {@link Sesion}.
     * @param nGrupos Número de grupos máximos de la {@link Sesion}.
     * @param recursos Lista de los {@link Recurso}s necesarios para realizar la {@link Sesion}.
     */
    public Sesion(String nombre, String titulacion, Asignatura asignatura, int duracion, int nGrupos, ArrayList<Recurso> recursos) {
        this.nombre = nombre;
        this.asignatura = asignatura;
        this.siglasAsignatura = asignatura.getSiglas();
        this.titulacion = titulacion;
        if(duracion <= 0)
            throw new java.lang.IllegalArgumentException("La duracion tiene que ser mayor que 0");
        this.duracion = duracion;
        if(nGrupos <= 0)
            throw new java.lang.IllegalArgumentException("Numero de grupos tiene que ser mayor que 0");
        this.nGrupos = nGrupos;
        this.recursos = recursos;
    }

    /**
     * @return Devuelve el nombre de la {@link Sesion}.
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Cambia el nombre de la {@link Sesion}.
     * @param nombre Nuevo nombre.
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return Devuevle la duracion de la {@link Sesion}.
     */
    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) { this.duracion = duracion; }

    /**
     * @return Devuelve el número de grupos máximo que puedes asistir de la {@link Sesion}.
     */
    public int getnGrupos() {
        return nGrupos;
    }

    public void setnGrupos(int nGrupos) {this.nGrupos = nGrupos;}

    /**
     * @return Devuelve la lista de {@link Recurso}s necesarios para realizar la {@link Sesion}.
     */
    public ArrayList<Recurso> getRecursos() {
        return recursos;
    }

    /**
     * Añade un {@link Recurso} necesario a la {@link Sesion}.
     * @param r El {@link Recurso} a añadir.
     */
    public void addRecurso(Recurso r){
        recursos.add(r);
    }

    /**
     * @return Devuelve la {@link Asignatura} de la {@link Sesion}.
     */
    public Asignatura getAsignatura() {
        return asignatura;
    }

    /**
     * Cambia la {@link Asignatura} de la {@link Sesion}.
     * @param asignatura Nueva {@link Asignatura}.
     */
    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
        this.siglasAsignatura = asignatura.getSiglas();
    }

    public String getTitulacion() {
        return titulacion;
    }

    public String getSiglasAsignatura() {
        return siglasAsignatura;
    }
}
