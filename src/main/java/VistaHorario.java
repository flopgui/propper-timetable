import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class VistaHorario extends JPanel implements LangListener {

    CtrlPresentacion ctrl;

    private JButton gen, regen;
    private JLabel noCreated;

    private JSplitPane view;
    private JPanel aulas, asignaturas, tablePanel;
    private JCheckBox aulasAll;
    private JTabbedPane tabPane;

    private HorarioTable table;

    private VistaGenerar vistaGenerar;

    private boolean renderingHorario;

    public VistaHorario(CtrlPresentacion ctrl) {
        this.ctrl = ctrl;
        Lang.registerListener(this);
        renderingHorario = false;

        vistaGenerar = new VistaGenerar(this, ctrl);

        gen = new JButton();
        gen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                vistaGenerar.setVisible(true);
            }
        });
        noCreated = new JLabel();

        aulasAll = new JCheckBox();


        tabPane = new JTabbedPane(JTabbedPane.BOTTOM, JTabbedPane.SCROLL_TAB_LAYOUT);
        tabPane.addTab("temp1", null);
        tabPane.addTab("temp2", null);

        regen = new JButton();
        regen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                vistaGenerar.setVisible(true);
            }
        });


        generateVistaWait();

        onLanguageChanged();
    }

    protected void generateVistaHorario() {
        if (this.getComponentCount() > 0)
            this.remove(gen);
        if (this.getComponentCount() > 0)
            this.remove(noCreated);
        if (this.getComponentCount() > 0)
            this.remove(view);

        aulas = new JPanel();
        aulas.setLayout(new BoxLayout(aulas, BoxLayout.Y_AXIS));
        aulasAll.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                for (int i = 1; i < aulas.getComponentCount(); i++)
                    ((JCheckBox) aulas.getComponent(i)).setSelected(aulasAll.isSelected());
            }
        });
        aulas.add(aulasAll);
        String[] au = ctrl.getAulas();
        for (int i = 0; i < au.length; i++) {
            JCheckBox c = new JCheckBox(au[i]);
            c.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent itemEvent) {
                    ((HorarioTableModel) table.getModel()).displayAula(c.getText(), c.isSelected());
                }
            });
            aulas.add(c);
        }

        asignaturas = new JPanel();
        asignaturas.setLayout(new BoxLayout(asignaturas, BoxLayout.Y_AXIS));
        ArrayList<String> ass = new ArrayList<>();
        String s[] = ctrl.getNombresTitulaciones();
        for (String tit : s) {
            JCheckBox alls = new JCheckBox(tit);
            asignaturas.add(alls);

            String[] as = ctrl.getAsignaturas(tit);
            JCheckBox asigs[] = new JCheckBox[as.length];
            for (int j = 0; j < asigs.length; j++) {
                JCheckBox check = new JCheckBox(as[j]);
                check.setName(as[j] + ":" + tit);
                check.setBorder(BorderFactory.createEmptyBorder(0, 20, 0, 0));
                check.addItemListener(new ItemListener() {
                    @Override
                    public void itemStateChanged(ItemEvent itemEvent) {
                        ((HorarioTableModel) table.getModel()).displayAsignatura(check.getName(), check.isSelected());
                    }
                });
                asigs[j] = check;
                asignaturas.add(asigs[j]);
                ass.add(as[j] + ":" + tit);
            }
            alls.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent itemEvent) {
                    for (JCheckBox jc : asigs)
                        jc.setSelected(alls.isSelected());
                }
            });
        }

        tabPane.setComponentAt(0, aulas);
        tabPane.setComponentAt(1, asignaturas);
        tabPane.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent changeEvent) {
                if (tabPane.getSelectedIndex() == 0) {
                    ((HorarioTableModel) table.getModel()).displayAulas();
                    for (int i = 1; i < aulas.getComponentCount(); i++) {
                        JCheckBox c = (JCheckBox) aulas.getComponent(i);
                        ((HorarioTableModel) table.getModel()).displayAula(c.getText(), c.isSelected());
                    }
                } else {
                    ((HorarioTableModel) table.getModel()).displayAsignaturas();
                    for (int i = 1; i < asignaturas.getComponentCount(); i++) {
                        JCheckBox c = (JCheckBox) aulas.getComponent(i);
                        if (c.getName() != null && c.getName().contains(":"))
                            ((HorarioTableModel) table.getModel()).displayAula(c.getName(), c.isSelected());
                    }
                }
            }
        });

        table = new HorarioTable(ctrl, au, ass);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(table);

        tablePanel = new JPanel(new BorderLayout());
        tablePanel.add(scrollPane, BorderLayout.CENTER);
        JPanel buttons = new JPanel(new GridLayout());
        buttons.add(regen);
        tablePanel.add(buttons, BorderLayout.SOUTH);

        view = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        view.setLeftComponent(tabPane);
        view.setRightComponent(tablePanel);
        view.setOneTouchExpandable(false);

        onLanguageChanged();

        this.setLayout(new BorderLayout());
        this.add(view, BorderLayout.CENTER);
        this.revalidate();
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                view.setDividerLocation(0.25);
            }
        });
    }

    private void generateVistaWait() {
        if (this.getComponentCount() > 0)
            this.remove(gen);
        if (this.getComponentCount() > 0)
            this.remove(noCreated);
        if (this.getComponentCount() > 0)
            this.remove(view);
        
        this.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        this.add(noCreated, gbc);
        gbc.gridy = 1;
        this.add(gen, gbc);
        this.repaint();
    }

    public void onTabSelected() {
        if (renderingHorario != ctrl.hasHorario()) {
            renderingHorario = ctrl.hasHorario();
            if (ctrl.hasHorario()) {
                generateVistaHorario();
            } else {
                generateVistaWait();
            }
            this.revalidate();
        }
    }

    @Override
    public void onLanguageChanged() {
        gen.setText(Lang.getString("generar"));
        regen.setText(Lang.getString("regenerar"));
        noCreated.setText(Lang.getString("nocreated"));
        tabPane.setTitleAt(0, Lang.getString("aulas"));
        tabPane.setTitleAt(1, Lang.getString("asignaturas"));
        aulasAll.setText(Lang.getString("seleccionartodo"));
    }

    public BufferedImage exportarImagen() {
        try {

            BufferedImage image = new BufferedImage(table.getWidth(), table.getHeight() + table.getTableHeader().getHeight(), BufferedImage.TYPE_INT_RGB);
            Graphics2D graphics2D = image.createGraphics();

            table.getTableHeader().paint(graphics2D);
            graphics2D.translate(0,table.getTableHeader().getHeight());
            table.paint(graphics2D);
            graphics2D.dispose();
            return image;
        }
        catch (Exception ex) {
            ctrl.gestionError(Lang.getString("error"));
            ex.printStackTrace();
        }
        return null;
    }
}