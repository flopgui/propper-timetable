import java.util.ArrayList;

/**
 * Esta clase representa un plan de estudios dentro de una {@link Titulacion}. Un plan de
 * estudios tiene un nombre y una lista de {@link Cuatrimestre}s que forman parde del plan de estudios.
 */
public class Titulacion {

    /**
     * Nombre del {@link Titulacion}.
     */
    private String nombre;
    
    /**
     * Lista de cuatrimestres del {@link Titulacion}.
     */
    private ArrayList<Cuatrimestre> cuatrimestres;

    /**
     * Lista de las {@link Asignatura}s de la {@link Titulacion}.
     */
    private ArrayList<Asignatura> asignaturas;

    /**
     * Constructor básico.
     * @param nombre Nombre del {@link Titulacion}.
     */
    Titulacion(String nombre) {
        if (nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("nombre no puede estar vacio");
        this.nombre = nombre;
        asignaturas = new ArrayList<Asignatura>();
        cuatrimestres = new ArrayList<Cuatrimestre>();
    }

    /**
     * Añade una {@link Asignatura} a la {@link Titulacion}.
     * @param a {@link Asignatura} a añadir.
     * @return Devuelve {@code true} sii la {@link Asignatura} se ha añadido (no existia ya).
     */
    public boolean addAsignatura(Asignatura a) {
        if (asignaturas.contains(a))
            return false;
        asignaturas.add(a);
        return true;
    }

    /**
     * @return Devuelve el número total de {@link Asignatura}s.
     */
    public int getNumeroAsignatura() {
        return asignaturas.size();
    }

    /**
     * Consulta la {@link Asignatura} en {@code i}-ésima posición. Muy útil para iterar sobre las {@link Asignatura}a.
     * @param i El número de la {@link Asignatura} a la que se quiere acceder.
     * @return Devuelve la {@link Asignatura} en la posición {@code i}.
     */
    public Asignatura getAsignatura(int i) {
        try {
            return asignaturas.get(i);
        } catch (IndexOutOfBoundsException e) {
            return null;
        }
    }

    /**
     * Consulta la {@link Asignatura} cuyo código es {@code s}.
     * @param s Código de la {@link Asignatura} a consultar.
     * @return Devuelve la {@link Asignatura} con código {@code s}.
     */
    public Asignatura getAsignatura(String s) {
        for (int i = 0; i < asignaturas.size(); i++) {
            if (asignaturas.get(i).getSiglas().equals(s))
                return asignaturas.get(i);
        }
        return null;
    }

    /**
     * Elimina una {@link Asignatura} de la {@link Titulacion}.
     * @param s Codigo de la {@link Asignatura}.
     * @return Devuelve {@code true} sii la {@link Asignatura} existía.
     */
    public boolean removeAsignatura(String s) {
        for (int i = 0; i < asignaturas.size(); i++) {
            if (asignaturas.get(i).getSiglas().equals(s)) {
                asignaturas.remove(i);
                return true;
            }
        }
        return false;
    }
    
    /**
     * @return nombre del {@link Titulacion}.
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * Cambia el nombre del {@link Titulacion}.
     * @param nombre Nuevo nombre.
     */
    public void setNombre(String nombre) {
        if (nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("nombre no puede estar vacio");
        this.nombre = nombre;
    }

    /**
     * Añade un {@link Cuatrimestre} al {@link Titulacion}.
     * @param c El {@link Cuatrimestre} que se quiere añadir.
     * @return Devuelve {@code true} sii no existía un cuatrimestre ya con el mismo nombre.
     */
    public boolean addCuatrimestre(Cuatrimestre c) {
        if (cuatrimestres.contains(c))
            return false;
        cuatrimestres.add(c);
        return true;
    }

    /**
     * @return Devuelve el número de {@link Cuatrimestre}s del {@link Titulacion}.
     */
    public int getNumeroCuatrimestres() {
        return cuatrimestres.size();
    }

    /**
     * Borra el {@link Cuatrimestre} cuyo nombre es {@code c}.
     * @param c Nombre del {@link Cuatrimestre} que se quiere borrar.
     * @return Devuelve {@code true} si existía un {@link Cuatrimestre} con nombre {@code c}
     * (y por lo tanto ahora se ha borrado).
     */
    public boolean removeCuatrimestre(String c) {
        for (int i = 0; i < cuatrimestres.size(); i++) {
            if (cuatrimestres.get(i).getNombre().equals(c)) {
                cuatrimestres.remove(i);
                return true;
            }
        }
        return false;
    }

    /**
     * Método usado para recorrer la lista de {@link Cuatrimestre}s.
     * @param i El número del {@link Cuatrimestre}.
     * @return Devuelve el {@link Cuatrimestre} en la posición {@code i}.
     */
    public Cuatrimestre getCuatrimestre(int i) {
        return cuatrimestres.get(i);
    }

    public Cuatrimestre getCuatrimestre(String s) {
        for (Cuatrimestre c: cuatrimestres) {
            if (c.getNombre().equals(s)) return c;
        }
        return null;
    }
    public boolean containsAsignatura(Asignatura a) {
        return this.asignaturas.contains(a);
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Titulacion)
            return this.nombre.equals(((Titulacion) other).nombre);
        return false;
    }

}
