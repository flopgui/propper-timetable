import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.DuplicateFormatFlagsException;

public class VistaRecursos extends JDialog {


    public static String[] gestionaRecursos(String[] allData, java.util.List<String> selectedData) {
        String[] original = new String[selectedData.size()];
        selectedData.toArray(original);
        final boolean[] killed = {false};

        JDialog d = new JDialog();
        d.setPreferredSize(new Dimension(300,300));
        JPanel mid = new JPanel();
        JPanel bot = new JPanel(new FlowLayout(FlowLayout.RIGHT));

        JScrollPane scrollPane1 = new JScrollPane();

        DefaultListModel<String> model1 = new DefaultListModel<>();
        for (String data : allData)
            if (!selectedData.contains(data))
                model1.addElement(data);
        JList<String> all = new JList<String>(model1);
        all.setDragEnabled(true);
        scrollPane1.setViewportView(all);

        JScrollPane scrollPane2 = new JScrollPane();
        DefaultListModel<String> model2 = new DefaultListModel<>();
        JList<String> selected = new JList<String>(model2);
        for (String data : selectedData)
            model2.addElement(data);
        selected.setDragEnabled(true);
        scrollPane2.setViewportView(selected);

        JPanel panel = new JPanel();
        JButton add = new JButton(Lang.getString("moverderecha"));
        add.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                for (int i : all.getSelectedIndices())
                    model2.addElement(model1.remove(i));
            }
        });
        JButton remove = new JButton(Lang.getString("moverizquierda"));
        remove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                for (int i : selected.getSelectedIndices())
                    model1.addElement(model2.remove(i));
            }
        });
        JButton newr = new JButton(Lang.getString("nuevo"));
        newr.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String s = JOptionPane.showInputDialog(Lang.getString("newname"), "");
                if(!model2.contains(s)) model2.addElement(s);
            }
        });
        GroupLayout layout = new GroupLayout(panel);
        panel.setLayout(layout);

        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(add, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(remove, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(newr, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addComponent(add)
                .addComponent(remove)
                .addComponent(newr)
        );

        GroupLayout lay = new GroupLayout(mid);
        lay.setHorizontalGroup(lay.createSequentialGroup()
                .addComponent(scrollPane1)
                .addComponent(panel)
                .addComponent(scrollPane2));
        lay.setVerticalGroup(lay.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addComponent(scrollPane1)
                .addComponent(panel)
                .addComponent(scrollPane2));

        JButton cancel = new JButton(Lang.getString("cancelar"));
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                d.dispose();
                killed[0] = true;
            }
        });
        JButton ok = new JButton(Lang.getString("confirmar"));
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                d.dispose();
            }
        });
        bot.add(cancel);
        bot.add(ok);

        d.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                killed[0] = true;
            }
        });

        d.setLayout(new BorderLayout());
        mid.setLayout(lay);
        d.add(mid, BorderLayout.CENTER);
        d.add(bot, BorderLayout.SOUTH);

        d.setModal(true);
        d.pack();
        d.setVisible(true);

        if (killed[0])
            return original;

        String[] s = new String[model2.getSize()];
        for (int i = 0; i < model2.getSize(); i++)
            s[i] = model2.get(i);
        return s;
    }
}
