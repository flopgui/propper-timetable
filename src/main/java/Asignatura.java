import java.awt.Color;
import java.util.ArrayList;

/**
 * Clase que representa una asignatura. Una asignatura tiene un nombre,
 * unas siglas, un número de grupos, una capacidad por grupo y una lista de todas
 * las {@link Sesion}es que hay que realizar durante una semana (p.e. dos horas de teoría y una de práctica).
 */
public class Asignatura {

    private Color color;

    private int numeroAlumnos;

    /**
     * Nombre de la {@link Asignatura}.
     */
    private String nombre;
    
    /**
     * Siglas de la {@link Asignatura}.
     */
    private String siglas;
	
    /**
     * Capacidad máxima de los {@link Grupo}s de la {@link Asignatura}.
     */
    private int capacidadGrupo;
    
    /**
     * Numero de {@link Grupo}s que tiene {@link Asignatura}.
     */
    private int numeroGrupos;
    
    /**
     * Conjunto de {@link Sesion}es que tiene {@link Asignatura}.
     */
    private ArrayList<Sesion> sesiones;

    /**
     * Constructor de la {@link Asignatura}.
     * @param nombre Nombre de la {@link Asignatura}.
     * @param siglas Siglas de la {@link Asignatura}.
     * @param capacidadGrupo Capacidad máxima de los {@link Grupo}s de la {@link Asignatura}.
     */
    public Asignatura(String nombre, String siglas, int capacidadGrupo, int numeroAlumnos) {
        if (nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("nombre no puede estar vacio"); 
        if (siglas.isEmpty())
            throw new java.lang.IllegalArgumentException("siglas no puede estar vacio"); 
        if (capacidadGrupo <= 0)
            throw new java.lang.IllegalArgumentException("capacidadGrupo <= 0"); 
    	this.nombre = nombre;
    	this.siglas = siglas;
    	this.capacidadGrupo = capacidadGrupo;
    	this.numeroAlumnos = numeroAlumnos;
    	this.sesiones = new ArrayList<Sesion>();
    	this.color = new Color((int)(Math.random() * 0x1000000));
    }

    /**
     * Comparador de la clase {@link Asignatura}. Útil para trabajar con {@link java.util.List}.
     * @param other Otro objeto.
     * @return Cierto si {@code other} es una {@link Asignatura} con las mismas siglas.
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof Asignatura)
            return this.siglas.equals(((Asignatura) other).siglas);
        return false;
    }

    /**
     * Añade una {@link Sesion} a realizar a la {@link Asignatura}.
     * @param s La {@link Sesion} a añadir.
     */
    public boolean addSesion(Sesion s) {
        if (sesiones.contains(s)) return false;
        sesiones.add(s);
        return true;
    }

    public boolean deleteSesion(String s) {
        for (int i=0; i<sesiones.size(); ++i) {
            if (sesiones.get(i).getNombre().equals(s)) {
                sesiones.remove(i);
                return true;
            }
        }
        return false;
    }

    /**
     * @return Nombre de la {@link Asignatura}.
     */
    public String getNombre() {
        return nombre;
    }
    
    /**
     * Cambia el nombre de la {@link Asignatura}.
     * @param nombre Nuevo nombre.
     */
    public void setNombre(String nombre) {
        if (nombre.isEmpty())
            throw new java.lang.IllegalArgumentException("nombre no puede estar vacio");
        this.nombre = nombre;
    }
    
    /**
     * @return siglas de la {@link Asignatura}.
     */
    public String getSiglas() {
        return siglas;
    }

    /**
     * Cambia las siglas de la {@link Asignatura}.
     * @param siglas Nuevas siglas.
     */
    public void setSiglas(String siglas) {
        if (siglas.isEmpty())
            throw new java.lang.IllegalArgumentException("siglas no puede estar vacio"); 
        this.siglas = siglas;
    }

    /**
     * @return Devuelve una lista con todas las {@link Sesion}es que tiene la asignatura.
     */
    public ArrayList<Sesion> getSesiones() {
        return sesiones;
    }

    public Sesion getSesion(String nombre) {
        for (Sesion s: sesiones) {
            if (s.getNombre().equals(nombre)) {
                return s;
            }
        }
        return null;
    }
    
    /**
     * @return capacidad de un grupo de la {@link Asignatura}.
     */
    public int getCapacidadGrupo() {
        return capacidadGrupo;
    }
    
    /**
     * Cambia la capacidad de un grupo de la {@link Asignatura}.
     * @param capacidadGrupo Nueva capacidad de un grupo.
     */
    public void setCapacidadGrupo(int capacidadGrupo) {
        if (capacidadGrupo <= 0)
            throw new java.lang.IllegalArgumentException("capacidadGrupo <= 0"); 
        this.capacidadGrupo = capacidadGrupo;
    }

    /**
     * @return numero de grupos de la {@link Asignatura}.
     */
    public int getNumeroGrupos() {
        return (int) Math.ceil(((double) numeroAlumnos)/((double) capacidadGrupo));
    }


    public Asignatura clone() {
        Asignatura a = new Asignatura(this.nombre+"*", this.siglas+"*", this.capacidadGrupo, this.numeroAlumnos);
        for (Sesion s: sesiones) {
            Sesion news = new Sesion(s.getNombre(),s.getTitulacion(), a, s.getDuracion(), s.getnGrupos());
            for (Recurso r: s.getRecursos()) news.addRecurso(r);
            a.addSesion(news);

        }
        a.numeroGrupos = this.numeroGrupos;
        return a;
    }

    public int getNumeroAlumnos() {return numeroAlumnos;}

    public void setNumeroAlumnos(int numeroAlumnos) {this.numeroAlumnos = numeroAlumnos;}

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public void asociaSesiones() {
        for (Sesion s : sesiones) {
            s.setAsignatura(this);
        }
    }

}
