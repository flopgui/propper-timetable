# PROPPER-TIMETABLE
## MANUAL DE USUARIO

<div class="pagebreak"></div>

**Índice**   

1. Introducción
2. Ejecución
3. Funcionalidades
4. Algoritmo
5. Créditos

<div class="pagebreak"></div>

# 1. Introducción

Este manual tiene como finalidad dar a conocer las funcionalidades básicas y las características del programa Propper-Timetable, un proyecto efectuado a la Facultad de Informática de Barcelona (FIB) durante el curso de la asignatura proyectos de programación (PROP). El proyecto ha sido  creado por Ernesto Lanchares Sánchez, Ferran López Guitart y Niebo Zhang y tutelado por el profesor Jordi Turmo.

El objetivo principal de este proyecto consiste en recrear un entorno cómodo y poder generar un horario válido a partir de unas condiciones  y datos establecidas previamente.

# 2. Ejecución

//TODO Esto suponiendo que van en el mismo jar

Se debe ejecutar el siguiente comando:

java -jar propper-timetable.jar

Es necesario tener instalado Java 8 o superior.

# 3. Funcionalidades

La interfície del usuario se divide principalmente en 3 grandes bloques, que llamaremos: VistaAula, VistaHorario y VistaTitulación. Una vez iniciado el programa, el usuario accederá de manera predeterminada a la VistaAula, aunque tiene la opción de escoger cual quiere visualizar a partir de unas pestañas que puede encontrar justo debajo la barra de menú.

![Barra menú](./Images/1.png)

En la barra de menú de la parte superior, que permancerá fija durante toda la ejecución del programa, se podrá realizar diferentes tareas. 

## 3.1 Barra de menú

### 3.1.1 Archivo

El usuario al darle clic a la opción de archivo de la barra de menú puede escoger diferentes acciones que tienen que ver directamenet con la gestión del proyecto, como por ejemplo abrir o guardar este:

![Menu archivo](./Images/VistaMenu.png)

#### 3.1.1.1 Crear nuevo proyecto

Al darle a la opción de crear nuevo proyecto o introduciendo ctrl + n, el programa automáticamente invoca una pestaña y pide al usuario si quiere guardar el proyecto actual para poder recuperarlos más adelante. 

![vista](./Images/Vistasave2.png)

En caso afirmativo, si el proyecto ya ha sido guardado previamente, el programa guarda automáticamente éste sobreescribiendo sobre el archivo .ttb correspondiente. Si el proyecto nunca ha sido guardado y, por ende, no existe ningun archivo que lo contenga, el usuario tiene que introducir el directorio donde quiere guardar el archivo y el nombre que desea que tenga el proyecto.

![save](./Images/VistaSaev2.png)

En caso negativo, **el proyecto actual se perderá y, por consecuencia, no se podrá recuperar**.  

Seguidamente el programa muestra una pestaña donde el usuario tiene que escoger el directorio donde se encuentra el proyecto que desea abrir y el nombre de este. Al finalizar el programa abrirá el proyecto y mostrará los datos que contiene este. 

#### 3.1.1.2 Abrir proyecto

Al darle a la opción de abrir proyecto o introduciendo ctrl + o, el programa automáticamente invoca una pestaña y pide al usuario si quiere guardar el proyecto actual para poder recuperarlos más adelante. 

En caso afirmativo, si el proyecto ya ha sido guardado previamente, el programa guarda automáticamente éste sobreescribiendo sobre el archivo .ttb correspondiente. Si el proyecto nunca ha sido guardado y, por ende, no existe ningun archivo que lo contenga, el usuario tiene que introducir el directorio donde quiere guardar el archivo y el nombre que desea que tenga el proyecto.

![abir](./Images/VistaAbrir.png)

#### 3.1.1.3 Abrir reciente

El programa guarda los cinco proyectos ejecutados más recientes automáticamente.  Al darle a la pestaña de abrir reciente muestra un submenú con estos cinco proyectos, o en caso de que no haya cinco proyectos,  los proyectos que tenga guardado hasta ese momento. El usuario puede abrir estos directamente dándole clic. Antes de abrir el proyecto escogido, el programa pide si el usuario quiere guardar el proyecto actual.

En caso afirmativo, si el proyecto ya ha sido guardado previamente, el programa guarda automáticamente éste sobreescribiendo sobre el archivo .ttb correspondiente. Si el proyecto nunca ha sido guardado y, por ende, no existe ningun archivo que lo contenga, el usuario tiene que introducir el directorio donde quiere guardar el archivo y el nombre que desea que tenga el proyecto.

En caso negativo, **el proyecto actual se perderá y, por consecuencia, no se podrá recuperar**.  

#### 3.1.1.4 Guardado

Podemos guardar proyecto o también introduciendo ctrl + s. Si el proyecto ya ha sido guardado previamente, el programa guarda automáticamente éste sobreescribiendo sobre el archivo .ttb correspondiente. Si el proyecto nunca ha sido guardado y, por ende, no existe ningun archivo que lo contenga, el usuario tiene que introducir el directorio donde quiere guardar el archivo y el nombre que desea que tenga el proyecto. **El proyecto se guarda por defecto con extensión ttb".

#### 3.1.1.5 Guardado como...

Podemos guardar el proyecto o también introduciendo ctrl + shift + s. El programa muestra la pestaña de guardado y el usuario tiene que escoger el nombre que quiere ponerle al proyecto, el directorio donde quiere guardar, y en caso de que se deseé puede escoger el formato del proyecto. **Si ya existe un proyecto con la misma extensión y nombre, el programa directamente sobreescribirá sobre este, y por consecuencia se perderá el proyecto anterior**. 

#### 3.1.1.6 Importar

//TODO

#### 3.1.1.7 Exportar

//TODO

#### 3.1.1.8 Preferencias

//TODO Si hay autoguardado

Al darle clic o introudicendo ctrl + shift + p, el programa nos saltará una pestaña para poder modificar ciertos aspectos del programa, por ejemplo, el usuario puede escoger el idioma que desea tener el programa. Las lenguas disponibles en esta versión son: catalán, español e inglés. Además el usuario puede marcar si quiere la opción de autoguardado y en este caso introducir el intervalo de tiempo que desea entre autoguardados. **Si el usuario deja este campo en vacío el programa entenderá que quiere autoguardado permanentemente, esto puede influir negativamente a la ejecución del programa.** Al darle al botón de aceptar se efectuarán los cambios que el usuario haya establecido.

![Preference](./Images/VistaPreference.png)

#### 3.1.1.9 Salir

La opción de salir o también ctrl + q cerrará el programa, pero antes este confirma si el usuario esta seguro de querer salir, **si el usuario acepta la confirmación el programa se cerrará y los cambios efectuados en el proyecto no se guardarán**. En caso contrario se cierra la pestaña, el programa sigue funcionando y no se efectua ningun cambio.

### 3.1.2 Help

#### 3.1.2.1 Help
#### 3.1.2.2 About

## 3.2 VistaAula

En la ventana de VistaAula tenemos una interfície simple y directa, nos muestra la tabla de aulas que contiene la unidad docente actual y sus propiedades básicas, es decir: el nombre de la aula, la capacidad de esta y los recursos que esta contiene.

![VistaAula](./Images/VistaAula1.png)

### 3.1.1  Crear Aula

Al darle clic al único botón que tenemos en todo la ventana de VistaAula, nos muestra una subventana donde el usuario introduce los datos básicos de la aula, es decir el nombre y la capacidad de este. 

![CrearAula](./Images/VistaAula2.png)

En caso de que el usuario escriba un nombre de aula de una ya existente, el programa notificará el error y cancelará la acción de creado. 

![error2](./Images/VistaErrorAula2.png)

En caso de que el usuario introduzca algo diferente a un entero positivo en el campo de capacidad, el programa notificará el error y cancelará la acción de creado.

![error](./Images/VistaErrorAula.png)

Si los dos campos cumplen con las condiciones establecidas, el programa crea una nueva fila en la tabla con los datos introducidos, en este caso la aula no tendrá ningun recurso.

### 3.1.2 Aula menú

En la tabla si damos clic derecho a una fila válida, es decir que contenga una aula, nos mostrará el siguiente menú:

![menuAula](./Images/VistaAula3.png)

#### 3.1.2.1 Eliminar

Al darle a la opció de eliminar, el programa pedirá la confirmación del usuario. Si el usuario acepta la acción, es este caso el programa eliminará la aula de la fila seleccionada o en caso que haya múltiples aulas, el programa eliminará todas las aulas seleccionadas. Si el usuario cancela la acción, las aulas permanecerán intactas.

![delete](./Images/vistadelte.png)

#### 3.1.2.2 Clonar

Al darle a la opción de clonar, el programa saltará una subventana donde el usuario puede introducir los nombre de las nuevas aulas que serán clones de la aula seleccionada actualmente, o en caso de múltiples selecciones, de la aula seleccionada más recientemente.

![Clone](./Images/CloneAula.png)

Para cada línea con un nombre el programa creará dicha aula, con las mismas propiedades que la original. **En caso de que ya exista la aula, se creará un clon con el nombre modificado añadiendo "*"**.

![Clone](/home/niebo11/project/propper-timetable/Images/vistaclone.png)



#### 3.1.2.3 Gestor de recursos

Al darle a la opción de clonar, el programa saltará una subventana donde podremos gestionar los recursos de la aula seleccionada.

En la lista de la izquierda nos muestra todos los recursos que han sido listados hasta el momento de la unidad docente y que no están en la aula actual, mientras que en la lista de la derecha nos muestra la lista de todos los recursos que tiene la aula. El usuario puede mover libremente de una lista a otro a partir de las flechas. 

![Recursos](/home/niebo11/project/propper-timetable/Images/VistaRecurso.png)

En caso que el usuario no encuentre el recurso que quiere en ninguna de las listas, este puede crear nuevos recursos introduciendo el nombre de este a partir del botón "nuevo".

![Recursos](./Images/VistaRecurso2.png)

**Los cambios se efectuarán solamente si el usuario le da al botón "OK", en caso contrario el programa deshace todos los cambios que el usuario haya efectuado**.

### 3.1.3 Cambios directos en la tabla

Para cambiar el nombre o la capacidad de una aula, el usuario puede modificar directamente estos valores en la tabla introduciendo el nuevo valor deseado.

En caso que ya exista una aula con nombre introducido, el programa notifica al usuario que está duplicado y deshace la acción.

En caso que el usuario introduzca un valor no válido en el campo de capacidad el programa notifica al usuario que no es posible tal acción y desahce el cambio deseado.

**El usuario sin embargo no podrá efectuar cambios en los recursos de una aula directamente sobre la table, solo podrá hacerlos en el gestor de recursos".

### 3.1.4 Opción de búsqueda

Al darle ctrl + f, saldra un campo de texto en nuestra ventana:
//TODO image, is not working anymore btw :D
El usuario puede introducir el nombre de la aula que desea buscar, la tabla solo mostrará aquellas aulas con campos iguales. O ninguna aula en caso que ninguna aula coincida.

## 3.3  VistaTitulacion

En la ventana de VistaTitulacion todos los cambios relacionados con las titulacions, asignaturas y sesiones de la unidad docente. En caso de que no se haya seleccionado ninguna titulación, entonces el programa muestra la lista de titulaciones existentes en la unidad docente y un botón para crear nuevas titulaciones, la lista puede estar vacía si no existe ninguna titulación. En caso de que hayamos seleccionado alguna titulación nos mostrará a la derecha de esta lista un gestor de cuatrimestres de esta titulación, donde podremos añadir y/o eliminar cuatrimestres, dentro de esta además podremos también añadir y/o eliminar asignaturas.

![VistaT](./Images/VistaTitulacion1.png)

### 3.3.1 Crear titulación

Dándole clic al botón de crear, el pogroma solicita que introduzcamos el nombre de la titulación que queremos crear. Si el usuario desea cancelar la acción de crear titulación puede anularlo dándole a cancelar. En caso contrario puede seguir la acción dándole a aceptar. 

![VistaTitulacion](./Images/visittitulacion2.png)

En caso que el nombre introducido ya exista el programa notifica con un error y cancelará la acción de creación.

En caso que el usuario deje el campo de nombre en vacío el programa notifica que es inválido y deshace la acción.

Si cumple todas las condiciones, el programa crea una nueva titulación y la añade a la lista.

### 3.3.2 Menú titulación

Si le damos clic derecho a la lista de titulación nos aparecerá dos opciones:

#### 3.3.2.1 Eliminar

Al darle a la opció de eliminar, el programa pedirá la confirmación del usuario. Si el usuario acepta la acción, es este caso el programa eliminará la titulación de la lista seleccionada. Si el usuario cancela la acción, la titulación permanecerán intactas.

#### 3.3.2.2 Cambiar nombre

Al darle a la opción de cambiar nombre, el programa pide que introduzcamos el nombre nuevo que queremos darle a la titulación.

En caso que el nombre introducido ya exista el programa notifica con un error y cancela el cambio.

En caso que el usuario deje el campo de nombre en vacío el programa notifica que es inválido y deshace la acción.

Si cumple todas las condiciones, el programa cambia el nombre de la titulación por el nombre nuevo.

### 3.3.3 Modificar titulación

Para modificar una titulación, el usuario tiene que darle clic a la titulación deseada de la lista, en caso que no exista tiene que crearla. Al darle clic a una titulación, en la ventana se mostrará un gestor de titulación.

![Modificartit](./Images/vistatitulacion3.png)

#### 3.3.3.1 Crear cuatrimestre

Para crear un cuatrimestre nuevo, el usuario tiene que darle clic al botón de "+" que más abajo se encuentre, en caso que no haya ningún cuatrimestre será el único botón de "+" que haya. El programa crea directamente el nuevo cuatrimestre solicitado, los cuatrimestres irán numerados, por ende, no es posible crear el cuatrimestre 5 sin que haya un cuatrimestre 4 previo.

![creacuatri](./Images/vistatitulacion4.png)

#### 3.3.3.2 Eliminar cuatrimestre

Para eliminar un cuatrimestre, el usuario tiene que darle clic al botón de "-", esta función, sin embargo, hace algo, solo si existe algun cuatrimestre previo. 
En caso que el cuatrimestre contenga alguna asignatura, el programa notifica que el cuatrimestre no está vacío y pregunta si quiere seguir la eliminación. Si el usuario le da al botón de aceptar, el cuatrimestre se eliminará junto a todas las asignaturas que esta contenía, en caso contrario no efectuará ningun cambio.
En caso que el cuatrimestre no contenga ninguna asignatura, el programa efectuará la eliminación directamente.

#### 3.3.3.3 Crear asignatura

Para crear una asignutara el usuario tiene que escoger el cuatrimestre donde quiere crearlo y en esta darle al botón de "+";

Seguidamente, el programa nos muestra una subventana con los campos a introducir y una table vacía que se podrá llenar con las sesiones de la asignatura.

![asig](./Images/VistaSesion.png)

En caso que introduzcamos campos inválidos el programa notifica del error y el usuario puede modificar los campos directamente.

El usuario puede asociarle un color a la asignatura creada, puede ser un color predefinido, o en cualquier caso uno personalizado.

![color](./Images/VistaColor.png)

Si se desea cancelar la opción de creación, tan solo hay que darle al botón de cancelar.

También tenemos la opción de crear las sesiones asociadas a la asignatura.

**Para confirmar la creació de la asignatura el usuario tiene que darle al botón de OK**.

##### 3.3.3.3.1 Crear sesión

Para crear una sesión tenemos que estar en la subventana de la asignatura. El usuario solo tiene que darle al botón de sesión nueva y el programa crea una nueva fila en la tabla de sesiones, estas están numeradas. Los valores predeterminados de estas sesiones son de 1 hora de duración y de 1 grupo por sesión.

##### 3.3.3.3.2 Modificar sesión

Para modificar los valores de las sesiones, es decir la duración y el número de grupos por sesión, se puede hacer directamente en la table. En caso que el valor introducido sea inválido el programa notifica al usuario del error y restablece el valor del campo a 1.

Para modificar los recursos necesarios de una sesión, tal solo hay que darle clic al campo de recursos y saltará un gestor similar al de VistaAula.

En la lista de la izquierda nos muestra todos los recursos que han sido listados hasta el momento en la unidad docente y que no son requeridos en la sesión actual, mientras que en la lista de la derecha nos muestra la lista de todos los recursos que son necesarios para la sesión. El usuario puede mover libremente los recursos en la unidad docente y los requeridos de una lista a otro a partir de las flechas. 

En caso que el usuario no encuentre el recurso que quiere en ninguna de las listas, este puede crear nuevos recursos introduciendo el nombre de este a partir del botón "nuevo".

**Los cambios se efectuarán solamente si el usuario le da al botón "OK", en caso contrario el programa deshace todos los cambios que el usuario haya efectuado**.

##### 3.3.3.3.3 Eliminar sesión

//TODO you can't so cry

#### 3.3.3.4 Modificar asignatura

Para modificar un asignatura, el usuario tiene que darle clic al botón correspondiente a la asignatura, este tendrá las siglas escritas en el botón. El programa abrirá una subventana similar a la de creación donde podremos modificar los valores. 

Si algun valor es inválido el programa lo notifica y lo deshace.

**Los cambios se efectuarán solamente si el usuario le da al botón "OK", en caso contrario el programa deshace todos los cambios que el usuario haya efectuado**.

#### 3.3.3.5 Eliminar asignatura

Para eliminar una asignatura, el usuario tiene que darle clic derecho al botón de la asignatura y escoger la opció eliminar del menú. 

//TODO Eliminar asignatura pedir permiso

Al darle a la opción de eliminar, el programa pedirá la confirmación del usuario. Si el usuario acepta la acción, en este caso el programa eliminará asignatura. Si el usuario cancela la acción, la asignatura permanecerá intacta.

#### 3.3.3.6 Clonar asignatura

Para clonar una asignatura, el usuario tiene que darle clic derecho al botón de la asignatura y escoger la opció clonar del menú. 

Al darle a la opción de clonar, el programa pedirá las siglas de la asignatura nueva. En caso que ya exista o el valor nuevo sea inválido el programa notifica al usuario y no crea la asignatura.

La nueva asignatura tiene todos los mismos valores que la asigntura del que se ha clonado menos las siglas.

#### 3.3.3.7 Mover asignatura

//TODO Arrastrar?

Para mover una asignatura de cuatrimestre, el usuario tiene que dar clic derecho al botón de la asignatura y escoger la opció mover del menú.

Al darle a la opción de clonar, el programa pedirá a que cuatrimestre quiere moverse la asignatura.

## 3.4 VistaHorario

En la ventana de VistaHorario muestra al usuario el horario generado, o en caso que este no haya sido generado todavía el botón de generar. 

![horario](./Images/VistaHorario1.png)

### 3.4.1 Generar Horario

Para generar horario el usuario le da clic al botón de generar. Seguidamente el programa muestra al usuario las condiciones que este quiere para el horario generado. El usuario solo tiene que seleccionar los campos que quiera que su horario cumpla. También tiene la opción de cambiar las horas lectivas en la parte superior de la subventana. 

![generar](./Images/VistaGenerar1.png)

Si el usuario quiere darle más peso a alguna restricción, puede cambiar el peso de la restricción directamente desde la tabla. Si introduce un valor inválido el programa marcará en rojo la celda donde se encuentre el error y no permite otros cambios en la tabla hasta que se corrija el error. En caso que el usuario dé a generar horario sin cambiar el error, el horario se calculará con el valor inicial.

![generar](./Images/VistaGenerar2.png)

El programa genera el horario cuando el usuario le da al botón de generar.

![generando](./Images/VistaCarregar.png)

El significado de cada una de las restricciones se puede encontrar en el capítulo 4 de este manual.

### 3.4.2 Horario por clases

Si existe horario, entonces la ventana de VistaHorario se divide en dos partes, en la parte de la izquierda, el usuario puede seleccionar los elementos que quiere ver en la tabla, en el caso de la pestaña de aulas, se encuentran todas las aulas existentes de la unidad docente.

En la parte derecha de la ventana se verá el horario respecto a las aulas seleccionadas. El color de cada clase dependerá del color que hemos asociado a la asignatura que se cursa en la aula.

### 3.4.3 Horario por titulaciones

Si existe horario, entonces la ventana de VistaHorario se divide en dos partes, en la parte de la izquierda, el usuario puede seleccionar los elementos que quiere ver en la tabla, en el caso de la pestaña de las titulaciones, se encuentran todas las titulaciones existentes de la unidad docente, además cada titulación tiene una rama con todas sus asignaturas, por lo tanto el usuario puede escoger ver solo algunas asignaturas de la titulación

En la parte derecha de la ventana se verá el horario respecto a las asignaturas y/o titulaciones seleccionadas. El color de cada clase dependerá del color que hemos asociado a la asignatura que se cursa.

### 3.4.4 Regenerar

Si existe horario, el usuario puede escoger crear otro horario, con el botón de regenerar horario. Al darle clic nos volverá a salir la subventana para que escojamos las restricciones queridas, para finalizar el usuario solo tiene que darle clic a generar horario para que aparezca el nuevo horario generado.
**Si el horario previo no se ha guardado, hay que tener en cuenta que se perderá al regenerar un horario nuevo**

### 3.4.5 Exportar como imagen

Una vez creado el horario, el usuario puede guardarlo como imagen JPEG. El horario será guardado tal como aparece en pantalla, con las aulas o asignaturas que se hayan seleccionado. Si el horario no 
cabe en la pantalla en la imagen exportada saldrá cortado.

# 4. Algoritmo

El algoritmo principal implementado es un algoritmo genético que genera un horario
que procura adaptarse lo mejor posible a las restricciones. El funcionamiento conceptual
del algoritmo es sencillo: 

1. Genera una **población** (un conjunto) inicial de horarios.
2. Ahora selecciona dos horarios, de tal forma que aquellos horarios que mejor se adapten
   a las restricciones sean los más probables en ser elegidos. 
3. Estos dos horarios se **reproducen** (crean un nuevo horario con atributos mezclados de los dos padres) y estenuevo horario creado, formará parte de la siguiente generación. 
4. El proceso de selección de padres y de reproducción se repite haste obtener una nueva generación con un tamaño igual a la anterior. Como hemos seleccionado a los mejores horarios de la generación pasada, idealmente esta nueva generación cumple (en general) mejor las restricciones que la generación anterior. 
5. Iteramos el proceso de creación de generaciones, obteniendo cada vez una generación mejor hasta que llegue un punto que el mejor horario de una generación sea una solución más que aceptable al problema.

## 4.1 Restricciones

|        Restricción       	|                                                               Significado                                                              	| Peso Recomendado 	|
|:------------------------:	|:--------------------------------------------------------------------------------------------------------------------------------------:	|:----------------:	|
| SOLAPAMIENTOS            	| Evita que las sesiones de más de una hora de duración se solapen. No es necesaria si todas las sesiones duran una hora.                	|         2        	|
| DISPERSION_HORAS         	| Reparte las sesiones equitativamente a lo largo del día.                                                                               	|       0.05       	|
| ACABAR_A_HORA            	| Evita que las sesiones de más de una hora acaben más tarde de la hora de cierre. No es necesaria si todas las sesiones duran una hora. 	|        0.5       	|
| CAPACIDAD_EXCEDIDA       	| Evita que las aulas se llenen por encima de su capacidad.                                                                              	|        0.2       	|
| CAPACIDAD_DESAPROVECHADA 	| Evita que las aulas queden demasiado vacías.                                                                                           	|       0.02       	|
| RECURSOS                 	| Hace que las aulas tengan los recursos necesarios para cada sesión.                                                                    	|        0.3       	|
| CUATRIMESTRES            	| Reparte las sesiones de las asignaturas de un mismo cuatrimestre para que se puedan cursar al mismo tiempo.                            	|        0.1       	|
| MISMA_ASIGNATURA         	| Evita que haya varias sesiones de la misma asignatura a la misma hora.                                                                 	|        0.2       	|
| MISMA_ASIGNATURA_DIA     	| Reparte las sesiones de una asignatura a lo largo de la semana.                                                                        	|        0.1       	|


# 5. Créditos

Ernesto Lancharez Sanchez

Ferran López Guitart

Niebo Zhang
